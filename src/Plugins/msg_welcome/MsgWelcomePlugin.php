<?php

namespace Plugins\msg_welcome;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Plugin;
use AppBundle\Event\Player\PlayerConnectedInfoEvent;
use AppBundle\Packet\IS_MTC;

class MsgWelcomePlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translation) {
        parent::__construct($plugin, $translation);

        $this->eventDispacher->addListener(PlayerConnectedInfoEvent::NAME, [$this, 'onPlayerConnectedInfo']);
    }

    /**
     * Event called on player connected
     * 
     * @param PlayerConnectedInfoEvent $event
     */
    public function onPlayerConnectedInfo(PlayerConnectedInfoEvent $event) {
        // Skip host connection
        if ($event->packet->UCID == 0) {
            return;
        }
        
        $this->translator->setLocale($event->player->getLocale());
                
        $msg = new IS_MTC();
        $msg->UCID = $event->packet->UCID;
        $msg->Text = $this->translator->trans('msg.welcome.1', ['%help%' => '!help']);
        $event->host->insim->send($msg);
        
        $msg->Text = $this->translator->trans('msg.welcome.2');
        $event->host->insim->send($msg);
    }

}
