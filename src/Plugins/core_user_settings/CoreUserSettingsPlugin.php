<?php

namespace Plugins\core_user_settings;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;
use AppBundle\Event\Player\PlayerShowSettingsEvent;
use Plugins\core_user_settings\UI\UIUserSettingsWindow;

class CoreUserSettingsPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translator) {
        parent::__construct($plugin, $translator);

        $this->registerCommand('opt', 'Displays users settings', [$this, 'showUserSettings']);

        $this->eventDispacher->addListener(PlayerShowSettingsEvent::NAME, [$this, 'onPlayerShowSettings']);
    }

    public function showUserSettings(Player $player, Host $host) {
        $this->translator->setLocale($player->getLocale());

        $window = new UIUserSettingsWindow($this->eventDispacher, $this->translator, $player, $host);
        $window->setPosition(50)->setWidth(80)->setHeight(60)->setCenter();
        $window->setTitle($this->translator->trans('user.settings.title'));

        $player->getUi()->showWindow($window);
    }

    public function onPlayerShowSettings(PlayerShowSettingsEvent $event) {
        $this->showUserSettings($event->player, $event->host);
    }

}
