<?php

namespace Plugins\core_user_settings\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\UI\Element\Filter;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Event\Button\ButtonClickedEvent;
use Plugins\core_user_settings\UI\Tab\LocalizationUnitsTab;
use Plugins\core_user_settings\UI\Tab\NotificationsGeneralTab;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI user settings window
 */
class UIUserSettingsWindow extends UIWindow {

    /** @var Filter */
    private $filter;

    public function __construct(EventDispatcherInterface $eventDispacher, LFSTranslator $translator, Player $player, Host $host) {
        parent::__construct($eventDispacher, $translator, $player, $host);

        // Filter
        $this->filter = new Filter($this);

        $filterValues = [
            [
                'title' => 'Notifications',
                'values' => [
                    ['key' => 'ntf-general', 'title' => 'General', 'state' => 1],
                ]
            ],
            [
                'title' => 'User interface',
                'values' => [
                    ['key' => 'time-bar', 'title' => 'Time bar', 'state' => 0],
                    ['key' => 'track-info', 'title' => 'Track info', 'state' => 0],
                ]
            ],
            [
                'title' => 'Localization',
                'values' => [
                    ['key' => 'loc-units', 'title' => 'Units', 'state' => 0],
                ]
            ],
        ];

        $this->filter->setValue($filterValues);
        $this->filter->setCallback([$this, 'onFilterChanged']);
        $this->addTab(new NotificationsGeneralTab('ntf-general', $this))->setPosition($this->getHeaderHeight() + 2, $this->filter->getWidth());
        $this->addTab(new LocalizationUnitsTab('loc-units', $this))->setPosition($this->getHeaderHeight() + 2, $this->filter->getWidth());
    }

    public function show() {
        parent::show();

        $this->filter->setPosition(($this->getTop() + $this->getHeaderHeight()), $this->getLeft());
        $this->filter->setHeight($this->getHeight());
        $this->filter->show($this->getCurrentId());

        $this->id_current = $this->filter->getEndId();

        $this->setActiveTab('ntf-general');

        return $this;
    }

    public function close() {
        unset($this->filter);

        foreach ($this->tabs as $tab) {
            unset($tab);
        }

        parent::close();
    }

    /**
     * 
     * @param ButtonClickedEvent $event
     * @return boolean
     */
    public function onButtonClicked(ButtonClickedEvent $event) {
        if (!parent::onButtonClicked($event)) {
            return false;
        }

        if (!$this->displayed) {
            return false;
        }

        $this->filter->eventClick($event->packet);

        if ($this->activeTab instanceof UITab) {
            $this->activeTab->eventClick($event->packet);
        }
    }

    public function onFilterChanged($filter) {
        $this->setActiveTab($filter['key']);
    }

}
