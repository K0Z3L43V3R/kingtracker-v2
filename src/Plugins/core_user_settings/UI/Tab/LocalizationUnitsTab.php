<?php

namespace Plugins\core_user_settings\UI\Tab;

use AppBundle\Entity\UI\Element\Label;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;

class LocalizationUnitsTab extends UITab {

    public function __construct($name, UIWindow $window) {
        parent::__construct($name, $window);

        $this->addElement(new Label($window, 'test', 'Test label', 4));
    }

}
