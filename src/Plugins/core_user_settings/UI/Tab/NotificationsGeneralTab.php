<?php

namespace Plugins\core_user_settings\UI\Tab;

use AppBundle\Entity\UI\Element\BtnSwitch;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Types\ButtonStyle;
use AppBundle\Types\NotifyTypes;

class NotificationsGeneralTab extends UITab {

    public function __construct($name, UIWindow $window) {
        parent::__construct($name, $window);

        $optionsYesNo = array(
            NotifyTypes::NO => ButtonStyle::COL_RED . 'No',
            NotifyTypes::YES => ButtonStyle::COL_GREEN . 'Yes',
        );

        $ntfHlvc = new BtnSwitch($window, 'ntf_hlvc', 'Display HLVC:', 4);
        $ntfHlvc->setValues($optionsYesNo)
                ->setOptionChangedCallback([$this, 'savePlayerOption']);

        $test = new BtnSwitch($window, 'test', 'Test:', 4);
        $test->setValues($optionsYesNo)
                ->setOptionChangedCallback([$this, 'savePlayerOption']);

        $this->addElement($ntfHlvc);
        $this->addElement($test);
    }

    public function savePlayerOption($name, $key, $value) {
        //dump([$name, $key, $value]);
    }

}
