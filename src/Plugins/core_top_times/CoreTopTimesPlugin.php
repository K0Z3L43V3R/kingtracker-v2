<?php

namespace Plugins\core_top_times;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;
use Plugins\core_top_times\UI\UITopTimesWindow;

class CoreTopTimesPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translator) {
        parent::__construct($plugin, $translator);

        $this->registerCommand('top', 'Displays TOP lap times', [$this, 'showLapTimes']);
    }

    public function showLapTimes(Player $player, Host $host) {
        $this->translator->setLocale($player->getLocale());
        
        $window = new UITopTimesWindow($this->eventDispacher, $this->translator, $player, $host);
        $window->setPosition(50)->setWidth(93)->setHeight(60)->setCenter();
        $window->setTitle($this->translator->trans('top.title'));

        $player->getUi()->showWindow($window);
    }

}
