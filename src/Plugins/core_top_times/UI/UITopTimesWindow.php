<?php

namespace Plugins\core_top_times\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\UI\Element\Filter;
use AppBundle\Entity\UI\Element\Table;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Button\ButtonTypedEvent;
use AppBundle\Helper\TimeHelper;
use AppBundle\Repository\LapRepository;
use AppBundle\Types\ButtonStyle;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI top times window
 */
class UITopTimesWindow extends UIWindow {

    /** @var LapRepository|EntityRepository */
    private $lapsRepository;

    public function __construct(EventDispatcherInterface $eventDispacher, LFSTranslator $translator, Player $player, Host $host) {
        parent::__construct($eventDispacher, $translator, $player, $host);

        $this->lapsRepository = $this->player->getStint()->getLapsRepository();

        // Filter
        $filter = new Filter($this, 12);

        $filterValues = [
            [
                'title' => $this->trans('top.filter.hlvc'),
                'values' => [
                    ['key' => 'clean', 'title' => $this->trans('top.filter.hlvc.clean'), 'state' => 0],
                ]
            ],
            [
                'title' => $this->trans('top.filter.controller'),
                'view' => 'H',
                'values' => [
                    ['key' => 'PF_W', 'title' => $this->trans('top.filter.controller.w'), 'state' => 0],
                    ['key' => 'PF_M', 'title' => $this->trans('top.filter.controller.m'), 'state' => 0],
                    ['key' => 'PF_KB', 'title' => $this->trans('top.filter.controller.kb'), 'state' => 0],
                    ['key' => 'PF_KBS', 'title' => $this->trans('top.filter.controller.kbs'), 'state' => 0],
                ]
            ],
            [
                'title' => $this->trans('top.filter.clutch'),
                'values' => [
                    ['key' => 'PF_AC', 'title' => $this->trans('top.filter.clutch.ac'), 'state' => 0],
                    ['key' => 'PF_BC', 'title' => $this->trans('top.filter.clutch.bc'), 'state' => 0],
                    ['key' => 'PF_AxisC', 'title' => $this->trans('top.filter.clutch.axis'), 'state' => 0],
                ]
            ],
            [
                'title' => $this->trans('top.filter.gearbox'),
                'values' => [
                    ['key' => 'PF_SQ', 'title' => $this->trans('top.filter.gearbox.sq'), 'state' => 0],
                    ['key' => 'PF_HS', 'title' => $this->trans('top.filter.gearbox.hs'), 'state' => 0],
                    ['key' => 'PF_AG', 'title' => $this->trans('top.filter.gearbox.ag'), 'state' => 0],
                ]
            ],
            [
                'title' => $this->trans('top.filter.setup'),
                'values' => [
                    ['key' => 'SF_ABS', 'title' => $this->trans('top.filter.setup.abs'), 'state' => 0],
                    ['key' => 'SF_TC', 'title' => $this->trans('top.filter.setup.tc'), 'state' => 0],
                ]
            ],
        ];

        $filter->setValue($filterValues);
        $filter->setCallback([$this, 'onFilterChanged']);
        $this->addElement($filter);

        // TOP times table
        $table = new Table($this);
        $table->setHeader([
            'number' => [
                ['width' => 6, 'title' => '#'.\AppBundle\Helper\SymbolHelper::toLFS('^H▼')]
            ],
            'time' => [
                ['width' => 10, 'title' => 'Time'.\AppBundle\Helper\SymbolHelper::getOrderAsc()]
            ],
            'driver' => [
                ['width' => 26, 'title' => 'Driver']
            ],
            'sectors' => [
                ['width' => 9, 'title' => 'SC1'],
                ['width' => 9, 'title' => 'SC2'],
                ['width' => 9, 'title' => 'SC3'],
                ['width' => 9, 'title' => 'SC4'],
            ],
        ]);
        $table->setColumns([
            'number' => [
                'style' => ButtonStyle::ISB_RIGHT + ButtonStyle::ISB_DARK, 'width' => 6
            ],
            'time' => [
                'style' => ButtonStyle::ISB_DARK, 'width' => 10
            ],
            'driver' => [
                'style' => ButtonStyle::ISB_LEFT + ButtonStyle::ISB_DARK, 'width' => 26
            ],
            'sectors' => [
                'style' => ButtonStyle::ISB_LEFT + ButtonStyle::ISB_DARK, 'width' => 36
            ],
        ]);

        $table->setNextPageCallBack([$this, 'pageChangedEvent']);
        $table->setPrevPageCallBack([$this, 'pageChangedEvent']);
        $table->setFirstPageCallBack([$this, 'pageChangedEvent']);
        $table->setLastPageCallBack([$this, 'pageChangedEvent']);

        $this->addElement($table);
    }

    public function show() {
        $filter = $this->findElement('filter');
        $table = $this->findElement('table');

        $filter->setPosition(($this->getTop() + $this->getHeaderHeight() + 1), $this->getLeft() + 1);
        $filter->setHeight($this->getHeight() - $table->getFooterHeight() - 2);

        $table->setPosition(($this->getTop() + $this->getHeaderHeight() + 1), $this->getLeft() + $filter->getWidth() + 2);
        $table->setWidth($this->getWidth() - $filter->getWidth() - 4);
        $table->setWidthFooter($this->getWidth());
        $table->setLeftFooterOffset(-1 * ($filter->getWidth() + 2));
        $table->setHeight($this->getHeight() - 1);
        $table->setLinesPerPage((int) (floor(($this->getHeight() - 10) / $table->getLineHeight()) - 1));
        $table->setCurrentPage(1);

        $laps = $this->lapsRepository->findBy([], ['time' => 'ASC'], $table->getLinesPerPage(), 0);

        $lapsCollection = [];
        $number = 0;
        foreach ($laps as $lap) {            
            $lapsCollection[] = [
                'number' => ++$number,
                'time' => ButtonStyle::COL_WHITE . TimeHelper::timeToString($lap->getTime()),
                'driver' => "{$lap->getPlayer()->getPName()} " . ButtonStyle::COL_NEUTRAL . "({$lap->getCar()})",
                'sectors' => ' '.implode('   ', $lap->getSectorsTimes()) . ' '
            ];
        }

        $table->setMaxpage((int) (40 / $table->getLinesPerPage()));

        $table->setValues($lapsCollection);

        return parent::show();
    }

    public function onFilterChanged($filter) {
        dump($filter);
    }

    public function pageChangedEvent($currentPage) {
        $table = $this->findElement('table');
        $offset = $table->getCurrentPage() == 1 ? 0 : ($table->getCurrentPage() - 1) * $table->getLinesPerPage();

        $laps = $this->lapsRepository->findBy([], ['time' => 'ASC'], $table->getLinesPerPage(), $offset);

        $lapsCollection = [];
        foreach ($laps as $lap) {
            $lapsCollection[] = [
                'player' => $lap->getPlayer()->getPName(),
                'time' => TimeHelper::timeToString($lap->getTime())
            ];
        }

        $table->setValues($lapsCollection);
    }

}
