<?php

namespace Plugins\core_help;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;
use Plugins\core_help\UI\UIHelpWindow;

class CoreHelpPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translator) {
        parent::__construct($plugin, $translator);

        $this->registerCommand('help', 'cmd.help', [$this, 'onHelpCommand']);
    }

    public function onHelpCommand(Player $player, Host $host) {
        $this->translator->setLocale($player->getLocale());

        $window = new UIHelpWindow($this->eventDispacher, $this->translator, $player, $host);
        $window->setPosition(50)->setWidth(80)->setHeight(52)->setCenter();
        $window->setTitle($this->translator->trans('help.title'));

        $player->getUi()->showWindow($window);
    }

}
