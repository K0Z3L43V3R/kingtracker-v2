<?php

namespace Plugins\core_help\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\UI\Element\Table;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Types\ButtonStyle;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI Help window
 */
class UIHelpWindow extends UIWindow {

    public function __construct(EventDispatcherInterface $eventDispacher, LFSTranslator $translator, Player $player, Host $host) {
        parent::__construct($eventDispacher, $translator, $player, $host);

        $this->headerHeight = 5;

        $table = new Table($this);

        $table->setColumns([
            'cmd' => [
                'style' => ButtonStyle::ISB_LEFT + ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY
            ]
        ]);

        $table->setNextPageCallBack([$this, 'pageChangedEvent']);
        $table->setPrevPageCallBack([$this, 'pageChangedEvent']);
        $table->setFirstPageCallBack([$this, 'pageChangedEvent']);
        $table->setLastPageCallBack([$this, 'pageChangedEvent']);

        $this->addElement($table);
    }

    public function show() {
        $table = $this->findElement('table');
        $table->setPosition($this->getTop() + $this->getHeaderHeight(), $this->getLeft());
        $table->setWidth($this->getWidth());
        $table->setHeight($this->getHeight());
        $table->setLinesPerPage((int) (floor($table->getHeight() / $table->getLineHeight()) - 1));
        $table->setCurrentPage(1);

        $offset = $table->getCurrentPage() == 1 ? 0 : ($table->getCurrentPage() - 1) * $table->getLinesPerPage();

        $commands = new ArrayCollection();
        foreach ($this->host->commandService->commands as $command) {
            $commands->add([
                'cmd' => ButtonStyle::COL_YELLOW . "!{$command->command} - " . ButtonStyle::COL_WHITE . $this->translator->trans($command->description)
            ]);
        }

        $table->setMaxpage((int) (ceil(count($commands) / $table->getLinesPerPage())));

        $table->setValues($commands->slice($offset, $table->getLinesPerPage()));

        return parent::show();
    }

    public function pageChangedEvent($currentPage) {
        $table = $this->findElement('table');
        $offset = $table->getCurrentPage() == 1 ? 0 : ($table->getCurrentPage() - 1) * $table->getLinesPerPage();

        $commands = new ArrayCollection();
        foreach ($this->host->commandService->commands as $command) {
            $commands->add([
                'cmd' => ButtonStyle::COL_YELLOW . "!{$command->command} - " . ButtonStyle::COL_WHITE . $this->translator->trans($command->description)
            ]);
        }
        
        $table->setValues($commands->slice($offset, $table->getLinesPerPage()));
    }

}
