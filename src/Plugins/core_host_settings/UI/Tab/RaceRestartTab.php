<?php

namespace Plugins\core_host_settings\UI\Tab;

use AppBundle\Entity\UI\Element\BtnInput;
use AppBundle\Entity\UI\Element\BtnSwitch;
use AppBundle\Entity\UI\Element\Label;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Types\ButtonStyle;

class RaceRestartTab extends UITab {

    public function __construct($name, UIWindow $window) {
        parent::__construct($name, $window);

        $optionsTrueFalse = [
            0 => ButtonStyle::COL_RED . $this->trans('no'),
            1 => ButtonStyle::COL_GREEN . $this->trans('yes'),
        ];

        $element = new BtnSwitch($window, 'race-auto-restart', $this->trans('host.race_auto_restart'), 4);
        $element->setValues($optionsTrueFalse, $this->host->getSetting('race-auto-restart')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnInput($window, 'race-auto-restart-after', $this->trans('host.race_auto_restart_after'), 4);
        $element->setValues(['race-auto-restart-after' => ButtonStyle::COL_WHITE . $this->host->getSetting('race-auto-restart-after')->getValue()])
                ->setOptionChangedCallback([$this, 'saveTypeInRestartAfter']);
        $this->addElement($element);

        $element = new BtnInput($window, 'rstmin', $this->trans('host.rstmin'), 4);
        $element->setValues(['race-auto-restart-after' => ButtonStyle::COL_WHITE . $this->host->getSetting('rstmin')->getValue()])
                ->setOptionChangedCallback([$this, 'saveTypeInRest']);
        $this->addElement($element);

        $element = new BtnInput($window, 'rstend', $this->trans('host.rstend'), 4);
        $element->setValues(['race-auto-restart-after' => ButtonStyle::COL_WHITE . $this->host->getSetting('rstend')->getValue()])
                ->setOptionChangedCallback([$this, 'saveTypeInRest']);
        $this->addElement($element);
    }

    public function saveOption($name, $key, $value) {
        $this->host->updateSetting($name, $key);
    }

    public function saveTypeInRestartAfter($name, $key, $value) {
        $val = intval($value);
        $val = $val < 10 ? 10 : $val;

        $this->host->updateSetting($name, $val);

        return $val;
    }

    public function saveTypeInRest($name, $key, $value) {
        $val = intval($value);
        $val = $val < 0 ? 0 : $val;

        $this->host->updateSetting($name, $val);

        return $val;
    }

}
