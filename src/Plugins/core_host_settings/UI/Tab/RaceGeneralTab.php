<?php

namespace Plugins\core_host_settings\UI\Tab;

use AppBundle\Entity\UI\Element\BtnSwitch;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Types\ButtonStyle;
use AppBundle\Types\NotifyTypes;

class RaceGeneralTab extends UITab {

    public function __construct($name, UIWindow $window) {
        parent::__construct($name, $window);

        $optionsYesNo = [
            'no' => ButtonStyle::COL_RED . $this->trans('no'),
            'yes' => ButtonStyle::COL_GREEN . $this->trans('yes'),
        ];

        $optionsTrueFalse = [
            0 => ButtonStyle::COL_RED . $this->trans('no'),
            1 => ButtonStyle::COL_GREEN . $this->trans('yes'),
        ];

        $optionsWrongWay = [
            'no' => ButtonStyle::COL_RED . $this->trans('no'),
            'kick' => ButtonStyle::COL_GREEN . $this->trans('kick'),
            'ban' => ButtonStyle::COL_GREEN . $this->trans('ban'),
            'spec' => ButtonStyle::COL_GREEN . $this->trans('spec')
        ];

        $element = new BtnSwitch($window, 'canreset', $this->trans('host.allow_car_reset'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('canreset')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'vote', $this->trans('host.allow_kick_ban'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('vote')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'select', $this->trans('host.allow_select_track'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('select')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'midrace', $this->trans('host.allow_midrace_join'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('midrace')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'fcv', $this->trans('host.force_cockpit_view'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('fcv')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'cruise', $this->trans('host.allow_wrong_way'), 4);
        $element->setValues($optionsYesNo, $this->host->getSetting('cruise')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'autokick', $this->trans('host.wrong_way_autokick'), 4);
        $element->setValues($optionsWrongWay, $this->host->getSetting('autokick')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);

        $element = new BtnSwitch($window, 'hlvc-ignore-concrete', $this->trans('host.hlvc_ignore_concrete'), 4);
        $element->setValues($optionsTrueFalse, $this->host->getSetting('hlvc-ignore-concrete')->getValue())
                ->setOptionChangedCallback([$this, 'saveOption']);
        $this->addElement($element);
    }

    public function saveOption($name, $key, $value) {
        $this->host->updateSetting($name, $key);
    }

}
