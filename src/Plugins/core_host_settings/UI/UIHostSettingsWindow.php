<?php

namespace Plugins\core_host_settings\UI;

use AppBundle\Entity\Host;
use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\UI\Element\Filter;
use AppBundle\Entity\UI\UITab;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Button\ButtonTypedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI host settings window
 */
class UIHostSettingsWindow extends UIWindow {

    /** @var Filter */
    private $filter;
    
    public function __construct(EventDispatcherInterface $eventDispacher, LFSTranslator $translator, $player, Host $host) {
        parent::__construct($eventDispacher, $translator, $player, $host);

        // Filter
        $this->filter = new Filter($this);

        $filterValues = [
            [
                'title' => $this->trans('host.tab.category.race'),
                'values' => [
                    ['key' => 'race-general', 'title' => $this->trans('host.tab.race.general.title'), 'state' => 1],
                    ['key' => 'race-restart', 'title' => $this->trans('host.tab.race.restart.title'), 'state' => 0],
                ]
            ],
        ];

        $this->filter->setValue($filterValues);
        $this->filter->setCallback([$this, 'onFilterChanged']);
        $this->addTab(new Tab\RaceGeneralTab('race-general', $this))->setPosition($this->getHeaderHeight() + 2, $this->filter->getWidth());
        $this->addTab(new Tab\RaceRestartTab('race-restart', $this))->setPosition($this->getHeaderHeight() + 2, $this->filter->getWidth());
    }

    public function show() {
        parent::show();

        $this->filter->setPosition(($this->getTop() + $this->getHeaderHeight()), $this->getLeft());
        $this->filter->show($this->getCurrentId());
        $this->filter->setHeight($this->getHeight());

        $this->id_current = $this->filter->getEndId();

        $this->setActiveTab('race-general');

        return $this;
    }

    public function close() {
        unset($this->filter);

        foreach ($this->tabs as $tab) {
            unset($tab);
        }

        parent::close();
    }

    /**
     * 
     * @param ButtonClickedEvent $event
     * @return boolean
     */
    public function onButtonClicked(ButtonClickedEvent $event) {
        if (!parent::onButtonClicked($event)) {
            return false;
        }

        if (!$this->displayed) {
            return false;
        }

        $this->filter->eventClick($event->packet);

        if ($this->activeTab instanceof UITab) {
            $this->activeTab->eventClick($event->packet);
        }
    }

    /**
     * 
     * @param ButtonTypedEvent $event
     * @return boolean
     */
    public function onButtonTyped(ButtonTypedEvent $event) {
        if (!parent::onButtonTyped($event)) {
            return false;
        }

        if (!$this->displayed) {
            return false;
        }

        $this->filter->eventType($event->packet);

        if ($this->activeTab instanceof UITab) {
            $this->activeTab->eventType($event->packet); 
        }
    }

    public function onFilterChanged($filter) {
        $this->setActiveTab($filter['key']);
    }

}
