<?php

namespace Plugins\core_host_settings;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;
use Plugins\core_host_settings\UI\UIHostSettingsWindow;

class CoreHostSettingsPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translator) {
        parent::__construct($plugin, $translator);

        $this->registerCommand('host', 'Displays host settings', [$this, 'showHostSettings']);
    }

    public function showHostSettings(Player $player, Host $host) {
        $this->translator->setLocale($player->getLocale());
        
        $window = new UIHostSettingsWindow($this->eventDispacher, $this->translator, $player, $host);
        $window->setPosition(50)->setWidth(80)->setHeight(60)->setCenter();
        $window->setTitle($this->translator->trans('host.settings.title'));

        $player->getUi()->showWindow($window);
    }

}
