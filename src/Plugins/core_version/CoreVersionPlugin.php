<?php

namespace Plugins\core_version;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;

class CoreVersionPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translation) {
        parent::__construct($plugin, $translation);
        
        $this->registerCommand('ver', 'Displays current version and changelog', [$this, 'onVersionCommand']);
    }
    
    public function onVersionCommand(Player $player, Host $host){
        print_r([
            'Version command', $host->getName(), $player->getUName()
        ]);
    }

}
