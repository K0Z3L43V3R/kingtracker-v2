<?php

namespace Plugins\msg_lap_saved;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Plugin;
use AppBundle\Event\Player\PlayerLapSavedEvent;
use AppBundle\Helper\TimeHelper;

class MsgLapSavedPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translation) {
        parent::__construct($plugin, $translation);

        $this->eventDispacher->addListener(PlayerLapSavedEvent::NAME, [$this, 'onPlayerLapSaved']);
    }

    /**
     * Event called on player connected
     * 
     * @param PlayerLapSavedEvent $event
     */
    public function onPlayerLapSaved(PlayerLapSavedEvent $event) {
        $lap = $event->lap;
        $player = $lap->getPlayer();

        $this->translator->setLocale($player->getLocale());

        $lapTime = TimeHelper::timeToString($lap->getTime());
        $msg = $event->isUpdated ? 'Lap time updated:' : 'Lap time saved:';

        dump("{$msg} {$player->getUName()} - {$lapTime}");
    }

}
