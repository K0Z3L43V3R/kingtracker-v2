<?php

namespace Plugins\core_host_commands;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Plugin;
use AppBundle\Entity\Timer;

class CoreHostCommandsPlugin extends Plugin {
    
    public function __construct(Plugin $plugin, LFSTranslator $translation) {
        parent::__construct($plugin, $translation);

        $this->registerCommand('laps', $translation->trans('cmd.laps'), [$this, 'onLapsCommand'], [
            'laps' => ['title' => $translation->trans('cmd.laps.arg'), 'type' => 'int', 'required' => true]
        ]);

        $this->registerCommand('restart', $translation->trans('cmd.restart'), [$this, 'onRestartCommand'], [
            'delay' => ['title' => $translation->trans('cmd.restart.arg'), 'type' => 'int']
        ]);
    }

    /**
     * Set laps 
     * 
     * @param Player $player
     * @param Host $host
     * @param array $arguments
     */
    public function onLapsCommand(Player $player, Host $host, $arguments) {
        $host->sendCommand("/laps {$arguments['laps']}");
    }

    /**
     * Restart race
     * @param Player $player
     * @param Host $host
     * @param array $arguments
     */
    public function onRestartCommand(Player $player, Host $host, $arguments) {
        $this->translator->setLocale($player->getLocale());
        
        $delay = isset($arguments['delay']) ? ($arguments['delay'] > 3 ? $arguments['delay'] : Timer::RESTART_DELAY) : 10;

        $host->addTimer(new Timer('restart', ($delay - Timer::RESTART_DELAY), [$host, 'restart']));

        foreach ($host->getPlayerService()->players as $player) {
            if ($player->getUi() !== null) {
                $this->translator->setLocale($player->getLocale());
                $player->getUi()->showRaceControl($this->translator->trans('rc.restart'), $delay);
            }
        }
    }

}
