<?php

namespace Plugins\core_time_bar;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Plugin;
use AppBundle\Event\Player\PlayerJoinedTrackEvent;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\Player\PlayerSpectatedEvent;
use AppBundle\Event\Player\PlayerSplitEvent;
use Plugins\core_time_bar\UI\UITimeBar;

class CoreTimeBarPlugin extends Plugin {

    public function __construct(Plugin $plugin, LFSTranslator $translator) {
        parent::__construct($plugin, $translator);

        $this->eventDispacher->addListener(PlayerJoinedTrackEvent::NAME, [$this, 'onPlayerJoinedTrack']);
        $this->eventDispacher->addListener(PlayerSpectatedEvent::NAME, [$this, 'onPlayerSpectated']);
        $this->eventDispacher->addListener(PlayerSplitEvent::NAME, [$this, 'onSplitEvent']);
        $this->eventDispacher->addListener(PlayerLapEvent::NAME, [$this, 'onLapEvent']);
    }

    public function onPlayerJoinedTrack(PlayerJoinedTrackEvent $event) {
        $player = $event->player;
        $this->translator->setLocale($player->getLocale());

        $timeBar = new UITimeBar($this->eventDispacher, $player, $player->getHostConnection()->getHost());
        $player->getUi()->addPermanentUI('time-bar', $timeBar);
    }

    public function onPlayerSpectated(PlayerSpectatedEvent $event) {
        $player = $event->player;

        $player->getUi()->removePermanentUI('time-bar');
    }

    public function onSplitEvent(PlayerSplitEvent $event) {
        if ($event->packet->STime == 3600000) {
            return;
        }

        $player = $event->player;

        if (($timeBar = $player->getUi()->getPermanentUI('time-bar'))) {
            $timeBar->onSplitEvent($event);
        }
    }

    public function onLapEvent(PlayerLapEvent $event) {
        if ($event->packet->LTime == 3600000) {
            return;
        }

        $player = $event->player;

        if (($timeBar = $player->getUi()->getPermanentUI('time-bar'))) {
            $timeBar->onLapEvent($event);
        }
    }

}
