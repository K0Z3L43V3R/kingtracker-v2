<?php

namespace Plugins\core_time_bar\UI;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Timer;
use AppBundle\Entity\UI\UI;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\Player\PlayerSplitEvent;
use AppBundle\Helper\TimeHelper;
use AppBundle\Packet\IS_BTN;
use AppBundle\Service\TimerService;
use AppBundle\Types\ButtonStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI RaceControl
 */
class UITimeBar extends UI {

    /** @var TimerService */
    private $timerService;

    /** @var array */
    private $buttons;

    /**
     * 
     * @param EventDispatcherInterface $eventDispacher
     * @param Player $player
     * @param Host $host
     * @param int $id_start
     * @param int $id_end
     */
    public function __construct(EventDispatcherInterface $eventDispacher, Player $player, Host $host, $id_start = 1, $id_end = 99) {
        parent::__construct($eventDispacher, $player, $host, $player->getTranslator(), $id_start);

        $this->timerService = new TimerService($eventDispacher);

        $this->id_start = $id_start;
        $this->id_end = $id_end;

        $this->width = 87;
        $this->height = 6;
        $this->top = 0;
        $this->left = (int) ((200 - $this->width) / 2);

        $this->buttons = array(
            'event' => array('id' => 0, 'value' => 'L1', 'width' => 10, 'color' => ButtonStyle::COL_WHITE, 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'split' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => ButtonStyle::COL_YELLOW, 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'split_diff' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => '', 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'sector' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => ButtonStyle::COL_YELLOW, 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'sector_diff' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => '', 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'lap' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => ButtonStyle::COL_LIGHT_BLUE, 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
            'speed' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => ButtonStyle::COL_LIGHT_BLUE, 'style' => ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY),
        );
    }

    public function show() {
        $this->displayed = true;

        // Set initial data
        $stint = $this->player->getStint();
        if ($stint) {
            $this->buttons['event']['value'] = "L{$stint->currentLap}";
            if ($stint->bestLap > 0) {
                $this->buttons['lap']['value'] = TimeHelper::timeToString($stint->bestLap);
            }
        }

        $button = new IS_BTN();

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';
        $this->sendQueued($button);

        $left = 1;
        foreach ($this->buttons as $buttonID => $btn) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + $left;
            $button->T = $this->top;
            $button->H = 5;
            $button->W = $btn['width'];

            $button->BStyle = $btn['style'];
            $button->Text = $btn['value'];

            $this->buttons[$buttonID]['id'] = $button->ReqI;
            $this->sendQueued($button);

            $left += $button->W;
        }

        $this->id_end = $button->ClickID;

        return $this;
    }

    public function redraw($faded = false) {
        if (!$this->displayed)
            return;

        $button = new IS_BTN();

        foreach ($this->buttons as $key => $btn) {
            $button->ReqI = $btn['id'];
            $button->ClickID = $btn['id'];
            $button->Text = (!$faded ? $btn['color'] : '') . $btn['value'];
            $this->sendQueued($button);
        }
    }

    public function tick() {
        if ($this->timerService) {
            $this->timerService->tick();
        }
    }

    public function onSplitEvent(PlayerSplitEvent $event) {
        $stint = $this->player->getStint();
        $split = $event->packet;

        $this->buttons['event']['value'] = "L{$stint->currentLap}S{$split->Split}";
        $this->buttons['split']['value'] = TimeHelper::timeToString($split->STime);
        $this->buttons['sector']['value'] = TimeHelper::timeToString($stint->sectors[$split->Split]);

        $negative = false;
        $this->buttons['split_diff']['value'] = TimeHelper::diffToString($stint->split_diff, $negative);
        $this->buttons['split_diff']['color'] = !$negative && $stint->split_diff != 0 ? ButtonStyle::COL_RED : ButtonStyle::COL_GREEN;

        $negative = false;
        $this->buttons['sector_diff']['value'] = TimeHelper::diffToString($stint->sector_diff, $negative);
        $this->buttons['sector_diff']['color'] = !$negative && $stint->sector_diff != 0 ? ButtonStyle::COL_RED : ButtonStyle::COL_GREEN;

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

    public function onLapEvent(PlayerLapEvent $event) {
        $stint = $this->player->getStint();
        $lap = $event->packet;

        $this->buttons['event']['value'] = "L{$stint->currentLap}";
        $this->buttons['split']['value'] = TimeHelper::timeToString($lap->LTime);
        $this->buttons['sector']['value'] = TimeHelper::timeToString($stint->prev_sectors[$stint->finish_sector]);
        $this->buttons['lap']['value'] = TimeHelper::timeToString($stint->bestLap);

        $negative = false;
        $this->buttons['split_diff']['value'] = TimeHelper::diffToString($stint->lap_diff, $negative);
        $this->buttons['split_diff']['color'] = !$negative && $stint->lap_diff != 0 ? ButtonStyle::COL_RED : ButtonStyle::COL_GREEN;

        $negative = false;
        $this->buttons['sector_diff']['value'] = TimeHelper::diffToString($stint->sector_diff, $negative);
        $this->buttons['sector_diff']['color'] = !$negative && $stint->sector_diff != 0 ? ButtonStyle::COL_RED : ButtonStyle::COL_GREEN;

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

}
