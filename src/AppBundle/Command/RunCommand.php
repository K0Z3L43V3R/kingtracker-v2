<?php

namespace AppBundle\Command;

use AppBundle\Component\LFSTranslator;
use AppBundle\Console\CustomStyle;
use AppBundle\Entity\Host;
use AppBundle\Event\LogEvent;
use AppBundle\Packet\IS_TINY;
use AppBundle\Types\ConnectionStatus;
use DirectoryIterator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\Loader\YamlFileLoader;

class RunCommand extends ContainerAwareCommand {

    /** @var boolean */
    private $isRunning = true;

    /** @var CustomStyle */
    private $io;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var array Active plugins instancies */
    private $plugins;

    /** @var LFSTranslator */
    private $translator;

    protected function configure() {
        $this->setName('app:run');
        $this->setDescription('Start KingTracker');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        ini_set('max_execution_time', 0);
        gc_enable();

        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Translator
        $this->translator = new LFSTranslator('en_US');
        $this->translator->setFallbackLocales(['en_US']);

        $this->translator->addLoader('yaml', new YamlFileLoader());

        // Load translations
        $langsDir = $this->getContainer()->getParameter('langs_dir');
        $dirIterator = new DirectoryIterator($langsDir);

        if ($dirIterator) {
            foreach ($dirIterator as $file) {
                if ($file->isFile() && $file->getExtension() == 'yml') {
                    $localeName = $file->getBasename('.yml');

                    // TODO: check if locale is valid
                    $this->translator->addResource('yaml', $file->getPathname(), $localeName);
                }
            }
        }

        $this->input = $input;
        $this->output = $output;
        $this->io = new CustomStyle($input, $output);
        $eventDispatcher = $this->getContainer()->get('event_dispatcher');
        $version = $this->getContainer()->getParameter('version_string');
        $hosts = $this->getContainer()->get('app.host.repository')->findBy(['active' => 1]);
        $this->getContainer()->get('app.host.connection.repository')->cleanUp();

        $output->writeln('======================================================================');
        $output->writeln('   KingTracker ' . $version);
        $output->writeln('======================================================================');

        // Log event listener
        $eventDispatcher->addListener(LogEvent::NAME, [$this, 'triggerLog']);

        // Set all hosts as disconnected
        $this->getContainer()->get('app.host.repository')->setAllDisconnected();

        // Hosts initialization
        foreach ($hosts as $host) {
            $this->initHost($host);
        }

        // Plugins initialization
        $plugins = $this->getContainer()->get('app.plugin.repository')->findBy(['active' => 1]);
        foreach ($plugins as $plugin) {
            $pluginClass = "\\Plugins\\{$plugin->getDir()}\\{$plugin->getClass()}";

            if (class_exists($pluginClass)) {
                $this->plugins[$plugin->getClass()] = new $pluginClass($plugin, $this->translator);
            }
        }

        // Run loop
        $tLastTick = microtime(true);
        $tLastReconnect = time();
        $tLastGC = time();

        while ($this->isRunning === true) {
            // Relaod hosts
            if ((time() - $tLastReconnect) >= 30) {
                $this->reloadHost($hosts);
                $tLastReconnect = time();
            }

            // Garbage collector
            if ((time() - $tLastGC) >= 30) {
                $tLastGC = time();

                gc_collect_cycles();
                //echo (memory_get_usage(false) / 1024) . PHP_EOL;
            }

            // Hosts tick
            foreach ($hosts as $host) {
                $host->tick();
            }

            $execTime = (microtime(true) - $tLastTick) * 1000000;
            $delayTime = 50 - $execTime;
            $delayTime = $delayTime < 0 ? 0 : $delayTime;

            usleep($delayTime);
            $tLastTick = microtime(true);
        }
    }

    /**
     * @param Host[] $hosts
     */
    private function reloadHost(&$oldHosts) {
        $hosts = $this->getContainer()->get('app.host.repository')->findBy(['active' => 1]);

        // Cleanup disconnected host
        foreach ($oldHosts as $key => $oHost) {
            if ($oHost->getStatus() === ConnectionStatus::DISCONNECTED) {
                $this->io->log("Cleanup {$oHost->getName()}");
                $oHost->destroy();
                unset($oHost);
                unset($oldHosts[$key]);
            }
        }

        // Disconnect inactive hosts
        foreach ($oldHosts as $oHost) {
            $found = false;
            foreach ($hosts as $nHost) {
                if ($oHost->getId() === $nHost->getId()) {
                    $found = true;
                }
            }

            if (!$found) {
                $this->disconnectHost($oHost);
            }
        }

        // Find new host
        foreach ($hosts as $nHost) {
            $found = false;
            foreach ($oldHosts as $oHost) {
                if ($oHost->getId() === $nHost->getId()) {
                    $found = true;
                }
            }

            // New host
            if (!$found) {
                $this->io->log("Adding host {$nHost->getname()}");
                $this->initHost($nHost);
                $oldHosts[] = $nHost;
            }
        }
    }

    /**
     * Initialize host
     * 
     * @param Host $host
     */
    private function initHost(Host &$host) {
        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $this->io->log("Connecting to {$host->getName()}  ...");
        }

        // Set services
        $host->setTranslator($this->translator);
        $host->setInsim($this->getContainer()->get('app.insim.service'));
        $host->setHostService($this->getContainer()->get('app.host.service'));
        $host->setPlayerService($this->getContainer()->get('app.player.service'));
        $host->setCommandService($this->getContainer()->get('app.command.service'));
        $host->setTimerService($this->getContainer()->get('app.timer.service'));

        // Connect hosts
        $host->insim->connect();
    }

    /**
     * Disconnect from host
     * 
     * @param Host $host
     */
    private function disconnectHost(Host &$host) {
        $host->insim->disconnect();
        $host->setStatus(ConnectionStatus::DISCONNECTED);

        $this->io->log("Disconneting {$host->getName()}");
    }

    /**
     * Output log
     * 
     * @param LogEvent $event
     */
    public function triggerLog(LogEvent $event) {
        if ($this->output->getVerbosity() >= $event->getVerbosity()) {
            $this->io->log($event->getMessage());
        }
    }

}
