<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_PFL;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player flags changed
 */
class PlayerFlagsChangedEvent extends Event {

    const NAME = 'player.flagschange';

    /** @var Host */
    public $host;

    /** @var IS_PFL */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_PFL $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
