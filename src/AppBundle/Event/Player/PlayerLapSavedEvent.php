<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Lap;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player lap time was saved
 */
class PlayerLapSavedEvent extends Event {

    const NAME = 'player.lap.saved';

    /** @var Lap */
    public $lap;

    /** @var bool Lap time was updated */
    public $isUpdated;

    public function __construct(Lap $lap, bool $isUpdated) {
        $this->lap = $lap;
        $this->isUpdated = $isUpdated;
    }

}
