<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_OBH;
use Symfony\Component\EventDispatcher\Event;

/**
 * Reports contact between two cars
 */
class PlayerObjectContactEvent extends Event {

    const NAME = 'player.contact.object';

    /** @var Host */
    public $host;

    /** @var IS_OBH */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_OBH $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
