<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player presses SHIFT+I
 */
class PlayerShowSettingsEvent extends Event {

    const NAME = 'player.show_settings';

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    public function __construct(Host $host, Player $player) {
        $this->host = $host;
        $this->player = $player;
    }

}
