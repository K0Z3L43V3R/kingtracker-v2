<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_NPL;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player joins track
 */
class PlayerJoinedTrackEvent extends Event {

    const NAME = 'player.joinedtrack';

    /** @var Host */
    public $host;

    /** @var IS_NPL */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_NPL $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
