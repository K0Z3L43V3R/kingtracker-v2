<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_RES;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player finishes race / qualify
 */
class PlayerResultEvent extends Event {

    const NAME = 'player.result';

    /** @var Host */
    public $host;

    /** @var IS_RES */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_RES $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
