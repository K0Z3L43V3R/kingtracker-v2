<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_NCN;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player joins server
 */
class PlayerConnectedEvent extends Event {

    const NAME = 'player.connected';

    /** @var Host */
    public $host;

    /** @var IS_NCN */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_NCN $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
