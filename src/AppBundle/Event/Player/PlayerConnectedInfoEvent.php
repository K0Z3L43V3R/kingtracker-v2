<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_NCI;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player connection info received
 */
class PlayerConnectedInfoEvent extends Event {

    const NAME = 'player.connected.info';

    /** @var Host */
    public $host;

    /** @var IS_NCI */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_NCI $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
