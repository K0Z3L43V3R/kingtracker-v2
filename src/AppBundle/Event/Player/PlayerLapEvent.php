<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_LAP;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player crosses finish line
 */
class PlayerLapEvent extends Event {

    const NAME = 'player.lap';

    /** @var Host */
    public $host;

    /** @var IS_LAP */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_LAP $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
