<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_CON;
use Symfony\Component\EventDispatcher\Event;

/**
 * Reports contact between two cars
 */
class PlayerCarContactEvent extends Event {

    const NAME = 'player.contact.car';

    /** @var Host */
    public $host;

    /** @var IS_CON */
    public $packet;

    /** @var Player */
    public $player;

    /** @var Player */
    public $playerB;

    public function __construct(Host $host, IS_CON $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
