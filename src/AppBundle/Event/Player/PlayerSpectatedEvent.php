<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_PLL;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player spectate
 */
class PlayerSpectatedEvent extends Event {

    const NAME = 'player.spectated';

    /** @var Host */
    public $host;

    /** @var IS_PLL */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_PLL $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
