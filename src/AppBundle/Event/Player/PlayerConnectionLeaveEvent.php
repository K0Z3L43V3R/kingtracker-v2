<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_CNL;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player leaves server
 */
class PlayerConnectionLeaveEvent extends Event {

    const NAME = 'player.leave';

    /** @var Host */
    public $host;

    /** @var IS_CNL */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_CNL $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
