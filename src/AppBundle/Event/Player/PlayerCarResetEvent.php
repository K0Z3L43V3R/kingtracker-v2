<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_CRS;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player resets car
 */
class PlayerCarResetEvent extends Event {

    const NAME = 'player.car-reset';

    /** @var Host */
    public $host;

    /** @var IS_CRS */
    public $packet;

    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_CRS $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
