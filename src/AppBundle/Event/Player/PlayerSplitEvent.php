<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_SPX;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player crosses split
 */
class PlayerSplitEvent extends Event {

    const NAME = 'player.split';

    /** @var Host */
    public $host;

    /** @var IS_SPX */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_SPX $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
