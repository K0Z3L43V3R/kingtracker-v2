<?php

namespace AppBundle\Event\Player;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_MCI;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when MCI was received
 */
class MCIReceivedEvent extends Event {

    const NAME = 'player.mci';

    /** @var Host */
    public $host;

    /** @var IS_MCI */
    public $packet;
    
    public function __construct(Host $host, IS_MCI $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
