<?php

namespace AppBundle\Event;

use AppBundle\Packet\Packet;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when packet was send
 */
class SendPacketEvent extends Event {

    const NAME = 'packet.send';

    /** @var Packet */
    public $packet;

    /** @var int */
    public $hostId;

    /**
     * @param Packet $packet
     * @param int $UCID
     * @param int $hostId
     */
    public function __construct(Packet $packet, $hostId) {
        $this->packet = $packet;
        $this->hostId = $hostId;
    }

}
