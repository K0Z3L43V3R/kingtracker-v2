<?php

namespace AppBundle\Event\Command;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when command was registered
 */
class CommandRegisteredEvent extends Event {

    const NAME = 'command.registered';

    /** @var string */
    public $command;

    /** @var string */
    public $description;

    /** @var array */
    public $arguments;

    /** @var callable */
    public $callback;

    /**
     * @param string $command
     * @param string $description
     * @param callable $callback
     * @param array $arguments
     */
    public function __construct($command, $description, callable $callback, array $arguments = []) {
        $this->command = $command;
        $this->description = $description;
        $this->arguments = $arguments;
        $this->callback = $callback;
    }

}
