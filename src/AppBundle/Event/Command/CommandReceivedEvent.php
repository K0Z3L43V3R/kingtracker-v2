<?php

namespace AppBundle\Event\Command;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_MSO;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when user command was received
 */
class CommandReceivedEvent extends Event {

    const NAME = 'command.received';

    /** @var Host */
    public $host;

    /** @var IS_MSO */
    public $packet;
    
    /** @var Player|null */
    public $player;

    public function __construct(Host $host, IS_MSO $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
