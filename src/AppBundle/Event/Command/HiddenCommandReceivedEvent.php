<?php

namespace AppBundle\Event\Command;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_III;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when hidden user command was received
 */
class HiddenCommandReceivedEvent extends Event {

    const NAME = 'command.hidden.received';

    /** @var Host */
    public $host;

    /** @var IS_III */
    public $packet;
    
    /** @var Player|null */
    public $player;

    public function __construct(Host $host, IS_III $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
