<?php

namespace AppBundle\Event;

use AppBundle\Types\LogType;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\Event;

class LogEvent extends Event {

    const NAME = 'console.log';

    /** @var string */
    private $message;

    /** @var int */
    private $verbosity;

    /** @var string */
    private $type;

    /**
     * @param string $message
     */
    public function __construct($message, $type = LogType::GENERAL, $verbosity = OutputInterface::VERBOSITY_NORMAL) {
        $this->message = $message;
        $this->verbosity = $verbosity;
        $this->type = $type;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getVerbosity() {
        return $this->verbosity;
    }

    public function getType() {
        return $this->type;
    }

}
