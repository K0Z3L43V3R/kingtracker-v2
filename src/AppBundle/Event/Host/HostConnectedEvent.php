<?php

namespace AppBundle\Event\Host;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_VER;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when insim connection with host is made
 */
class HostConnectedEvent extends Event {

    const NAME = 'host.connected';

    /** @var Host */
    public $host;

    /** @var IS_VER */
    public $packet;

    /**
     * @param string $packet
     */
    public function __construct(Host $host, IS_VER $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
