<?php

namespace AppBundle\Event\Host;

use AppBundle\Entity\Host;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when host disconnected
 */
class HostDisconnectedEvent extends Event {

    const NAME = 'host.disconnected';

    /** @var Host */
    public $host;

    /**
     * @param string $packet
     */
    public function __construct(Host $host) {
        $this->host = $host;
    }

}
