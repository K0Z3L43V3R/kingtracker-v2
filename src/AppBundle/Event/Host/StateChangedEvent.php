<?php

namespace AppBundle\Event\Host;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_STA;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when IS_STA packet was received
 */
class StateChangedEvent extends Event {

    const NAME = 'host.state.changed';

    /** @var IS_STA */
    public $packet;

    /** @var Host */
    public $host;

    /**
     * @param Host $host
     * @param IS_STA $packet
     */
    public function __construct(Host $host, IS_STA $packet) {
        $this->packet = $packet;
        $this->host = $host;
    }

}
