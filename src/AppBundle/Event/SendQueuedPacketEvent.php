<?php

namespace AppBundle\Event;

use AppBundle\Packet\Packet;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when packet was queued to send
 */
class SendQueuedPacketEvent extends Event {

    const NAME = 'packet.queued';

    /** @var Packet */
    public $packet;

    /** @var int */
    public $UCID;

    /** @var int */
    public $hostId;

    /**
     * @param Packet $packet
     * @param int $UCID
     * @param int $hostId
     */
    public function __construct(Packet $packet, $UCID, $hostId) {
        $this->packet = $packet;
        $this->UCID = $UCID;
        $this->hostId = $hostId;
    }

}
