<?php

namespace AppBundle\Event\HLVC;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

/**
 * HLVC track bounds event
 */
class HLVCBoundsEvent extends Event {

    const NAME = 'hlvc.bounds';

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    public function __construct(Host $host, Player $player) {
        $this->host = $host;
        $this->player = $player;
    }

}
