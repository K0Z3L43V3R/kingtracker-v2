<?php

namespace AppBundle\Event\HLVC;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

/**
 * HLVC ground event
 */
class HLVCGroundEvent extends Event {

    const NAME = 'hlvc.ground';

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    public function __construct(Host $host, Player $player) {
        $this->host = $host;
        $this->player = $player;
    }

}
