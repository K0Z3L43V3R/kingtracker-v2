<?php

namespace AppBundle\Event\HLVC;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

/**
 * HLVC pit speeding event
 */
class HLVCPitSpeedingEvent extends Event {

    const NAME = 'hlvc.pit-speeding';

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    public function __construct(Host $host, Player $player) {
        $this->host = $host;
        $this->player = $player;
    }

}
