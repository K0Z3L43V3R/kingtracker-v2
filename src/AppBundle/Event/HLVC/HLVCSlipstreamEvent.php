<?php

namespace AppBundle\Event\HLVC;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player drafted other car
 */
class HLVCSlipstreamEvent extends Event {

    const NAME = 'hlvc.slipstream';

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    public function __construct(Host $host, Player $player) {
        $this->host = $host;
        $this->player = $player;
    }

}
