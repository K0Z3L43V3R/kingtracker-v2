<?php

namespace AppBundle\Event\Packet;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_AXI;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when IS_AXI packet was received
 */
class AXIReceivedEvent extends Event {

    const NAME = 'packet.axi';

    /** @var IS_AXI */
    public $packet;

    /** @var Host */
    public $host;

    /**
     * @param Host $host
     * @param IS_AXI $packet
     */
    public function __construct(Host $host, IS_AXI $packet) {
        $this->packet = $packet;
        $this->host = $host;
    }

}
