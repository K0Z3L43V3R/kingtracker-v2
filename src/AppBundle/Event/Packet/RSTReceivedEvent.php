<?php

namespace AppBundle\Event\Packet;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_RST;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when IS_RST packet was received
 */
class RSTReceivedEvent extends Event {

    const NAME = 'packet.rst';

    /** @var IS_RST */
    public $packet;

    /** @var Host */
    public $host;

    /**
     * @param Host $host
     * @param IS_RST $packet
     */
    public function __construct(Host $host, IS_RST $packet) {
        $this->packet = $packet;
        $this->host = $host;
    }

}
