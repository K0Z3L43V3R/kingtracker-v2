<?php

namespace AppBundle\Event\Packet;

use AppBundle\Entity\Host;
use AppBundle\Packet\IS_HLV;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when IS_HLV packet was received
 */
class HLVReceivedEvent extends Event {

    const NAME = 'packet.hlv';

    /** @var IS_HLV */
    public $packet;

    /** @var Host */
    public $host;

    /**
     * @param Host $host
     * @param IS_HLV $packet
     */
    public function __construct(Host $host, IS_HLV $packet) {
        $this->packet = $packet;
        $this->host = $host;
    }

}
