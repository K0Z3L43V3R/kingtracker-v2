<?php

namespace AppBundle\Event\Button;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_BFN;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player presses SHIFT+I / SHIFT+B
 */
class ButtonClearRequestEvent extends Event {

    const NAME = 'button.clear_request';

    /** @var Host */
    public $host;

    /** @var IS_BFN */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_BFN $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
