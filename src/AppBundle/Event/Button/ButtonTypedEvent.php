<?php

namespace AppBundle\Event\Button;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_BTT;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player types in button
 */
class ButtonTypedEvent extends Event {

    const NAME = 'button.typed';

    /** @var Host */
    public $host;

    /** @var IS_BTT */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_BTT $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
