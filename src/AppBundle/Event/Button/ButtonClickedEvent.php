<?php

namespace AppBundle\Event\Button;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Packet\IS_BTC;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when player clicks on button
 */
class ButtonClickedEvent extends Event {

    const NAME = 'button.clicked';

    /** @var Host */
    public $host;

    /** @var IS_BTC */
    public $packet;
    
    /** @var Player */
    public $player;

    public function __construct(Host $host, IS_BTC $packet) {
        $this->host = $host;
        $this->packet = $packet;
    }

}
