<?php

namespace AppBundle\Event\UI;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

class WindowClosedEvent extends Event {

    const NAME = 'ui.window.closed';

    /** @var Player */
    private $player;

    /** @var Host */
    private $host;

    public function __construct(Player $player, Host $host) {
        $this->player = $player;
        $this->host = $host;
    }

    public function getPlayer(): Player {
        return $this->player;
    }

    public function getHost(): Host {
        return $this->host;
    }

}
