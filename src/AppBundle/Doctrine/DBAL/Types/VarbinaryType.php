<?php

namespace AppBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class VarbinaryType extends Type {

    public function getName() {
        return 'varbinary';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        if (isset($fieldDeclaration['length'])) {
            return "VARBINARY (" . $fieldDeclaration['length'] . ")";
        } else {
            return "VARBINARY (255)";
        }
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        return ($value === null) ? null : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return ($value === null) ? null : $value;
    }

}
