<?php

namespace AppBundle\Packet;

/**
 * Result (qualify or confirmed finish)
 */
class IS_RES extends Packet {

    const PACK = 'CCxCa24a24a8a4VVxCCxvvCCv';
    const UNPACK = 'CSize/CType/CReqI/CPLID/a24UName/a24PName/A8Plate/a4CName/VTTime/VBTime/CSpA/CNumStops/CConfirm/CSpB/vLapsDone/vFlags/CResultNum/CNumRes/vPSeconds';
    const CONF_MENTIONED = 1;
    const CONF_CONFIRMED = 2;
    const CONF_PENALTY_DT = 4;
    const CONF_PENALTY_SG = 8;
    const CONF_PENALTY_30 = 16;
    const CONF_PENALTY_45 = 32;
    const CONF_DID_NOT_PIT = 64;

    //define CONF_DISQ	(CONF_PENALTY_DT | CONF_PENALTY_SG | CONF_DID_NOT_PIT)
    //define CONF_TIME = (isRES::CONF_PENALTY_30 | isRES::CONF_PENALTY_45);

    protected $Size = 84;       # 84
    protected $Type = Packet::ISP_RES;  # ISP_RES
    public $ReqI;               # 0 unless this is a reply to a TINY_RES request
    public $PLID;               # player's unique id (0 = player left before result was sent)
    public $UName;              # username
    public $PName;              # nickname
    public $Plate;              # number plate - NO ZERO AT END!
    public $CName;              # skin prefix
    public $TTime;              # race time (ms)
    public $BTime;              # best lap (ms)
    protected $SpA;
    public $NumStops;           # number of pit stops
    public $Confirm;            # confirmation flags : disqualified etc - see below
    protected $SpB;
    public $LapsDone;           # laps completed
    public $Flags;              # player flags : help settings etc - see below
    public $ResultNum;          # finish or qualify pos (0 = win / 255 = not added to table)
    public $NumRes;             # total number of results (qualify doesn't always add a new one)
    public $PSeconds;           # penalty time in seconds (already included in race time)

    public function isConfirmed() {
        return ($this->Confirm & self::CONF_CONFIRMED);
    }

}
