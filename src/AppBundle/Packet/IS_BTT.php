<?php

namespace AppBundle\Packet;

/**
 * Button type
 */
class IS_BTT extends Packet {

    const PACK = 'CCCCCCCxa96';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CTypeIn/CSp3/a96Text';

    protected $Size = 104;
    protected $Type = Packet::ISP_BTT;

    /** @var int ReqI as received in the IS_BTN */
    public $ReqI;

    /** @var int connection that typed into the button (zero if local) */
    public $UCID;

    /** @var int button identifier originally sent in IS_BTN */
    public $ClickID;

    /** @var int used internally by InSim */
    public $Inst;

    /** @var int from original button specification */
    public $TypeIn;
    protected $Sp3;

    /** @var string typed text, zero to TypeIn specified in IS_BTN */
    public $Text;

}
