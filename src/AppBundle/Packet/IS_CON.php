<?php

namespace AppBundle\Packet;

/**
 * Reports contacts between two cars if the closing speed is above 0.25 m/s
 */
class IS_CON extends Packet {

    const PACK = 'CCCCvv';
    const UNPACK = 'CSize/CType/CReqI/CZero/vSpClose/vTime';

    protected $Size = 40;       # 40
    protected $Type = Packet::ISP_CON;  # ISP_CON
    protected $ReqI = 0;        # 0
    protected $Zero;
    public $SpClose;            # high 4 bits : reserved / low 12 bits : closing speed (10 = 1 m/s)
    public $Time;               # looping time stamp (hundredths - time since reset - like TINY_GTH)
    /** @var CarContact */
    public $A;
    /** @var CarContact */
    public $B;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $this->A = new CarContact(substr($rawPacket, 8, 16));
        $this->B = new CarContact(substr($rawPacket, 24, 16));

        return $this;
    }

}
