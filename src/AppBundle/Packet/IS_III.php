<?php

namespace AppBundle\Packet;

/**
 * Hidden Message /i
 */
class IS_III extends Packet {

    const PACK = 'CCxxCCxxa64';
    const UNPACK = 'CSize/CType/CReqI/CZero/CUCID/CPLID/CSp2/CSp3/a*Msg';

    protected $Size;
    protected $Type = Packet::ISP_III;
    protected $ReqI = 0;
    protected $Zero = null;

    /** @var int connection's unique id (0 = host) */
    public $UCID = 0;

    /** @var int player's unique id (if zero, use UCID) */
    public $PLID = 0;
    protected $Sp2 = null;
    protected $Sp3 = null;

    /** @var string */
    public $Msg;

}
