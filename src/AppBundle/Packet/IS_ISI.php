<?php

namespace AppBundle\Packet;

/**
 * InSim Init - packet to initialise the InSim system
 */
class IS_ISI extends Packet {

    const PACK = 'CCCxvvCCva16a16';
    const UNPACK = 'CSize/CType/CReqI/CZero/vUDPPort/vFlags/CInSimVer/CPrefix/vInterval/a16Admin/a16IName';

    protected $Size = 44;
    protected $Type = Packet::ISP_ISI;

    /** @var int If non-zero LFS will send an IS_VER packet */
    public $ReqI;
    protected $Zero = null;

    /** @var int Port for UDP replies from LFS (0 to 65535) */
    public $UDPPort;

    /** @var int */
    public $Flags;

    /** @var int */
    public $InSimVer = 7;

    /** @var string Special host message prefix character */
    public $Prefix;

    /** @var int Time in ms between NLP or MCI (0 = none) */
    public $Interval;

    /** @var string Admin password (if set in LFS) */
    public $Admin;

    /** @var string A short name for your program */
    public $IName;

    public function __conStruct($rawPacket = null) {
        parent::__conStruct($rawPacket);
    }

}
