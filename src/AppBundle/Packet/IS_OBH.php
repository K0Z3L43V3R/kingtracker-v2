<?php

namespace AppBundle\Packet;

/**
 * Car object contact packet
 */
class IS_OBH extends Packet {

    const PACK = 'CCCCvvx8ssCxCC';
    const UNPACK = 'CSize/CType/CReqI/CPLID/vSpClose/vTime/x8C/sX/sY/CZbyte/xSp1/CIndex/COBHFlags';
    const OBH_LAYOUT = 1;       // an added object
    const OBH_CAN_MOVE = 2;     // a movable object
    const OBH_WAS_MOVING = 4;   // was moving before this hit
    const OBH_ON_SPOT = 8;      // object in original position

    protected $Size = 24;       # 24
    protected $Type = Packet::ISP_OBH;  # ISP_OBH
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id
    public $SpClose;            # high 4 bits : reserved / low 12 bits : closing speed (10 = 1 m/s)
    public $Time;               # looping time stamp (hundredths - time since reset - like TINY_GTH)
    /** @var CarContOBJ */
    public $C;
    public $X;                  # as in ObjectInfo
    public $Y;                  # as in ObjectInfo
    public $Zbyte;              # if OBH_LAYOUT is set : Zbyte as in ObjectInfo
    private $Sp1;
    public $Index;              # AXO_x as in ObjectInfo or zero if it is an unknown object
    public $OBHFlags;           # see below

    public function isLayout() {
        return ($this->OBHFlags & self::OBH_LAYOUT);
    }

    public function canMove() {
        return ($this->OBHFlags & self::OBH_CAN_MOVE);
    }

    public function wasMoving() {
        return ($this->OBHFlags & self::OBH_WAS_MOVING);
    }

    public function isOnSpot() {
        return ($this->OBHFlags & self::OBH_ON_SPOT);
    }

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $this->C = new CarContOBJ(substr($rawPacket, 8, 8));

        return $this;
    }

}
