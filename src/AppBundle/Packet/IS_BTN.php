<?php

namespace AppBundle\Packet;

/**
 * Button
 */
class IS_BTN extends Packet {

    const PACK = 'CCCCCCCCCCCCa240';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CBStyle/CTypeIn/CL/CT/CW/CH/a240Text';
    const IS_X_MIN = 0;
    const IS_X_MAX = 110;
    const IS_Y_MIN = 30;
    const IS_Y_MAX = 170;

    protected $Size = 252;
    protected $Type = Packet::ISP_BTN;

    /** @var int non-zero (returned in IS_BTC and IS_BTT packets) */
    public $ReqI = 255;

    /** @var int connection to display the button (0 = local / 255 = all) */
    public $UCID = 255;

    /** @var int button ID (0 to 239) */
    public $ClickID = 0;

    /** @var int some extra flags - see below */
    public $Inst = null;

    /** @var int button style flags */
    public $BStyle = null;

    /** @var int max chars to type in - see below */
    public $TypeIn = null;

    /** @var int left   : 0 - 200 */
    public $L = self::IS_X_MIN;

    /** @var int top    : 0 - 200 */
    public $T = self::IS_Y_MIN;

    /** @var int width  : 0 - 200 */
    public $W = 0;

    /** @var int height : 0 - 200 */
    public $H = 0;

    /** @var string 0 to 240 characters of text */
    public $Text = '';

    public function pack() {
        if (strlen($this->Text) > 239) {
            $this->Text = substr($this->Text, 0, 239);
        }

        return parent::pack();
    }

}
