<?php

namespace AppBundle\Packet;

/**
 * Version packet
 */
class IS_VER extends Packet {

    const PACK = 'CCCxa8a6v';
    const UNPACK = 'CSize/CType/CReqI/CZero/a8Version/a6Product/vInSimVer';

    protected $Size = 20;
    protected $Type = Packet::ISP_VER; 
    public $ReqI;
    protected $Zero;
    
    /** @var string LFS version */
    public $Version;
    
    /** @var string Product : DEMO or S1, S2, S4 */
    public $Product;
    
    /** @var string InSim Version */
    public $InSimVer;

}
