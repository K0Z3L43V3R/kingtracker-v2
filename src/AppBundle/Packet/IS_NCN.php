<?php

namespace AppBundle\Packet;

/**
 * New ConN
 */
class IS_NCN extends Packet {

    const PACK = 'CCCCa24a24CCCx';
    const UNPACK = 'CSize/CType/CReqI/CUCID/a24UName/a24PName/CAdmin/CTotal/CFlags/CSp3';
    const FLAG_REMOTE = 4;

    protected $Size = 56;
    protected $Type = Packet::ISP_NCN;
    
    /**
     * @var int 0 unless this is a reply to a TINY_NCN request
     */
    public $ReqI = null; 
    
    /**
     * @var int new connection's unique id (0 = host)
     */
    public $UCID;
    
    /**
     * username
     */
    public $UName;
    
    /**
     * nickname
     */
    public $PName; 
    
    /**
     * 1 if admin
     */
    public $Admin;
    
    /**
     * number of connections including host
     */
    public $Total;
    
    /**
     * bit 2 : remote
     */
    public $Flags;
    
    protected $Sp3;
    
    public function isRemote() {
        return ($this->Flags & 4);
    }
}