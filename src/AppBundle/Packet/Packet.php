<?php

namespace AppBundle\Packet;

use ReflectionClass;

abstract class Packet {

    /**
     * Not used
     */
    const ISP_NONE = 0;

    /**
     * Insim initialise
     */
    const ISP_ISI = 1;

    /**
     * Version info
     */
    const ISP_VER = 2;

    /**
     * Multi purpose
     */
    const ISP_TINY = 3;

    /**
     * Multi purpose
     */
    const ISP_SMALL = 4;

    /**
     * State info
     */
    const ISP_STA = 5;

    /**
     * Send single character
     */
    const ISP_SCH = 6;

    /**
     * Set state flags pack
     */
    const ISP_SFP = 7;

    /**
     * Sst car camera
     */
    const ISP_SCC = 8;

    /**
     * Camera position
     */
    const ISP_CPP = 9;

    /**
     * Start multiplayer
     */
    const ISP_ISM = 10;

    /**
     * Message out
     */
    const ISP_MSO = 11;

    /**
     * Hidden /i message
     */
    const ISP_III = 12;

    /**
     * Type message or /command
     */
    const ISP_MST = 13;

    /**
     * Message to a connection
     */
    const ISP_MTC = 14;

    /**
     * Set screen mode
     */
    const ISP_MOD = 15;

    /**
     * Vote notification
     */
    const ISP_VTN = 16;

    /**
     * Race start
     */
    const ISP_RST = 17;

    /**
     * New connection
     */
    const ISP_NCN = 18;

    /**
     * Connection left
     */
    const ISP_CNL = 19;

    /**
     * Connection renamed
     */
    const ISP_CPR = 20;

    /** New player (joined race) */
    const ISP_NPL = 21;

    /** Player pit (keeps slot in race) */
    const ISP_PLP = 22;

    /** Player leave (spectate - loses slot) */
    const ISP_PLL = 23;

    /**
     * Lap time
     */
    const ISP_LAP = 24;

    /**
     * Split x time
     */
    const ISP_SPX = 25;

    /**
     * Pit stop start
     */
    const ISP_PIT = 26;

    /**
     * Pit stop finish
     */
    const ISP_PSF = 27;

    /**
     * Pit lane enter / leave
     */
    const ISP_PLA = 28;

    /**
     * Camera changed
     */
    const ISP_CCH = 29;

    /**
     * Penalty given or cleared
     */
    const ISP_PEN = 30;

    /**
     * Take over car
     */
    const ISP_TOC = 31;

    /**
     * Flag (yellow or blue
     */
    const ISP_FLG = 32;

    /**
     * Player FLags (help flags changed)
     */
    const ISP_PFL = 33;

    /**
     * Finished race
     */
    const ISP_FIN = 34;

    /**
     * Result confirmed
     */
    const ISP_RES = 35;

    /**
     * Reorder (info or inStruction)
     */
    const ISP_REO = 36;

    /**
     * Node and lap packet
     */
    const ISP_NLP = 37;

    /**
     * Multi car info
     */
    const ISP_MCI = 38;

    /**
     * Type message
     */
    const ISP_MSX = 39;

    /**
     * Message to local computer
     */
    const ISP_MSL = 40;

    /**
     * Car reset
     */
    const ISP_CRS = 41;

    /**
     * Delete buttons / receive button requests
     */
    const ISP_BFN = 42;

    /**
     * Autocross layout information
     */
    const ISP_AXI = 43;

    /**
     * Hit an autocross object
     */
    const ISP_AXO = 44;

    /**
     * Show a button on local or remote screen
     */
    const ISP_BTN = 45;

    /**
     * Sent when a user clicks a button
     */
    const ISP_BTC = 46;

    /**
     * Sent after typing into a button
     */
    const ISP_BTT = 47;

    /**
     * Replay information packet
     */
    const ISP_RIP = 48;

    /**
     * Screenshot
     */
    const ISP_SSH = 49;

    /**
     * Contact (collision report
     */
    const ISP_CON = 50;

    /**
     * Contact car + object (collision report
     */
    const ISP_OBH = 51;

    /**
     * Report incidents that would violate HLVC
     */
    const ISP_HLV = 52;

    /**
     * Player cars
     */
    const ISP_PLC = 53;

    /**
     * Autocross multiple objects
     */
    const ISP_AXM = 54;

    /**
     * Admin command report
     */
    const ISP_ACR = 55;

    /**
     * Car handicaps
     */
    const ISP_HCP = 56;

    /**
     * New connection - extra info for host
     */
    const ISP_NCI = 57;

    /**
     * Reply to a join request (allow / disallow)
     */
    const ISP_JRR = 58;

    /**
     * Report InSim checkpoint / InSim circle
     */
    const ISP_UCO = 59;

    /**
     * Object control (currently used for lights)
     */
    const ISP_OCO = 60;

    /**
     * Multi purpose - target to connection
     */
    const ISP_TTC = 61;

    /**
     * Connection selected a car
     */
    const ISP_SLC = 62;

    /**
     * Car state changed
     */
    const ISP_CSC = 63;

    public function __conStruct($rawPacket = null) {
        if ($rawPacket !== null) {
            $this->unpack($rawPacket);
        }

        return $this;
    }

    /* PHPInSim methods */

    public function &__get($name) {
        return property_exists(get_class($this), $name) ? $this->$name : false;
    }

    public function &__call($name, $arguments) {
        if (property_exists(get_class($this), $name)) {
            $this->$name = array_shift($arguments);
        }

        return $this;
    }

    public function __isset($name) {
        return isset($this->$name);
    }

    public function __unset($name) {
        if (isset($this->$name)) {
            $this->$name = null;
        }
    }

    public function unpack($rawPacket) {
        foreach (unpack($this::UNPACK, $rawPacket) as $property => $value) {
            if (is_string($value)) {
                $value = trim($value);
            }
            $this->$property = $value;
        }

        return $this;
    }

    public function pack() {
        $return = '';
        $packFormat = $this->parsePackFormat();
        $propertyNumber = -1;

        foreach ($this as $property => $value) {
            $pkFnkFormat = $packFormat[++$propertyNumber];

            if ($pkFnkFormat == 'x') {
                $return .= pack('C', 0); # null & 0 are the same thing in Binary (00000000) and Hex (x00), so null == 0.
            } elseif (is_array($pkFnkFormat)) {
                list($type, $elements) = $pkFnkFormat;

                for ($i = 0; $i < $elements; ++$i) {
                    if (isset($value[$i])) {
                        //var_dump($value, $type, $elements, $i, $value[$i]);
                        $return .= pack($type, $value[$i]);
                    } else {
                        $return .= pack("x", 0);
                    }
                }
            } else {
                $return .= pack($pkFnkFormat, $value);
            }
        }

        return $return;
    }

    public function parseUnpackFormat() {
        $return = array();

        foreach (explode('/', $this::UNPACK) as $element) {
            for ($i = 1; is_numeric($element{$i}); ++$i) {
                
            }

            $dataType = substr($element, 0, $i);
            $dataName = substr($element, $i);
            $return[$dataName] = $dataType;
        }

        return $return;
    }

    public function parsePackFormat() {
        $format = $this::PACK; # It does not like using $this::PACK directly.
        $elements = array();

        for ($i = 0, $j = 1, $k = strlen($format); $i < $k; ++$i, ++$j) {    # i = Current Character; j = Look ahead for numbers.
            if (is_string($format{$i}) && !isset($format[$j]) || !is_numeric($format[$j])) {
                $elements[] = $format{$i};
            } else {
                while (isset($format{$j}) && is_numeric($format{$j})) {
                    ++$j;    # Will be the last number of the current element.
                }

                $number = substr($format, $i + 1, $j - ($i + 1));

                if ($format{$i} == 'a' || $format{$i} == 'A') {    # In these cases it's a string type where dealing with.
                    $elements[] = $format{$i} . $number;
                } else { # In these cases, we should get an array.
                    $elements[] = array($format{$i}, $number);
                }

                $i = $j - 1; # Movies the pointer to the end of this element.
            }
        }

        return $elements;
    }

    public static function binaryMask($test, $value) {
        return ($value & $test);
    }

    public static function getPacketType($value) {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());


        return isset($constants[$value]) ? $constants[$value] : 'Undefined(' . $value . ')';
    }

}
