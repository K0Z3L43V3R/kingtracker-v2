<?php

namespace AppBundle\Packet;

/**
 * Split
 */
class IS_SPX extends Packet {

    const PACK = 'CCxCVVCCCx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VSTime/VETime/CSplit/CPenalty/CNumStops/CSp3';

    protected $Size = 16;
    protected $Type = Packet::ISP_SPX;
    protected $ReqI = null;

    /** @var int player's unique id */
    public $PLID;

    /** @var int split time (ms) */
    public $STime;

    /** @var int total time (ms) */
    public $ETime;

    /** @var int split number 1, 2, 3 */
    public $Split;

    /** @var int current penalty value */
    public $Penalty;

    /** @var int number of pit stops */
    public $NumStops;
    protected $Sp3;

    public function __conStruct($rawPacket = null) {
        parent::__conStruct($rawPacket);
    }

}
