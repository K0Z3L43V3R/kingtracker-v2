<?php

namespace AppBundle\Packet;

class CarContOBJ extends Packet {

    const PACK = 'CCCCss';
    const UNPACK = 'CDirection/CHeading/CSpeed/CZbyte/sX/sY';

    public $Direction;                    # car's motion if Speed > 0 : 0 = world y direction, 128 = 180 deg
    public $Heading;                    # direction of forward axis : 0 = world y direction, 128 = 180 deg
    public $Speed;                        # m/s
    public $Zbyte;
    public $X;                            # position (1 metre = 16)
    public $Y;                            # position (1 metre = 16)

}
