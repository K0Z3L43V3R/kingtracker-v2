<?php

namespace AppBundle\Packet;

/**
 * Message To Connection 
 * - hosts only
 * - send to a connection / a player / all
 */
class IS_MTC extends Packet {

    const PACK = 'CCxCCCxxa128';
    const UNPACK = 'CSize/CType/CReqI/CSound/CUCID/CPLID/CSp2/CSp3/a128Text';

    protected $Size = 136;      # 8 + TEXT_SIZE (TEXT_SIZE = 4, 8, 12... 128)
    protected $Type = Packet::ISP_MTC;
    protected $ReqI = 0;
    
    /** @var int Sound effect (@see MessageSound) */
    public $Sound = null;
    
    /** @var int Connection's unique id (0 = host / 255 = all)*/
    public $UCID = 0;
    
    /** @var int Player's unique id (if zero, use UCID) */
    public $PLID = 0;
    
    protected $Sp2 = null;
    protected $Sp3 = null;
    
    /** @var string Up to 128 characters of text - last byte must be zero */
    public $Text;

    public function pack() {
        if (strlen($this->Text) > 127) {
            $this->Text = substr($this->Text, 0, 127);
        }

        return parent::pack();
    }

}
