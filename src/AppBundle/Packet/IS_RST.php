<?php

namespace AppBundle\Packet;

use AppBundle\Types\Timing;

/**
 * Race start
 */
class IS_RST extends Packet {

    const PACK = 'CCCxCCCCa6CCvvvvvv';
    const UNPACK = 'CSize/CType/CReqI/CZero/CRaceLaps/CQualMins/CNumP/CTiming/a6Track/CWeather/CWind/vFlags/vNumNodes/vFinish/vSplit1/vSplit2/vSplit3';
    

    protected $Size = 28;
    protected $Type = Packet::ISP_RST;
    public $ReqI = true;
    protected $Zero = null;

    /** @var int 0 if qualifying */
    public $RaceLaps;

    /** @var int 0 if race */
    public $QualMins;

    /** @var int number of players in race */
    public $NumP;

    /** @var int lap timing (see below) */
    public $Timing;

    /** @var string short track name */
    public $Track;

    /** @var int */
    public $Weather;

    /** @var int */
    public $Wind;

    /** @var int race flags (must pit, can reset, etc - see below) */
    public $Flags;

    /** @var int total number of nodes in the path */
    public $NumNodes;

    /** @var int node index - finish line */
    public $Finish;

    /** @var int node index - split 1 */
    public $Split1;

    /** @var int node index - split 2 */
    public $Split2;

    /** @var int node index - split 3 */
    public $Split3;

    /** @var int */
    public $splitsCount = 0;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $timing = $this->Timing;

        $this->Timing = ($this->Timing & 192);

        if ($this->Timing == Timing::RST_TIMING_CUSTOM) {
            $this->splitsCount = ($timing & 1) + ($timing & 2);
        }

        if ($this->Timing == Timing::RST_TIMING_STANDARD) {
            if ($this->Split1 != 65535)
                $this->splitsCount++;
            if ($this->Split2 != 65535)
                $this->splitsCount++;
            if ($this->Split3 != 65535)
                $this->splitsCount++;
        }

        return $this;
    }

    public function raceLapsToString() {
        if ($this->RaceLaps == 0) {
            return $this->QualMins == 0 ? 'practice' : '---';
        } elseif ($this->RaceLaps >= 1 && $this->RaceLaps <= 99) {
            return $this->RaceLaps . ' laps';
        } elseif ($this->RaceLaps >= 100 && $this->RaceLaps <= 190) {
            return (($this->RaceLaps - 100) * 10 + 100) . ' laps';
        } elseif ($this->RaceLaps >= 191 && $this->RaceLaps <= 238) {
            return ($this->RaceLaps - 190) . ' hours';
        }
    }

}

// RaceLaps (rl) : (various meanings depending on range)

// 0       : practice
// 1-99    : number of laps...   laps  = rl
// 100-190 : 100 to 1000 laps... laps  = (rl - 100) * 10 + 100
// 191-238 : 1 to 48 hours...    hours = rl - 190

// HOSTF_CAN_VOTE		1
// HOSTF_CAN_SELECT		2
// HOSTF_MID_RACE		32
// HOSTF_MUST_PIT		64
// HOSTF_CAN_RESET		128
// HOSTF_FCV			256
// HOSTF_CRUISE			512