<?php

namespace AppBundle\Packet;

class IS_MCI extends Packet {

    const PACK = 'CCCC';
    const UNPACK = 'CSize/CType/CReqI/CNumC';

    protected $Size;            # 4 + NumC * 28
    protected $Type = Packet::ISP_MCI;  # ISP_MCI
    public $ReqI;               # 0 unless this is a reply to an TINY_MCI request
    public $NumC;               # number of valid CompCar Structs in this packet
    public $Info = array();     # car info for each player, 1 to 8 of these (NumC)

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        for ($i = 0; $i < $this->NumC; $i++) {
            $this->Info[$i] = new CompCar(substr($rawPacket, 4 + ($i * 28), 28));
        }

        return $this;
    }

}
