<?php

namespace AppBundle\Packet;

/**
 * New PLayer joining race (if PLID already exists, then leaving pits)
 */
class IS_NPL extends Packet {

    const PACK = 'CCCCCCva24a8a4a16C4CCCCVCCxx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CUCID/CPType/vFlags/a24PName/A8Plate/a4CName/a16SName/C4Tyres/CH_Mass/CH_TRes/CModel/CPass/VSpare/CSetF/CNumP/CSp2/CSp3';

    protected $Size = 76;
    protected $Type = Packet::ISP_NPL;
    
    /** @var int 0 unless this is a reply to an TINY_NPL request */
    public $ReqI;
    
    /** @var int player's newly assigned unique id */
    public $PLID;
    
    /** @var int connection's unique id */
    public $UCID;
    
    /** @var int bit 0 : female / bit 1 : AI / bit 2 : remote */
    public $PType;
    
    /** @var int player flags */
    public $Flags;
    
    /** @var string nickname */
    public $PName;
    
    /** @var string number plate - NO ZERO AT END! */
    public $Plate;
    
    /** @var string car name */
    public $CName;
    
    /** @var string skin name - MAX_CAR_TEX_NAME */
    public $SName;
        
    public $Tyres = array();    # compounds
    public $H_Mass;             # added mass (kg)
    public $H_TRes;             # intake restriction
    public $Model;              # driver model
    public $Pass;               # passengers byte
    protected $Spare;
    public $SetF;               # setup flags (see below)
    public $NumP;               # number in race (same when leaving pits, 1 more if new)
    protected $Sp2;
    protected $Sp3;

    public function unpack($rawPacket) {
        $pkClass = unpack($this::UNPACK, $rawPacket);

        for ($Tyre = 1; $Tyre <= 4; ++$Tyre) {
            $pkClass['Tyres'][] = $pkClass["Tyres{$Tyre}"];
            unset($pkClass["Tyres{$Tyre}"]);
        }

        foreach ($pkClass as $property => $value) {
            if (is_string($value)) {
                $value = trim($value);
            }
            $this->$property = $value;
        }

        return $this;
    }

    public function isFemale() {
        return ($this->PType & 1);
    }

    public function isAI() {
        return ($this->PType & 2);
    }

    public function isRemote() {
        return ($this->PType & 4);
    }
}
