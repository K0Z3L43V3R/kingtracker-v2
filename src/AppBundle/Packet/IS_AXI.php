<?php

namespace AppBundle\Packet;

/**
 * AutoX Info
 */
class IS_AXI extends Packet {

    const PACK = 'CCCxCCva32';
    const UNPACK = 'CSize/CType/CReqI/CZero/CAXStart/CNumCP/vNumO/a32LName';

    protected $Size = 40;
    protected $Type = Packet::ISP_AXI;
    public $ReqI;
    protected $Zero;

    /** @var int autocross start position */
    public $AXStart;

    /** @var int number of checkpoints */
    public $NumCP;

    /** @var int number of objects */
    public $NumO;

    /** @var string the name of the layout last loaded (if loaded locally) */
    public $LName;

}
