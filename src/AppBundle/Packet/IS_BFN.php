<?php

namespace AppBundle\Packet;

use AppBundle\Types\ButtonFunction;

/**
 * Button function
 */
class IS_BFN extends Packet {

    const PACK = 'CCxCCCCx';
    const UNPACK = 'CSize/CType/CReqI/CSubT/CUCID/CClickID/CClickMax/CSp3';

    protected $Size = 8;
    protected $Type = Packet::ISP_BFN;
    protected $ReqI;

    /** @var int subtype, from BFN_ enumeration (see below) */
    public $SubT;

    /** @var int connection to send to or from (0 = local / 255 = all) */
    public $UCID;

    /** @var int ID of button to delete (if SubT is BFN_DEL_BTN) */
    public $ClickID;

    /** @var int used internally by InSim */
    public $ClickMax;
    protected $Sp3;

    function isUserRequest() {
        return $this->SubT == ButtonFunction::BFN_REQUEST;
    }

    function isUserClear() {
        return $this->SubT == ButtonFunction::BFN_USER_CLEAR;
    }

}
