<?php

namespace AppBundle\Packet;

use AppBundle\Types\HLVCType;

/**
 * Report incidents that would violate HLVC
 */
class IS_HLV extends Packet {

    const PACK = 'CCCCCCvx8';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CHLVC/xSp1/vTime/x8C';

    protected $Size = 16;
    protected $Type = Packet::ISP_HLV;
    protected $ReqI = null;

    /** @var int player's unique id */
    public $PLID;

    /** @var int */
    public $HLVC;
    private $Sp1;

    /** @var int looping time stamp (hundredths - time since reset - like TINY_GTH) */
    public $Time;

    /** @var CarContOBJ car contact */
    public $C;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $this->C = new CarContOBJ(substr($rawPacket, 8, 8));

        return $this;
    }

}
