<?php

namespace AppBundle\Packet;

/**
 * Connection leave
 */
class IS_CNL extends Packet {

    const PACK = 'CCxCCCxx';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CReason/CTotal/CSp2/CSp3';

    protected $Size = 8;
    protected $Type = Packet::ISP_CNL;
    public $ReqI;
    
    /** @var int unique id of the connection which left */
    public $UCID;
    
    /** @var int */
    public $Reason;
    
    /** @var int number of connections including host*/
    public $Total;
    
    protected $Sp2;
    protected $Sp3;
}
