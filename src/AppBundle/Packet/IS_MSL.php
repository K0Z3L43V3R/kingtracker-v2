<?php

namespace AppBundle\Packet;

/**
 * MSg Local - message to appear on local computer only
 */
class IS_MSL extends Packet {

    const PACK = 'CCxCa128';
    const UNPACK = 'CSize/CType/CReqI/CSound/a128Msg';

    protected $Size = 132;      # 132
    protected $Type = Packet::ISP_MSL;  # ISP_MSL
    protected $ReqI = 0;        # 0
    public $Sound = 0; # sound effect (see Message Sounds below)
    public $Msg;                # last byte must be zero

}
