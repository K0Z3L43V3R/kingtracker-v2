<?php

namespace AppBundle\Packet;

use ReflectionClass;
use ReflectionProperty;

/**
 * General purpose 4 byte packet
 */
class IS_TINY extends Packet {

    const PACK = 'CCCC';
    const UNPACK = 'CSize/CType/CReqI/CSubT';
    const TINY_NONE = 0;     //  0 - keep alive        : see "maintaining the connection"
    const TINY_VER = 1;     //  1 - info request    : get version
    const TINY_CLOSE = 2;     //  2 - inStruction        : close insim
    const TINY_PING = 3;     //  3 - ping request    : external progam requesting a reply
    const TINY_REPLY = 4;     //  4 - ping reply        : reply to a ping request
    const TINY_VTC = 5;     //  5 - both ways        : game vote cancel (info or request
    const TINY_SCP = 6;     //  6 - info request    : send camera pos
    const TINY_SST = 7;     //  7 - info request    : send state info
    const TINY_GTH = 8;     //  8 - info request    : get time in hundredths (i.e. SMALL_RTP
    const TINY_MPE = 9;     //  9 - info            : multi player end
    const TINY_ISM = 10;    // 10 - info request    : get multiplayer info (i.e. ISP_ISM
    const TINY_REN = 11;    // 11 - info            : race end (return to game setup screen
    const TINY_CLR = 12;    // 12 - info            : all players cleared from race
    const TINY_NCN = 13;    // 13 - info request    : get all connections
    const TINY_NPL = 14;    // 14 - info request    : get all players
    const TINY_RES = 15;    // 15 - info request    : get all results
    const TINY_NLP = 16;    // 16 - info request    : send an IS_NLP
    const TINY_MCI = 17;    // 17 - info request    : send an IS_MCI
    const TINY_REO = 18;    // 18 - info request    : send an IS_REO
    const TINY_RST = 19;    // 19 - info request    : send an IS_RST
    const TINY_AXI = 20;    // 20 - info request    : send an IS_AXI - AutoX Info
    const TINY_AXC = 21;    // 21 - info            : autocross cleared
    const TINY_RIP = 22;    // 22 - info request    : send an IS_RIP - Replay Information Packet
    const TINY_NCI = 23;    // 23 - info request    : get NCI for all guests (on host only
    const TINY_ALC = 24;    // 24 - info request	: send a SMALL_ALC (allowed cars)
    const TINY_AXM = 25;    // 25 - info request	: send IS_AXM packets for the entire layout
    const TINY_SLC = 26;    // 26 - info request	: send IS_SLC packets for all connections

    protected $Size = 4;            # always 4
    protected $Type = Packet::ISP_TINY;
    public $ReqI;                   # 0 unless it is an info request or a reply to an info request
    public $SubT;                   # subtype, from TINY_ enumeration (e.g. TINY_RACE_END)

    public static function getTinyType($value) {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        return isset($constants[$value]) ? $constants[$value] : 'Undefined(' . $value . ')';
    }

}
