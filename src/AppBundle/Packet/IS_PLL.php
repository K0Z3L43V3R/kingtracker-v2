<?php

namespace AppBundle\Packet;

/**
 * Player spectate
 */
class IS_PLL extends Packet {

    const PACK = 'CCxC';
    const UNPACK = 'CSize/CType/CReqI/CPLID';

    protected $Size = 4;
    protected $Type = Packet::ISP_PLL;
    protected $ReqI = null;
    
    /** @var int player's unique id */
    public $PLID;

}
