<?php

namespace AppBundle\Packet;

use AppBundle\Types\ButtonClick;

/**
 * Button click
 */
class IS_BTC extends Packet {

    const PACK = 'CCCCCCCx';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CCFlags/CSp3';

    protected $Size = 8;
    protected $Type = Packet::ISP_BTC;

    /** @var int ReqI as received in the IS_BTN */
    public $ReqI;

    /** @var int connection that clicked the button (zero if local) */
    public $UCID;

    /** @var int button identifier originally sent in IS_BTN */
    public $ClickID;

    /** @var int used internally by InSim */
    public $Inst;

    /** @var int button click flags - see below */
    public $CFlags;
    protected $Sp3;

    public function isLMB() {
        return $this->CFlags & ButtonClick::ISB_LMB;
    }

    public function isRMB() {
        return $this->CFlags & ButtonClick::ISB_RMB;
    }

    public function isCTRL() {
        return $this->CFlags & ButtonClick::ISB_CTRL;
    }

    public function isSHIFT() {
        return $this->CFlags & ButtonClick::ISB_SHIFT;
    }

}
