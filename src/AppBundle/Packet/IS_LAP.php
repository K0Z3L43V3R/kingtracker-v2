<?php

namespace AppBundle\Packet;

use AppBundle\Types\PenaltyType;
use AppBundle\Types\PlayerFlags;

/**
 * Lap
 */
class IS_LAP extends Packet {

    const PACK = 'CCxCVVvvxCCx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VLTime/VETime/vLapsDone/vFlags/CSp0/CPenalty/CNumStops/CSp3';

    protected $Size = 20;
    protected $Type = Packet::ISP_LAP;
    protected $ReqI;

    /** @var int player's unique id */
    public $PLID;

    /** @var int lap time (ms) */
    public $LTime;

    /** @var int total time (ms) */
    public $ETime;

    /** @var int laps completed */
    public $LapsDone;

    /** @var PlayerFlags */
    public $Flags;
    protected $Sp0;

    /** @var PenaltyType current penalty value (see below) */
    public $Penalty;

    /** @var int number of pit stops */
    public $NumStops;
    protected $Sp3;

}
