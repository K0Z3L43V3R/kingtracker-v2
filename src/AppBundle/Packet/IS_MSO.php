<?php

namespace AppBundle\Packet;

/**
 * Message out
 */
class IS_MSO extends Packet {

    const PACK = 'CCxxCCCCa128';
    const UNPACK = 'CSize/CType/CReqI/CZero/CUCID/CPLID/CUserType/CTextStart/a*Msg';

    protected $Size;            # Variable size
    protected $Type = Packet::ISP_MSO;  # ISP_MSO
    protected $ReqI = null;     # 0
    protected $Zero = null;

    public $UCID = 0;           # connection's unique id (0 = host)
    public $PLID = 0;           # player's unique id (if zero, use UCID)
    public $UserType;           # set if typed by a user (see User Values below)
    public $TextStart;          # first character of the actual text (after player name)

    public $Msg;
    
    /**
     * system message
     */
    const MSO_SYSTEM = 0;
    
    /**
     * normal visible user message
     */
    const MSO_USER = 1;
    
    /**
     * hidden message starting with special prefix (see ISI)
     */
    const MSO_PREFIX = 2;
    
    /**
     * hidden message typed on local pc with /o command
     */
    const MSO_O = 3;
}
