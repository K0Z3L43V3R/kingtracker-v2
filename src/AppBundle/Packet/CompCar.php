<?php

namespace AppBundle\Packet;

class CompCar extends Packet {

    const PACK = 'vvCCCxVVVvvvs';
    const UNPACK = 'vNode/vLap/CPLID/CPosition/CInfo/CSp3/lX/lY/lZ/vSpeed/vDirection/vHeading/sAngVel';
    const CCI_BLUE = 1;     // this car is in the way of a driver who is a lap ahead
    const CCI_YELLOW = 2;   // this car is slow or stopped and in a dangerous place
    const CCI_LAG = 32;     // this car is lagging (missing or delayed position packets)
    const CCI_FIRST = 64;   // this is the first compcar in this set of MCI packets
    const CCI_LAST = 128;   // this is the last compcar in this set of MCI packets

    public $Node;       # current path node
    public $Lap;        # current lap
    public $PLID;       # player's unique id
    public $Position;   # current race position : 0 = unknown, 1 = leader, etc...
    public $Info;       # flags and other info - see below
    protected $Sp3;
    public $X;          # X map (65536 = 1 metre)
    public $Y;          # Y map (65536 = 1 metre)
    public $Z;          # Z alt (65536 = 1 metre)
    public $Speed;      # speed (32768 = 100 m/s)
    public $Direction;  # car's motion if Speed > 0 : 0 = world y direction, 32768 = 180 deg
    public $Heading;    # direction of forward axis : 0 = world y direction, 32768 = 180 deg
    public $AngVel;     # signed, rate of change of heading : (16384 = 360 deg/s)

    public function isCausingYellow() {
        return ($this->Info & CompCar::CCI_YELLOW);
    }

    public function isCausingBlue() {
        return ($this->Info & CompCar::CCI_BLUE);
    }

    public function isLagging() {
        return ($this->Info & CompCar::CCI_LAG);
    }

}
