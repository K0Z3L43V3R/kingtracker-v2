<?php

namespace AppBundle\Packet;

/**
 * New Conn Info - sent on host only if an admin password has been set
 */
class IS_NCI extends Packet {

    const PACK = 'CCCCCCCCVV';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CLanguage/CSp1/CSp2/CSp3/VUserID/VIPAddress';

    protected $Size = 16;
    protected $Type = Packet::ISP_NCI;
    public $ReqI = null;
    
    /** @var int connection's unique id (0 = host) */
    public $UCID;
    public $Language;
    protected $Sp1;
    protected $Sp2;
    protected $Sp3;
    
    /** @var int LFS UserID*/
    public $UserID;
    public $IPAddress;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $IPSegments = [];
        for ($i = 0; $i < 4; ++$i) {
            $IPSegments[$i] = unpack("C", substr($rawPacket, 12 + $i))[1];
        }
        $this->IPAddress = implode('.', $IPSegments);

        return $this;
    }

}
