<?php

namespace AppBundle\Entity;

use AppBundle\Component\LFSTranslator;
use AppBundle\Event\Command\CommandRegisteredEvent;
use DirectoryIterator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Translation\Loader\YamlFileLoader;

/**
 * Plugin
 *
 * @ORM\Table(name="plugin")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PluginRepository")
 * @ORM\EntityListeners({"AppBundle\Entity\Listener\PluginListener"})
 */
class Plugin {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255)
     */
    private $class;

    /**
     * @var string
     *
     * @ORM\Column(name="dir", type="string", length=255)
     */
    private $dir;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=32, nullable=true, options={"default" : "1.0.0"})
     */
    private $version;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default" : 0})
     */
    private $active;

    /** @var EventDispatcherInterface */
    protected $eventDispacher;

    /** @var EntityManager */
    protected $entityManager;

    /** @var ContainerInterface */
    protected $container;

    /** @var LFSTranslator */
    protected $translator;

    public function __construct(Plugin $plugin = null, LFSTranslator $translator = null) {
        if ($plugin !== null) {
            $this->eventDispacher = $plugin->getEventDispacher();
            $this->entityManager = $plugin->getEntityManager();
            $this->container = $plugin->getContainer();
            $this->translator = $translator;

            $rootDir = $this->container->getParameter('kernel.root_dir');
            $pluginDir = $rootDir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . $plugin->getDir() . DIRECTORY_SEPARATOR;
            $langsDir = $pluginDir . 'translations' . DIRECTORY_SEPARATOR;

            // Load translations
            if ($this->translator !== null && file_exists($langsDir)) {
                $this->translator->addLoader('yaml', new YamlFileLoader());

                $dirIterator = new DirectoryIterator($langsDir);

                if ($dirIterator) {
                    foreach ($dirIterator as $file) {
                        if ($file->isFile() && $file->getExtension() == 'yml') {
                            $localeName = $file->getBasename('.yml');

                            // TODO: check if locale is valid
                            $this->translator->addResource('yaml', $file->getPathname(), $localeName);
                        }
                    }
                }
            }
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Plugin
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Plugin
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Plugin
     */
    public function setAuthor($author) {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return Plugin
     */
    public function setVersion($version) {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Plugin
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return Plugin
     */
    public function setClass($class) {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass() {
        return $this->class;
    }

    /**
     * Get dir
     *
     * @return string
     */
    public function getDir() {
        return $this->dir;
    }

    /**
     * Set dir
     *
     * @param string $dir
     *
     * @return Plugin
     */
    public function setDir($dir) {
        $this->dir = $dir;
    }

    public function getEventDispacher(): EventDispatcherInterface {
        return $this->eventDispacher;
    }

    public function getEntityManager(): EntityManager {
        return $this->entityManager;
    }

    public function setEventDispacher(EventDispatcherInterface $eventDispacher) {
        $this->eventDispacher = $eventDispacher;
    }

    public function setEntityManager(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function getContainer(): ContainerInterface {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Register command
     * 
     * @param string $command - eg. help
     * @param string $description - description of command
     * @param callable $callback
     * @param array $arguments - list of arguments ['arg_name' => ['title' => 'Arg name', 'type' => 'int|string', 'required' => 'true|false']]
     * 
     * @return boolean
     */
    public function registerCommand($command, $description, callable $callback, array $arguments = []) {
        if ($this->eventDispacher == null) {
            return false;
        }

        $this->eventDispacher->dispatch(CommandRegisteredEvent::NAME, new CommandRegisteredEvent($command, $description, $callback, $arguments));

        return true;
    }

}
