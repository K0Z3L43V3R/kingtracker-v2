<?php

namespace AppBundle\Entity;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\HostConnection;
use AppBundle\Service\StintService;
use AppBundle\Service\UIService;
use AppBundle\Types\PlayerFlags;
use AppBundle\Types\SetupFlags;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="UserId", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="UName", type="string", length=64, nullable=true)
     */
    private $UName;

    /**
     * @var string
     *
     * @ORM\Column(name="PName", type="varbinary", length=64, nullable=true)
     */
    private $PName;

    /**
     * @var string
     *
     * @ORM\Column(name="licence", type="string", length=16, nullable=true)
     */
    private $licence;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=64, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=64, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=32, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=16, nullable=true)
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="offset", type="string", length=16, nullable=true)
     */
    private $offset;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=64, nullable=true)
     */
    private $timezone;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_joined", type="datetime", nullable=true)
     */
    private $lastJoined;

    /**
     * Host connection
     * 
     * @ORM\OneToOne(targetEntity="HostConnection", mappedBy="player")
     */
    private $hostConnection;

    /** @var PlayerCar */
    private $car;

    /** @var SetupFlags */
    private $setupFlags;

    /** @var PlayerFlags */
    private $playerFlags;

    /** @var string */
    private $status;

    /** @var UIService */
    private $ui;

    /** @var LFSTranslator */
    private $translator;

    /** @var StintService */
    private $stint;
    
    /** @var bool */
    private $isLagging = false;

    public function __destruct() {
        if (isset($this->ui) && $this->ui !== null) {
            $this->ui->__destruct();
            unset($this->ui);
        }
        if (isset($this->stint) && $this->stint !== null) {
            $this->stint->__destruct();
            unset($this->stint);
        }

        unset($this->translator);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Player
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set uName
     *
     * @param string $UName
     *
     * @return Player
     */
    public function setUName($UName) {
        $this->UName = $UName;

        return $this;
    }

    /**
     * Get uName
     *
     * @return string
     */
    public function getUName() {
        return $this->UName;
    }

    /**
     * Set pName
     *
     * @param string $PName
     *
     * @return Player
     */
    public function setPName($PName) {
        $this->PName = $PName;

        return $this;
    }

    /**
     * Get pName
     *
     * @return string
     */
    public function getPName() {
        return $this->PName;
    }

    /**
     * Set licence
     *
     * @param string $licence
     *
     * @return Player
     */
    public function setLicence($licence) {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return string
     */
    public function getLicence() {
        return $this->licence;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Player
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Player
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return Player
     */
    public function setTimezone($timezone) {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone() {
        return $this->timezone;
    }

    /**
     * Set lastJoined
     *
     * @param DateTime $lastJoined
     *
     * @return Player
     */
    public function setLastJoined($lastJoined) {
        $this->lastJoined = $lastJoined;

        return $this;
    }

    /**
     * Get lastJoined
     *
     * @return DateTime
     */
    public function getLastJoined() {
        return $this->lastJoined;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Player
     */
    public function setIp($ip) {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Player
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        if ($this->translator !== null) {
            $this->translator->setLocale($locale);
        }

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set offset
     *
     * @param string $offset
     *
     * @return Player
     */
    public function setOffset($offset) {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Get offset
     *
     * @return string
     */
    public function getOffset() {
        return $this->offset;
    }

    /**
     * Set hostConnection
     *
     * @param HostConnection $hostConnection
     *
     * @return Player
     */
    public function setHostConnection(HostConnection $hostConnection = null) {
        $this->hostConnection = $hostConnection;

        return $this;
    }

    /**
     * Get hostConnection
     *
     * @return HostConnection
     */
    public function getHostConnection() {
        return $this->hostConnection;
    }

    /**
     * 
     * @return SetupFlags
     */
    public function getSetupFlags() {
        return $this->setupFlags;
    }

    /**
     * 
     * @return PlayerFlags
     */
    public function getPlayerFlags() {
        return $this->playerFlags;
    }

    public function setSetupFlags(SetupFlags $setupFlags) {
        $this->setupFlags = $setupFlags;
    }

    public function setPlayerFlags(PlayerFlags $playerFlags) {
        $this->playerFlags = $playerFlags;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getUi(): UIService {
        return $this->ui;
    }

    public function setUi(UIService $ui) {
        $this->ui = $ui;
        $this->ui->player = $this;

        return $this;
    }

    public function getStint(): StintService {
        return $this->stint;
    }

    public function setStint(StintService $stint) {
        $this->stint = $stint;
        $this->stint->setPlayer($this);
        return $this;
    }

    public function getTranslator(): LFSTranslator {
        return $this->translator;
    }

    public function setTranslator(LFSTranslator $translator) {
        $this->translator = $translator;
        $this->translator->setLocale('en_US');

        return $this;
    }
    
    public function isLagging() {
        return $this->isLagging;
    }

    public function setIsLagging($isLagging) {
        $this->isLagging = $isLagging;
        return $this;
    }

    
    /**
     * @return PlayerCar
     */
    public function getCar() {
        return $this->car;
    }

    /**
     * @param PlayerCar|null $car
     */
    public function setCar(PlayerCar $car = null) {
        $this->car = $car;
    }

    // Helper functions
    /**
     * Translate string
     * 
     * @param string $id
     * @param array $parameters
     * 
     * @return string
     */
    public function trans($id, array $parameters = array()) {
        if ($this->translator === null) {
            return $id;
        }

        return $this->translator->trans($id, $parameters);
    }

    /**
     * @return boolean
     */
    public function hasCar() {
        return $this->car !== null ? true : false;
    }

    /**
     * Send error message to player
     * 
     * @param string $text
     * @param array $replace
     */
    public function sendErrorMsg($text, array $replace = []) {
        if ($this->ui !== null) {
            $this->ui->sendErrorMsg($text, $replace);
        }
    }

}
