<?php

namespace AppBundle\Entity\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\UI\Element\Element;
use AppBundle\Entity\UI\UITab;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Button\ButtonTypedEvent;
use AppBundle\Event\UI\WindowClosedEvent;
use AppBundle\Helper\SymbolHelper;
use AppBundle\Packet\IS_BTN;
use AppBundle\Types\ButtonStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI Window
 */
class UIWindow extends UI {

    /** @var string */
    protected $title;

    /** @var Element[] */
    protected $elements = [];

    /** @var UITab[] */
    protected $tabs = [];

    /** @var UITab|null */
    protected $activeTab;

    /** @var boolean */
    protected $showHelp = false;

    /** @var int Window header height */
    protected $headerHeight = 5;

    /** @var int */
    private $button_id_help;

    /** @var int */
    private $button_id_close;

    /** @var int */
    private $button_id_title;

    public function __construct(EventDispatcherInterface $eventDispacher, LFSTranslator $translator, Player $player, Host $host, $id_start = 100, $id_end = 254) {
        parent::__construct($eventDispacher, $player, $host, $translator);

        $this->id_start = $id_start;
        $this->id_end = $id_end;

        $this->eventDispacher->addListener(ButtonClickedEvent::NAME, [$this, 'onButtonClicked']);
        $this->eventDispacher->addListener(ButtonTypedEvent::NAME, [$this, 'onButtonTyped']);
    }

    public function show() {
        $this->displayed = true;

        // Background
        $this->id_current = $this->id_start;

        $button = new IS_BTN();
        $button->ReqI = $this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height + $this->headerHeight;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_LIGHT;
        $button->Text = '';

        $this->sendQueued($button);

        // Header
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->headerHeight;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';

        $this->sendQueued($button);

        // Header 2nd bg
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->headerHeight;
        $button->W = $this->width - ($this->showHelp ? 8 : 4);
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';

        $this->sendQueued($button);


        // Help
        if ($this->showHelp) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L += $button->W;
            $button->T = $this->top;
            $button->H = $this->headerHeight;
            $button->W = 4;
            $button->BStyle = ButtonStyle::ISB_DARK;
            $button->Text = '';

            $this->sendQueued($button);

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            //$button->L -= 1;
            $button->T = $this->top;
            $button->H = $this->headerHeight;
            $button->W = 4;
            $button->BStyle = ButtonStyle::ISB_CLICK + ButtonStyle::COLOUR_LIGHT_GREY;
            $button->Text = '?';

            $this->button_id_help = $this->id_current;
            $this->sendQueued($button);
        }

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->T = $this->top;
        $button->H = $this->headerHeight;
        $button->W = 4;
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';

        $this->sendQueued($button);

        // close button
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->T = $this->top;
        $button->H = $this->headerHeight;
        $button->W = 4;
        $button->BStyle = ButtonStyle::ISB_CLICK + ButtonStyle::COLOUR_LIGHT_GREY;
        $button->Text = SymbolHelper::getClose();

        $this->button_id_close = $this->id_current;
        $this->sendQueued($button);

        // Title
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->headerHeight;
        $button->W = $this->width - 4;
        $button->BStyle = ButtonStyle::ISB_LEFT + ButtonStyle::COLOUR_LIGHT_GREY;
        $button->Text = '^7' . $this->title;
        $this->button_id_title = $button->ClickID;

        $this->sendQueued($button);

        foreach ($this->elements as $element) {
            $element->show($this->id_current);
            $this->id_current = $element->getEndId();
        }

        return $this;
    }

    public function close() {
        $this->eventDispacher->removeListener(ButtonClickedEvent::NAME, [$this, 'onButtonClicked']);
        $this->eventDispacher->removeListener(ButtonTypedEvent::NAME, [$this, 'onButtonTyped']);

        $this->displayed = false;

        foreach ($this->elements as $element) {
            unset($element);
        }

        parent::close();

        $this->eventDispacher->dispatch(WindowClosedEvent::NAME, new WindowClosedEvent($this->player, $this->host));
    }

    /**
     * Event called when player clicks on button
     * 
     * @param ButtonClickedEvent $event
     * @return bool
     */
    public function onButtonClicked(ButtonClickedEvent $event) {
        if (!($this->host->getId() === $event->host->getId() && ($this->player->getHostConnection()->getUCID() === $event->packet->UCID || $event->host->isLocal()))) {
            return false;
        }

        $event->stopPropagation();

        switch ($event->packet->ClickID) {
            case $this->button_id_close:
                $this->close();
                break;
            case $this->button_id_help:
                // TODO: implement help
                break;
        }

        foreach ($this->elements as $element) {
            $element->eventClick($event->packet);
        }

        return true;
    }

    /**
     * Event called when player types in button
     * 
     * @param ButtonTypedEvent $event
     * @return bool
     */
    public function onButtonTyped(ButtonTypedEvent $event) {
        if (!($this->host->getId() === $event->host->getId() && ($this->player->getHostConnection()->getUCID() === $event->packet->UCID || $event->host->isLocal()))) {
            return false;
        }

        $event->stopPropagation();

        foreach ($this->elements as $element) {
            $element->eventType($event->packet);
        }

        return true;
    }

    /**
     * Add new tab
     * 
     * @param UITab $tab
     * @return UITab
     */
    public function addTab(UITab $tab) {
        $this->tabs[] = $tab;

        return $tab;
    }

    /**
     * @param string $name
     * @param Element $element
     * 
     * @return Element
     */
    public function addElement(Element $element) {
        $this->elements[] = $element;

        return $element;
    }

    /**
     * 
     * @param string $name
     * @return mixed|null
     */
    public function findElement($name) {
        foreach ($this->elements as $element) {
            if ($name === $element->getName()) {
                return $element;
            }
        }

        return null;
    }

    /**
     * 
     * @return int
     */
    public function getHeaderHeight() {
        return $this->headerHeight;
    }

    /**
     * Set active tab
     * 
     * @param string $tabName
     * @return bool
     */
    public function setActiveTab($tabName) {
        if ($this->activeTab instanceof UITab) {
            $this->activeTab->close();
        }

        unset($this->activeTab);

        foreach ($this->tabs as $tab) {
            if ($tab->getName() === $tabName) {
                $this->activeTab = $tab;
                $this->activeTab->setIdStart($this->getCurrentId() + 1);
                $this->activeTab->show();

                return true;
            }
        }

        $this->activeTab = null;
        return false;
    }

    /**
     * Set title
     * 
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Set top left position
     * 
     * @param int $top
     * @param int $left
     * @return $this
     */
    public function setPosition($top, $left = 0) {
        $this->top = $top;
        $this->left = $left;

        return $this;
    }

}
