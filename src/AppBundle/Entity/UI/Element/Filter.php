<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\Element\Element;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTN;
use AppBundle\Types\ButtonStyle;

class Filter extends Element {

    const COLOR_ENABLED = '^6';
    const COLOR_DISABLED = '^1';
    const COLOR_NEUTRAL = '^8';

    protected $values;
    protected $onFilterChangeCallback;
    protected $currentFilter;
    protected $heightSectionLabel = 4;
    protected $heightValue = 3;

    /**
     * 
     * @param UIWindow $window
     * @param int $width
     * @param int $top
     * @param int $left
     */
    function __construct(UIWindow $window, $width = 20, $top = 0, $left = 0, $name = 'filter') {
        parent::__construct($window, $name, '', $height = 5, $width, $top, $left);

        $this->width = $width;
    }

    /**
     * 
     * @param array $values
     */
    public function setValue(array $values) {
        $this->values = $values;
    }

    /**
     * 
     * @param int $button_id
     */
    function show($button_id) {
        parent::show($button_id);

        // Filter bg
        $button = new IS_BTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + $this->left_offset;
        $button->T = $this->top + $this->top_offset;
        $button->H = $this->height ? $this->height : $this->window->getHeight();
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';

        $this->addButton($button);
        $this->send($button);

        // Filter
        $topOffset = 0;
        foreach ($this->values as $sectionKey => $filters) {
            $top = $button->T + $topOffset;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left;
            $button->T = $top;
            $button->H = $this->heightSectionLabel;
            $button->W = $this->width;
            $button->BStyle = ButtonStyle::ISB_LEFT + ButtonStyle::ISB_CLICK + ButtonStyle::ISB_LIGHT;
            $button->Text = '^7' . $filters['title'];

            $left = $button->L;
            $viewMode = isset($filters['view']) ? $filters['view'] : 'V';
            $btnWidth = $this->width;
            if ($viewMode == 'H') {
                $btnWidth = $this->width / 2;
            }

            $this->addButton($button);
            $this->send($button);
            $this->values[$sectionKey]['id'] = $button->ReqI;

            $fIndex = 0;
            $top = $button->T;
            $topOffset = $this->heightSectionLabel;
            foreach ($filters['values'] as $filterKey => $filter) {
                if ($filter['state'] == -1) {
                    $color = ButtonStyle::COL_DISABLED;
                } elseif ($filter['state'] == 1) {
                    $color = ButtonStyle::COL_ENABLED;
                } else {
                    $color = ButtonStyle::COL_NEUTRAL;
                }

                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $left;
                $button->T = $top + $topOffset;
                $button->H = $this->heightValue;
                $button->W = $btnWidth;
                $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
                $button->Text = $color . $filter['title'];

                $this->addButton($button);
                $this->send($button);
                $this->values[$sectionKey]['values'][$filterKey]['id'] = $button->ReqI;

                if ($viewMode == 'H') {
                    $left += $btnWidth;
                    if ($fIndex % 2) {
                        $left = $this->left;
                        $top = $button->T;
                        $topOffset = $this->heightValue;
                    }
                }
                if ($viewMode == 'V') {
                    $top = $button->T;
                    $topOffset = $this->heightValue;
                }

                $fIndex++;
            }
        }

        $this->end_id = $this->id_current;
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return boolean
     */
    public function eventClick(IS_BTC $packet) {
        if (!parent::eventClick($packet)) {
            return false;
        }

        $button = new IS_BTN();

        foreach ($this->values as $sectionKey => $filters) {
            foreach ($filters['values'] as $filterKey => $filter) {
                if ($filter['id'] == $packet->ClickID) {
                    // Current filter
                    $color = '';
                    if (empty($filter['state']) OR $filter['state'] < 1) {
                        $filter['state'] = 1;
                        $color = self::COLOR_ENABLED;
                    } else {
                        $filter['state'] = 0;
                        $color = self::COLOR_NEUTRAL;
                    }

                    $this->values[$sectionKey]['values'][$filterKey]['state'] = $filter['state'];
                    $button->ClickID = $filter['id'];
                    $button->Text = $color . $filter['title'];

                    $this->send($button);

                    $this->currentFilter = $filter;
                } else {
                    // Clear state
                    $this->values[$sectionKey]['values'][$filterKey]['state'] = false;
                    $button->ClickID = $filter['id'];
                    $button->Text = $filter['title'];
                    $this->send($button);
                }
            }
        }

        // Callback
        if (is_callable($this->onFilterChangeCallback)) {
            call_user_func($this->onFilterChangeCallback, $this->currentFilter);
        }
    }

    /**
     * 
     * @param callable $callback
     */
    public function setCallback(callable $callback) {
        $this->onFilterChangeCallback = $callback;
    }

}
