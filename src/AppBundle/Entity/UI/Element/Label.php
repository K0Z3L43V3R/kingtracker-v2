<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\Element\Element;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTN;
use AppBundle\Types\ButtonStyle;

class Label extends Element {

    /** @var array */
    protected $values;

    /** @var int */
    protected $button_id_value = 0;

    /**
     * 
     * @param UIWindow $window
     * @param string $name
     * @param string $label
     * @param int $height
     * @param int $width
     * @param int $top
     * @param int $left
     */
    function __construct(UIWindow $window, $name = '', $label = '', $height = 5, $width = 25, $top = 0, $left = 0) {
        parent::__construct($window, $name, $label, $height, $width, $top = 0, $left = 0);
    }

    /**
     * 
     * @param int $button_id
     */
    function show($button_id) {
        parent::show($button_id);

        $button = new IS_BTN();

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_RIGHT;
        $button->Text = '^7' . $this->label;

        $this->addButton($button);
        $this->send($button);

        $lft = $this->left + 1 + $button->W;

        if (count($this->values) > 0) {
            foreach ($this->values as $value) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $lft;
                $button->T = $this->top;
                $button->H = $this->height;
                $button->W = 10;
                $button->BStyle = 0;
                $button->Text = $value['value'];

                $this->button_id_value = $button->ReqI;
                $this->addButton($button);
                $this->send($button);

                $lft += $button->W;
            }
        }

        $this->end_id = $this->id_current;
    }

    /**
     * 
     * @param array $values
     */
    function setValues(array $values) {
        $index = 0;
        foreach ($values as $key => $value) {
            $this->values[$index] = array(
                'key' => $key,
                'value' => $value
            );

            $index++;
        }
    }

}
