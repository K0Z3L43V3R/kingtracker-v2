<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTN;
use AppBundle\Packet\IS_BTT;
use AppBundle\Packet\Packet;

class Element {

    /** @var UIWindow */
    protected $window;

    /** @var int */
    protected $id_current = 0;

    /** @var int */
    protected $end_id = 0;

    /** @var int */
    protected $start_id = 0;

    /** @var int */
    protected $top = 0;

    /** @var int */
    protected $top_offset = 0;

    /** @var int */
    protected $left = 0;

    /** @var int */
    protected $left_offset = 0;

    /** @var int */
    protected $height = 0;

    /** @var int */
    protected $width = 0;

    /** @var array */
    protected $buttons;

    /** @var string */
    protected $label;

    /** @var string */
    protected $name;

    /**
     * 
     * @param UIWindow $window
     * @param string $name
     * @param string $label
     * @param int $height
     * @param int $width
     * @param int $top
     * @param int $left
     */
    function __construct(UIWindow $window, $name = '', $label = '', $height = 5, $width = 20, $top = 0, $left = 0) {
        $this->top_offset = $top;
        $this->left_offset = $left;
        $this->height = $height;
        $this->width = $width;
        $this->name = $name;
        $this->label = $label;
        $this->window = $window;
    }

    /**
     * 
     * @param int $button_id
     */
    function show($button_id) {
        $this->start_id = $button_id;
        $this->id_current = $this->start_id;
    }

    function hide() {
        
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return boolean
     */
    public function eventClick(IS_BTC $packet) {
        if ($packet->ClickID >= $this->start_id && $packet->ClickID <= $this->end_id) {
            return true;
        }

        return false;
    }

    /**
     * 
     * @param IS_BTT $packet
     * @return boolean
     */
    public function eventType(IS_BTT $packet) {
        if ($packet->ClickID >= $this->start_id && $packet->ClickID <= $this->end_id) {
            return true;
        }

        return false;
    }

    /**
     * 
     * @param IS_BTN $button
     * @param callabl $callBack
     */
    public function addButton(IS_BTN $button, callable $callBack = null) {
        $this->buttons[$button->ReqI] = array(
            'button' => $button,
            'callBack' => $callBack
        );
    }

    /**
     * 
     * @param Packet $packet
     */
    public function send($packet) {
        $this->window->sendQueued($packet);
    }

    /**
     * 
     * @return int
     */
    public function getEndId() {
        return $this->end_id;
    }

    /**
     * 
     * @return int
     */
    public function getTop() {
        return $this->top;
    }

    /**
     * 
     * @return int
     */
    public function getLeft() {
        return $this->left;
    }

    /**
     * 
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * 
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set top left position
     * 
     * @param int $top
     * @param int $left
     */
    public function setPosition($top, $left = 0) {
        $this->top = $top;
        $this->left = $left;

        return $this;
    }
    
    public function setTop($top) {
        $this->top = $top;
        return $this;
    }

    public function setLeft($left) {
        $this->left = $left;
        return $this;
    }

    public function setHeight($height) {
        $this->height = $height;
        return $this;
    }

    public function setWidth($width) {
        $this->width = $width;
        return $this;
    }



}
