<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTN;
use AppBundle\Packet\IS_BTT;
use AppBundle\Types\ButtonStyle;

class BtnInput extends Element {

    /** @var array */
    protected $values;

    /** @var int */
    protected $button_id_value = 0;

    /** @var int */
    protected $key_value = -1;

    /** @var callable */
    protected $eventOptionChanged = null;

    /**
     * 
     * @param UIWindow $window
     * @param string $name
     * @param string $label
     * @param int $height
     * @param int $width
     * @param int $top
     * @param int $left
     */
    function __construct(UIWindow $window, $name = '', $label = '', $height = 5, $width = 30, $top = 0, $left = 0) {
        parent::__construct($window, $name, $label, $height, $width, $top, $left);
    }

    /**
     * 
     * @param int $button_id
     */
    function show($button_id) {
        parent::show($button_id);

        $button = new IS_BTN();
        $lft = 0;

        if (!empty($this->label)) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left;
            $button->T = $this->top;
            $button->H = $this->height;
            $button->W = $this->width;
            $button->BStyle = ButtonStyle::ISB_RIGHT;
            $button->Text = '^7' . $this->label;

            $this->addButton($button);
            $this->send($button);

            $lft = $this->left + 1 + $button->W;
        } else {
            $lft = $this->left + 1;
        }

        foreach ($this->values as $key => $value) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $lft;
            $button->T = $this->top;
            $button->H = $this->height;
            $button->W = 10;
            $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
            $button->TypeIn = 32;
            $button->Text = $value['value'];

            $this->values[$key]['id'] = $button->ReqI;
            $this->addButton($button, [$this, 'typeChangeValue']);
            $this->send($button);

            $lft += $button->W;
        }

        $this->end_id = $this->id_current;
    }

    /**
     * 
     * @param array $values
     * @param mixed|null $current
     * 
     * @return BtnInput
     */
    function setValues(array $values, $current = null) {
        $index = 0;
        foreach ($values as $key => $value) {
            $this->values[$index] = array(
                'key' => $key,
                'value' => $value
            );

            if ($key === $current) {
                $this->key_value = $index;
            }

            $index++;
        }

        return $this;
    }

    /**
     * 
     * @param callable $callback
     */
    public function setOptionChangedCallback(callable $callback) {
        $this->eventOptionChanged = $callback;
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return boolean
     */
    public function eventClick(IS_BTC $packet) {
        if (!parent::eventClick($packet)) {
            return false;
        }

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack'])) {
                call_user_func($this->buttons[$packet->ClickID]['callBack']);
            }
        }
    }

    function typeChangeValue($value, IS_BTT $packet) {
        if ($this->eventOptionChanged && isset($value['id']) && $value['id']) {
            $return = call_user_func($this->eventOptionChanged, $this->getName(), $value['key'], $packet->Text);

            $button = new IS_BTN();
            $button->ReqI = $value['id'];
            $button->ClickID = $value['id'];
            $button->Text = ButtonStyle::COL_WHITE . $return;
            $this->send($button);
        }
    }

    public function eventType(IS_BTT $packet) {
        if (!parent::eventType($packet)) {
            return false;
        }

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack'])) {
                $callBack = $this->buttons[$packet->ClickID]['callBack'];
                foreach ($this->values as $val) {
                    if ($val['id'] == $packet->ClickID) {
                        call_user_func($callBack, $val, $packet);
                        break;
                    }
                }
            }
        }
    }

}
