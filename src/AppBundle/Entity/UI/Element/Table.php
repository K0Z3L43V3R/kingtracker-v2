<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\Element\Element;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTN;
use AppBundle\Packet\IS_BTT;
use AppBundle\Types\ButtonStyle;
use Doctrine\Common\Collections\ArrayCollection;

class Table extends Element {

    /** @var ArrayCollection Table rows */
    protected $values;

    /** @var array Button ids of row/columns */
    protected $rowsButtons;

    /** @var array */
    protected $columns = [];

    /** @var array */
    protected $header = [];

    /** @var int Row line height */
    protected $lineHeight = 4;

    /** @var int Footer height */
    protected $footerHeight = 4;

    /** @var int */
    protected $linesPerPage = 10;

    /** @var int */
    protected $currentPage = 1;

    /** @var int */
    protected $maxPage = 1;

    /** @var string Footer status line */
    protected $statusLine = '';

    /** @var int */
    protected $widthFooter;

    /** @var int */
    protected $leftFooterOffset = 0;

    /** @var int Status line button id */
    protected $statusLineButtonId = 0;

    /** @var callable */
    protected $nextPageCallBack;

    /** @var callable */
    protected $prevPageCallBack;

    /** @var callable */
    protected $lastPageCallBack;

    /** @var callable */
    protected $firstPageCallBack;

    /**
     * Table UI element
     * 
     * @param UIWindow $window
     * @param string $name
     */
    function __construct(UIWindow $window, $name = 'table') {
        parent::__construct($window, $name);

        $this->values = new ArrayCollection();
    }

    /**
     * Show
     * 
     * @param int $button_id
     */
    function show($button_id) {
        parent::show($button_id);

        if ($this->widthFooter == null) {
            $this->widthFooter = $this->width;
        }

        $this->statusLine = $this->getCurrentPage() . ' / ' . $this->getMaxPage();

        $button = new IS_BTN();
        $newLine = 0;

        // Header
        if (count($this->header) > 0) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->T = $this->getTop() + $newLine;
            $button->L = $this->getLeft();
            $button->H = $this->getLineHeight();
            $button->W = $this->getWidth() + 1;
            $button->BStyle = ButtonStyle::ISB_DARK;
            $button->Text = '';
            $this->addButton($button);
            $this->send($button);

            $firstColumn = true;
            foreach ($this->getHeader() as $columns) {
                
                foreach ($columns as $column) {
                    $button->ReqI = ++$this->id_current;
                    $button->ClickID = $button->ReqI;
                    $button->T = $this->getTop() + $newLine;
                    $button->L = $firstColumn ? $this->getLeft() : ($button->L + $button->W);
                    $button->H = $this->getLineHeight();
                    $button->W = isset($column['width']) ? $column['width'] : $this->getWidth() + 1;
                    $button->BStyle = ButtonStyle::ISB_DARK;
                    $button->Text = $column['title'];
                    $this->addButton($button);
                    $this->send($button);
                    
                    $firstColumn = false;
                }
            }

            $newLine += $this->getLineHeight();
        }

        // Show values

        $rowIndex = 0;
        foreach ($this->values as $key => $row) {
            $firstColumn = true;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $firstColumn ? $this->getLeft() : ($button->L + $button->W);
                $button->T = $this->getTop() + $newLine;
                $button->H = $this->getLineHeight();
                $button->W = isset($column['width']) ? $column['width'] : $this->getWidth();
                $button->BStyle = isset($column['style']) ? $column['style'] : 0;
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->rowsButtons[$rowIndex][$keyCol] = $button->ClickID;

                $this->addButton($button);
                $this->send($button);

                $firstColumn = false;
            }
            $rowIndex++;
            $newLine += $this->getLineHeight();
        }

        // Empty lines
        $emptyLines = $this->getLinesPerPage() - count($this->values);

        for ($i = 0; $i < $emptyLines; $i++) {
            $firstColumn = true;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $firstColumn ? $this->getLeft() : ($button->L + $button->W);
                $button->T = $this->getTop() + $newLine;
                $button->H = $this->getLineHeight();
                $button->W = isset($column['width']) ? $column['width'] : $this->getWidth();
                $button->BStyle = isset($column['style']) ? $column['style'] : 0;
                $button->Text = '';

                $this->rowsButtons[$rowIndex][$keyCol] = $button->ClickID;

                $this->addButton($button);
                $this->send($button);

                $firstColumn = false;
            }
            $rowIndex++;
            $newLine += $this->getLineHeight();
        }

        // Footer
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->T = $this->top + ($this->getHeight() - $this->getLineHeight());
        $button->L = $this->left + $this->leftFooterOffset;
        $button->H = $this->footerHeight;
        $button->W = $this->widthFooter;
        $button->BStyle = ButtonStyle::ISB_DARK;
        $button->Text = '';
        $this->addButton($button);
        $this->send($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->H = $this->footerHeight;
        $button->W = 10;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
        $button->Text = '<<<';
        $this->send($button);
        $this->addButton($button, [$this, 'firstPageClicked']);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = $this->footerHeight;
        $button->W = 10;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
        $button->Text = '<';
        $this->send($button);
        $this->addButton($button, [$this, 'prevPageClicked']);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = $this->footerHeight;
        $button->W = $this->widthFooter - 40;
        $button->BStyle = ButtonStyle::ISB_CLICK;
        $button->Text = $this->statusLine;
        $button->TypeIn = 10;
        $this->send($button);
        $this->addButton($button, [$this, 'pageTypeInEvent']);
        $this->statusLineButtonId = $button->ClickID;
        $button->TypeIn = 0;

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = $this->footerHeight;
        $button->W = 10;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
        $button->Text = '>';
        $this->send($button);
        $this->addButton($button, [$this, 'nextPageClicked']);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = $this->footerHeight;
        $button->W = 10;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
        $button->Text = '>>>';
        $this->send($button);
        $this->addButton($button, [$this, 'lastPageClicked']);

        $this->end_id = $this->id_current;
    }

    /**
     * Redraw buttons values
     */
    function update() {
        $this->statusLine = $this->getCurrentPage() . ' / ' . $this->getMaxPage();

        $button = new IS_BTN();

        // Status line
        $button->ClickID = $this->statusLineButtonId;
        $button->Text = $this->statusLine;
        $this->send($button);

        // Values
        foreach ($this->rowsButtons as $rowId => $row) {
            foreach ($row as $columnId => $buttonId) {
                $button->ClickID = $buttonId;
                $button->Text = isset($this->values[$rowId][$columnId]) ? $this->values[$rowId][$columnId] : '';
                $this->send($button);
            }
        }
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return boolean
     */
    public function eventClick(IS_BTC $packet) {
        if (!parent::eventClick($packet)) {
            return false;
        }

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack']) && is_callable($this->buttons[$packet->ClickID]['callBack'])) {
                call_user_func($this->buttons[$packet->ClickID]['callBack']);
            }
        }
    }

    /**
     * 
     * @param IS_BTT $packet
     * @return boolean
     */
    public function eventType(IS_BTT $packet) {
        if (!parent::eventType($packet)) {
            return false;
        }

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack']) && is_callable($this->buttons[$packet->ClickID]['callBack'])) {
                call_user_func($this->buttons[$packet->ClickID]['callBack'], $packet);
            }
        }
    }

    /**
     * Event called when next page button was clicked
     */
    protected function nextPageClicked() {
        $this->currentPage += 1;
        $this->currentPage = $this->currentPage > $this->maxPage ? $this->maxPage : $this->currentPage;

        if (is_callable($this->nextPageCallBack)) {
            call_user_func($this->nextPageCallBack, $this->currentPage);
        }

        $this->update();
    }

    /**
     * Event called when previous page button was clicked
     */
    protected function prevPageClicked() {
        $this->currentPage -= 1;
        $this->currentPage = $this->currentPage < 1 ? 1 : $this->currentPage;

        if (is_callable($this->prevPageCallBack)) {
            call_user_func($this->prevPageCallBack, $this->currentPage);
        }

        $this->update();
    }

    /**
     * Event called when last page button was clicked
     */
    protected function lastPageClicked() {
        $this->currentPage = $this->maxPage;

        if (is_callable($this->lastPageCallBack)) {
            call_user_func($this->lastPageCallBack, $this->currentPage);
        }

        $this->update();
    }

    /**
     * Event called when first page button was clicked
     */
    protected function firstPageClicked() {
        $this->currentPage = 1;
        $this->currentPage = $this->currentPage > $this->maxPage ? $this->maxPage : $this->currentPage;

        if (is_callable($this->firstPageCallBack)) {
            call_user_func($this->firstPageCallBack, $this->currentPage);
        }

        $this->update();
    }

    /**
     * Event called when page number was typed in
     * 
     * @param IS_BTT $packet
     */
    protected function pageTypeInEvent(IS_BTT $packet) {
        $this->currentPage = intval($packet->Text);
        $this->currentPage = $this->currentPage > $this->maxPage ? $this->maxPage : $this->currentPage;
        $this->currentPage = $this->currentPage < 1 ? 1 : $this->currentPage;

        if (is_callable($this->lastPageCallBack)) {
            call_user_func($this->lastPageCallBack, $this->currentPage);
        }

        $this->update();
    }

    /**
     * Set table row values
     * 
     * @param array $values
     */
    public function setValues(array $values) {
        $this->values->clear();

        foreach ($values as $value) {
            $this->values->add($value);
        }
    }

    /**
     * Get table rows values
     * 
     * @return ArrayCollection
     */
    public function getValues(): ArrayCollection {
        return $this->values;
    }

    /**
     * Set columns definition
     * 
     * [
     *      column_key => [
     *          width       => width of the column,
     *          style       => ButtonStyle
     *      ],
     *      ...
     * ]
     * 
     * @param array $columns
     */
    public function setColumns(array $columns) {
        $this->columns = $columns;
    }

    /**
     * 
     * @return array
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * 
     * @param array $header
     * @return Table
     */
    public function setHeader($header) {
        $this->header = $header;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getLineHeight() {
        return $this->lineHeight;
    }

    /**
     * 
     * @param int $lineHeight
     * @return Table
     */
    public function setLineHeight($lineHeight) {
        $this->lineHeight = $lineHeight;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getFooterHeight() {
        return $this->footerHeight;
    }

    /**
     * 
     * @param int $footerHeight
     * @return Table
     */
    public function setFooterHeight($footerHeight) {
        $this->footerHeight = $footerHeight;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getWidthFooter() {
        return $this->widthFooter;
    }

    /**
     * 
     * @param int $widthFooter
     * @return Table
     */
    public function setWidthFooter($widthFooter) {
        $this->widthFooter = $widthFooter;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getLeftFooterOffset() {
        return $this->leftFooterOffset;
    }

    /**
     * 
     * @param int $leftFooterOffset
     * @return Table
     */
    public function setLeftFooterOffset($leftFooterOffset) {
        $this->leftFooterOffset = $leftFooterOffset;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getLinesPerPage() {
        return $this->linesPerPage;
    }

    /**
     * 
     * @return int
     */
    public function getCurrentPage() {
        return $this->currentPage;
    }

    /**
     * 
     * @param int $linesPerPage
     * @return Table
     */
    public function setLinesPerPage($linesPerPage) {
        $this->linesPerPage = $linesPerPage;
        return $this;
    }

    /**
     * 
     * @param int $currentPage
     * @return Table
     */
    public function setCurrentPage($currentPage) {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getMaxPage() {
        return $this->maxPage;
    }

    /**
     * 
     * @param int $maxPage
     * @return Table
     */
    public function setMaxPage($maxPage) {
        $this->maxPage = $maxPage;
        return $this;
    }

    /**
     * 
     * @param callable $nextPageCallBack
     * @return Table
     */
    public function setNextPageCallBack($nextPageCallBack) {
        $this->nextPageCallBack = $nextPageCallBack;
        return $this;
    }

    /**
     * 
     * @param callable $prevPageCallBack
     * @return Table
     */
    public function setPrevPageCallBack($prevPageCallBack) {
        $this->prevPageCallBack = $prevPageCallBack;
        return $this;
    }

    /**
     * 
     * @param callable $lastPageCallBack
     * @return Table
     */
    public function setLastPageCallBack($lastPageCallBack) {
        $this->lastPageCallBack = $lastPageCallBack;
        return $this;
    }

    /**
     * 
     * @param callable $firstPageCallBack
     * @return Table
     */
    public function setFirstPageCallBack($firstPageCallBack) {
        $this->firstPageCallBack = $firstPageCallBack;
        return $this;
    }

}
