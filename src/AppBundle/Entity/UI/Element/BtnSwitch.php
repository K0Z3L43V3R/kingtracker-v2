<?php

namespace AppBundle\Entity\UI\Element;

use AppBundle\Entity\UI\UIWindow;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTN;
use AppBundle\Types\ButtonStyle;

class BtnSwitch extends Element {

    /** @var array */
    protected $values;

    /** @var int */
    protected $button_id_value = 0;

    /** @var int */
    protected $key_value = -1;

    /** @var callable */
    protected $eventOptionChanged = null;

    /**
     * 
     * @param UIWindow $window
     * @param string $name
     * @param string $label
     * @param int $height
     * @param int $width
     * @param int $top
     * @param int $left
     */
    function __construct(UIWindow $window, $name = '', $label = '', $height = 5, $width = 30, $top = 0, $left = 0) {
        parent::__construct($window, $name, $label, $height, $width, $top, $left);
    }

    /**
     * 
     * @param int $button_id
     */
    function show($button_id) {
        parent::show($button_id);

        $button = new IS_BTN();

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_RIGHT;
        $button->Text = '^7' . $this->label;

        $this->addButton($button);
        $this->send($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1 + $button->W;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = 10;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::ISB_CLICK;
        $button->Text = isset($this->values[$this->key_value]) ? $this->values[$this->key_value]['value'] : '^8---';

        $this->button_id_value = $button->ReqI;
        $this->addButton($button, [$this, 'clickChangeValue']);
        $this->send($button);

        $this->end_id = $this->id_current;
    }

    /**
     * 
     * @param array $values
     * @param mixed|null $current
     * @return BtnSwitch
     */
    function setValues(array $values, $current = null) {
        $index = 0;
        foreach ($values as $key => $value) {
            $this->values[$index] = array(
                'key' => $key,
                'value' => $value
            );

            if ($key == $current) {
                $this->key_value = $index;
            }

            $index++;
        }

        return $this;
    }

    /**
     * 
     * @param callable $callback
     */
    public function setOptionChangedCallback(callable $callback) {
        $this->eventOptionChanged = $callback;
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return boolean
     */
    public function eventClick(IS_BTC $packet) {
        if (!parent::eventClick($packet)) {
            return false;
        }

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack'])) {
                call_user_func($this->buttons[$packet->ClickID]['callBack']);
            }
        }
    }

    public function clickChangeValue() {
        $this->key_value++;
        if ($this->key_value > (count($this->values) - 1)) {
            $this->key_value = 0;
        }

        // Update button
        $button = new IS_BTN();
        $button->ReqI = $this->button_id_value;
        $button->ClickID = $this->button_id_value;
        $button->Text = $this->values[$this->key_value]['value'];
        $this->send($button);

        if ($this->eventOptionChanged) {
            call_user_func($this->eventOptionChanged, $this->getName(), $this->values[$this->key_value]['key'], $this->values[$this->key_value]['value']);
        }
    }

}
