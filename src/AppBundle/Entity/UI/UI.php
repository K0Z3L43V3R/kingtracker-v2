<?php

namespace AppBundle\Entity\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Event\SendPacketEvent;
use AppBundle\Event\SendQueuedPacketEvent;
use AppBundle\Packet\IS_BFN;
use AppBundle\Packet\Packet;
use AppBundle\Types\ButtonFunction;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI
 */
class UI {

    /** @var boolean */
    protected $displayed = false;

    /** @var int */
    protected $id_start;

    /** @var int */
    protected $id_current;

    /** @var int */
    protected $id_end;

    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /** @var int */
    protected $left;

    /** @var int */
    protected $top;

    /** @var int */
    protected $left_offset;

    /** @var int */
    protected $top_offset;

    /** @var EventDispatcherInterface */
    protected $eventDispacher;

    /** @var Player */
    protected $player;

    /** @var Host */
    protected $host;

    /** @var LFSTranslator|null */
    protected $translator;

    /**
     * @param EventDispatcherInterface $eventDispacher
     * @param int $player
     * @param int $hostId
     * @param LFSTranslator|null $translator
     */
    public function __construct(EventDispatcherInterface $eventDispacher, Player $player, Host $host, LFSTranslator $translator = null, $id_start = null) {
        $this->eventDispacher = $eventDispacher;
        $this->player = $player;
        $this->host = $host;
        $this->translator = $translator;
        $this->id_current = $id_start - 1;
    }

    public function show(){
        $this->displayed = true;
    }

    public function close() {
        $this->displayed = false;
        
        $bfn = new IS_BFN();
        $bfn->SubT = ButtonFunction::BFN_DEL_BTN;
        $bfn->ClickID = $this->id_start;
        $bfn->ClickMax = $this->id_end;
        
        $this->send($bfn);
    }

    public function tick(){
        
    }

    /**
     * @param Packet $packet
     */
    public function send(Packet $packet) {
        $packet->UCID = $this->player->getHostConnection()->getUCID();
        $this->eventDispacher->dispatch(SendPacketEvent::NAME, new SendPacketEvent(clone $packet, $this->host->getId()));
    }

    public function sendQueued(Packet $packet) {
        $packet->UCID = $this->player->getHostConnection()->getUCID();
        $this->eventDispacher->dispatch(SendQueuedPacketEvent::NAME, new SendQueuedPacketEvent(clone $packet, $this->player->getHostConnection()->getUCID(), $this->host->getId()));
    }
    
    /**
     * 
     * @return int
     */
    public function getCurrentId() {
        return $this->id_current;
    }

    /**
     * 
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * 
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * 
     * @return int
     */
    public function getLeft() {
        return $this->left;
    }

    /**
     * 
     * @return int
     */
    public function getTop() {
        return $this->top;
    }

    /**
     * 
     * @return int
     */
    public function getLeftOffset() {
        return $this->left_offset;
    }

    /**
     * 
     * @return int
     */
    public function getTopOffset() {
        return $this->top_offset;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setWidth($width) {
        $this->width = $width;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setHeight($height) {
        $this->height = $height;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setLeft($left) {
        $this->left = $left;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setTop($top) {
        $this->top = $top;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setLeftOffset($left_offset) {
        $this->left_offset = $left_offset;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return $this
     */
    public function setTopOffset($top_offset) {
        $this->top_offset = $top_offset;
        return $this;
    }

    /**
     * Set top left position
     * 
     * @param int $top
     * @param int $left
     * @return $this
     */
    public function setPosition($top, $left = 0) {
        $this->top_offset = $top;
        $this->left_offset = $left;

        return $this;
    }

    /**
     * Set position to center
     * 
     * @return $this
     */
    public function setCenter() {
        $this->left = (int) ((200 - $this->width) / 2);

        return $this;
    }

    /**
     * 
     * @return LFSTranslator
     */
    public function getTranslator() {
        return $this->translator;
    }

    /**
     * 
     * @param LFSTranslator $translator
     * @return $this
     */
    public function setTranslator(LFSTranslator $translator) {
        $this->translator = $translator;
        return $this;
    }
    
    public function setIdStart($id_start) {
        $this->id_start = $id_start;
        $this->id_current = $id_start - 1;
        return $this;
    }

    /**
     * 
     * @param string $id
     * @param array $parameters
     * @param string|null $domain
     * @param string|null $locale
     * 
     * @return string
     */
    public function trans($id, array $parameters = array(), $domain = null, $locale = null) {
        if ($this->translator instanceof LFSTranslator) {
            return $this->translator->trans($id, $parameters, $domain, $locale);
        }

        return $id;
    }

}
