<?php

namespace AppBundle\Entity\UI;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Helper\TimeHelper;
use AppBundle\Packet\IS_BTN;
use AppBundle\Types\ButtonStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * UI RaceControl
 */
class UIRaceControl extends UI {

    /** @var string */
    protected $title;
    public $redrawTitle = false;
    private $timer = 0;
    private $timer_init_value = 0;
    private $timer_sec_last = 0;
    private $destroyEvent;
    private $delayedShow = false;
    private $button_id_title;
    private $button_id_timer;

    /**
     * 
     * @param EventDispatcherInterface $eventDispacher
     * @param Player $player
     * @param Host $host
     * @param int $id_start
     * @param int $id_end
     */
    public function __construct(EventDispatcherInterface $eventDispacher, Player $player, Host $host, $id_start = 1, $id_end = 99) {
        parent::__construct($eventDispacher, $player, $host, $player->getTranslator(), $id_start);

        $this->id_start = $id_start;
        $this->id_end = $id_end;
        $this->width = 25;
        $this->top = 10;
        $this->left = (int) ((200 - $this->width) / 2);
    }

    public function show() {
        $this->displayed = true;

        $button = new IS_BTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = 12;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY;
        $button->Text = '';
        $this->sendQueued($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = 4;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::ISB_DARK + ButtonStyle::COLOUR_LIGHT_GREY;
        $button->Text = ButtonStyle::COL_WHITE . $this->title;
        $this->button_id_title = $button->ReqI;
        $this->sendQueued($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top + $button->H;
        $button->H = 8;
        $button->W = $this->width;
        $button->BStyle = ButtonStyle::COLOUR_LIGHT_GREY;
        $button->Text = ButtonStyle::COL_WHITE . TimeHelper::secToString($this->timer);
        $this->button_id_timer = $button->ReqI;
        $this->sendQueued($button);

        $this->id_end = $button->ClickID;

        return $this;
    }

    public function redrawTimer() {
        if (!$this->button_id_timer)
            return;

        $button = new IS_BTN();
        $button->ReqI = $this->button_id_timer;
        $button->ClickID = $this->button_id_timer;
        $button->Text = ButtonStyle::COL_WHITE . TimeHelper::secToString($this->timer);

        $this->sendQueued($button);

        if ($this->redrawTitle) {
            $this->redrawTitle();
        }
    }

    public function redrawTitle() {
        if (!$this->button_id_title)
            return;

        $button = new IS_BTN();
        $button->ReqI = $this->button_id_title;
        $button->ClickID = $this->button_id_title;
        $button->Text = ButtonStyle::COL_WHITE . $this->title;

        $this->sendQueued($button);

        $this->redrawTitle = false;
    }

    public function tick() {
        if (time() - $this->timer_sec_last >= 1) {
            $this->timer--;

            if (!$this->displayed && $this->delayedShow > 0 && $this->delayedShow == $this->timer) {
                $this->show();
            }

            if ($this->timer < 0 && $this->destroyEvent) {
                call_user_func($this->destroyEvent);
            } else {
                if ($this->displayed && $this->timer >= 0) {
                    $this->redrawTimer();
                } else {
                    if ($this->timer <= ($this->timer_init_value - 20)) {
                        $this->show();
                    }
                }
            }

            $this->timer_sec_last = time();
        }
    }

    /**
     * Set title
     * 
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Set top left position
     * 
     * @param int $top
     * @param int $left
     * @return $this
     */
    public function setPosition($top, $left = 0) {
        $this->top = $top;
        $this->left = $left;

        return $this;
    }

    /**
     * 
     * @param int $countDown
     * @param callable $destroyEvent
     * @param int $delayedShow
     */
    public function setData($countDown, $destroyEvent, $delayedShow = 0) {
        $this->timer = $countDown + 1;
        $this->timer_init_value = $this->timer;
        $this->destroyEvent = $destroyEvent;
        $this->delayedShow = $delayedShow;
    }

}
