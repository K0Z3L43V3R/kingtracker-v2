<?php

namespace AppBundle\Entity\UI;

use AppBundle\Entity\UI\Element\Element;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTT;

/**
 * UI Tab
 */
class UITab extends UI {

    /** @var Element[] */
    protected $elements = [];

    /** @var string */
    protected $name;

    /** @var UIWindow */
    protected $window;

    public function __construct($name, UIWindow $window) {
        parent::__construct($window->eventDispacher, $window->player, $window->host);

        $this->name = $name;
        $this->window = $window;
        $this->id_start = $window->id_current;
        $this->translator = $window->getTranslator();
    }

    /**
     * @param string $name
     * @param Element $element
     * 
     * @return Element
     */
    public function addElement(Element $element) {
        $this->elements[] = $element;

        return $element;
    }

    /**
     * 
     * @param string $name
     * @return Element|null
     */
    public function findElement($name) {
        foreach ($this->elements as $element) {
            if ($name === $element->getName()) {
                return $element;
            }
        }

        return null;
    }

    /**
     * 
     * @param int $button_id
     * @return $this
     */
    public function show() {
        $this->id_current = $this->id_start;

        $this->displayed = true;

        $this->top = $this->window->getTop() + $this->top_offset;
        $this->left = $this->window->getLeft() + $this->left_offset;

        $topOffset = 0;
        foreach ($this->elements as $element) {
            $element->setPosition(($this->top + $topOffset), $this->left);
            $element->show($this->id_current);
            $this->id_current = $element->getEndId();

            $topOffset += $element->getHeight() + 1;
        }

        $this->id_end = $this->id_current;

        return $this;
    }

    public function close() {
        foreach ($this->elements as $element) {
            unset($element);
        }

        $this->displayed = false;

        parent::close();
    }

    /**
     * 
     * @param IS_BTC $packet
     * @return bool
     */
    public function eventClick(IS_BTC $packet) {
        if (!$this->displayed) {
            return false;
        }

        foreach ($this->elements as $element) {
            $element->eventClick($packet);
        }
    }

    /**
     * 
     * @param IS_BTT $packet
     * @return bool
     */
    public function eventType(IS_BTT $packet) {
        if (!$this->displayed) {
            return false;
        }

        foreach ($this->elements as $element) {
            $element->eventType($packet);
        }
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

}
