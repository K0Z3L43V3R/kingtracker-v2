<?php

namespace AppBundle\Entity;

use AppBundle\Component\LFSTranslator;
use AppBundle\Entity\HostConnection;
use AppBundle\Entity\HostSetting;
use AppBundle\Entity\Timer;
use AppBundle\Service\CommandService;
use AppBundle\Service\HostService;
use AppBundle\Service\InsimService;
use AppBundle\Service\PlayerService;
use AppBundle\Service\TimerService;
use AppBundle\Types\Timing;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Host
 *
 * @ORM\Table(name="host")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HostRepository")
 */
class Host {

    const STATE_RACE_INPROGRESS = 1;
    const STATE_RACE_FINISHED = 2;
    const STATE_QUALIFY_INPROGRESS = 3;
    const STATE_QUALIFY_FINISHED = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true, options={"default" : "localhost"})
     */
    private $ip;

    /**
     * @var int
     *
     * @ORM\Column(name="port", type="integer", nullable=true, options={"default" : "29999"})
     */
    private $port;

    /**
     * @var int
     *
     * @ORM\Column(name="udp_port", type="integer", nullable=true, options={"default" : "0"})
     */
    private $udpPort;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true, options={"default" : ""})
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="preffix", type="string", length=1, nullable=true, options={"default" : "!"})
     */
    private $preffix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default" : "1"})
     */
    private $active;

    /**
     * @var boolean
     *
     * 
     * @ORM\Column(name="local", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $local;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true, options={"default" : "0"})
     */
    private $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="connected_at", type="datetime", nullable=true)
     */
    private $connectedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="Track", type="string", length=255, nullable=true, options={"default" : ""})
     */
    private $Track;

    /**
     * @var string
     *
     * @ORM\Column(name="LName", type="string", length=255, nullable=true, options={"default" : ""})
     */
    private $LName;

    /**
     * @var int
     *
     * @ORM\Column(name="Wind", type="integer", nullable=true, options={"default" : "0"})
     */
    private $Wind;

    /**
     * Host connections
     * 
     * @ORM\OneToMany(targetEntity="HostConnection", mappedBy="host")
     */
    private $hostConnections;

    /**
     * Host settings
     * 
     * @ORM\OneToMany(targetEntity="HostSetting", mappedBy="host", cascade={"persist"})
     */
    private $settings;

    /** @var Track */
    private $trackObj;

    /** @var Timing */
    private $timing;

    /** @var InsimService */
    public $insim;

    /** @var HostService */
    public $hostService;

    /** @var PlayerService */
    public $playerService;

    /** @var CommandService */
    public $commandService;

    /** @var LFSTranslator */
    public $translator;

    /** @var TimerService */
    public $timerService;

    /** @var int Host state */
    public $state = 0;

    /**
     * Host
     * 
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Result", cascade={"persist"})
     * @ORM\JoinColumn(name="result_id", referencedColumnName="id")
     */
    private $result;

    /**
     * Constructor
     */
    public function __construct() {
        $this->hostConnections = new ArrayCollection();
        $this->settings = new ArrayCollection();
    }

    /**
     * Tick
     */
    public function tick() {
        if ($this->insim !== null) {
            $this->insim->tick();
        }

        if ($this->hostService !== null) {
            $this->hostService->tick();
        }

        if ($this->playerService !== null) {
            $this->playerService->tick();
        }
        
        if ($this->timerService !== null) {
            $this->timerService->tick();
        }
    }

    // Getters & Setters
    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Host
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Host
     */
    public function setIp($ip) {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * Set port
     *
     * @param integer $port
     *
     * @return Host
     */
    public function setPort($port) {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port
     *
     * @return int
     */
    public function getPort() {
        return $this->port;
    }

    /**
     * Set status
     *
     * @param int $status
     *
     * @return Host
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set Insim
     *
     * @param InsimService $service
     *
     * @return Host
     */
    public function setInsim(InsimService $service) {
        $this->insim = $service;
        $this->insim->host = &$this;

        return $this;
    }

    /**
     * Set host service
     *
     * @param HostService $service
     *
     * @return Host
     */
    public function setHostService(HostService $service) {
        $this->hostService = $service;
        $this->hostService->host = &$this;

        return $this;
    }

    /**
     * Set player service
     *
     * @param PlayerService $service
     *
     * @return Host
     */
    public function setPlayerService(PlayerService $service) {
        $this->playerService = $service;
        $this->playerService->host = &$this;

        return $this;
    }
    
    /**
     * 
     * @return PlayerService
     */
    public function getPlayerService() {
        return $this->playerService;
    }

    
    /**
     * Set command service
     * 
     * @param CommandService $commandService
     * 
     * @return Host
     */
    public function setCommandService(CommandService $commandService) {
        $this->commandService = $commandService;
        $this->commandService->host = &$this;

        return $this;
    }

    public function getActive() {
        return $this->active;
    }

    public function isLocal() {
        return $this->local;
    }

    public function getConnectedAt(): DateTime {
        return $this->connectedAt;
    }

    public function getPath() {
        return $this->path;
    }

    public function getLName() {
        return $this->LName;
    }

    public function getWind() {
        return $this->Wind;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setIsLocal($local) {
        $this->local = $local;
    }

    public function setConnectedAt(DateTime $connectedAt) {
        $this->connectedAt = $connectedAt;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function setLName($LName) {
        $this->LName = $LName;

        if ($this->getTrack() !== null) {
            $this->getTrack()->setLName($LName);
        }
    }

    public function setWind($wind) {
        $this->Wind = $wind;

        if ($this->getTrack() !== null) {
            $this->getTrack()->setWind($wind);
        }
    }

    public function getPassword() {
        return $this->password;
    }

    public function getPreffix() {
        return $this->preffix;
    }

    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    public function setPreffix($preffix) {
        $this->preffix = $preffix;

        return $this;
    }

    public function getUdpPort() {
        return $this->udpPort;
    }

    public function setUdpPort($udpPort) {
        $this->udpPort = $udpPort;

        return $this;
    }

    public function getTranslator(): LFSTranslator {
        return $this->translator;
    }

    public function setTranslator(LFSTranslator $translator) {
        $this->translator = $translator;
    }

    public function getTiming(): Timing {
        return $this->timing;
    }

    public function setTiming(Timing $timing) {
        $this->timing = $timing;

        if ($timing->getRaceLaps() > 0) {
            $this->state = self::STATE_RACE_INPROGRESS;
        }

        if ($timing->getRaceLaps() == 0 && $timing->getQualMins() > 0) {
            $this->state = self::STATE_QUALIFY_INPROGRESS;
        }
    }

    /**
     * @return Track|null
     */
    public function getTrack() {
        return $this->trackObj;
    }

    public function setTrack(Track $track) {
        $this->trackObj = $track;
        $this->Track = $track->getShortName();
        $this->Wind = $track->getWind()->getValue();

        return $this;
    }

    public function getState() {
        return $this->state;
    }

    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    /**
     * 
     * @return TimerService
     */
    public function getTimerService() {
        return $this->timerService;
    }

    /**
     * 
     * @param TimerService $timerService
     * @return Host
     */
    public function setTimerService(TimerService $timerService) {
        $this->timerService = $timerService;
        $this->timerService->host = &$this;

        return $this;
    }

    /**
     * Add hostConnection
     *
     * @param HostConnection $hostConnection
     *
     * @return Host
     */
    public function addHostConnection(HostConnection $hostConnection) {
        $this->hostConnections[] = $hostConnection;

        return $this;
    }

    /**
     * Remove hostConnection
     *
     * @param HostConnection $hostConnection
     */
    public function removeHostConnection(HostConnection $hostConnection) {
        $this->hostConnections->removeElement($hostConnection);
    }

    /**
     * Get hostConnections
     *
     * @return Collection
     */
    public function getHostConnections() {
        return $this->hostConnections;
    }

    /**
     * Get host settings
     *
     * @return HostSetting[]
     */
    public function getSettings() {
        return $this->settings;
    }

    /**
     * 
     * @return Result
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * 
     * @param Result $result
     * @return Host
     */
    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    // Actions
    /**
     * 
     * @param string $name
     * @return boolean
     */
    public function hasSetting($name) {
        foreach ($this->settings as $setting) {
            if ($setting->getName() === $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param string $name
     * @return HostSetting|null
     */
    public function getSetting($name) {
        foreach ($this->settings as $setting) {
            if ($setting->getName() === $name) {
                return $setting;
            }
        }

        return null;
    }

    /**
     * 
     * @param string $name
     * @param mixed $value
     * @param array|null $data
     */
    public function updateSetting($name, $value, $data = null) {
        if ($this->hostService !== null) {
            $this->hostService->updateHostSetting($name, $value, $data);
        }
    }

    /**
     * 
     * @param HostSetting $setting
     * @return HostSetting
     */
    public function addSetting(HostSetting $setting) {
        $setting->setHost($this);
        $this->settings[] = $setting;

        return $setting;
    }

    /**
     * 
     * @param string $command
     */
    public function sendCommand($command) {
        if ($this->hostService !== null) {
            $this->hostService->sendCommand($command);
        }
    }
    
    /**
     * 
     * @param Timer $timer
     */
    public function addTimer(Timer $timer){
        
        if($this->timerService !== null){
            $this->timerService->addTimer($timer);
        }
    }
    
    /**
     * Restart host
     */
    public function restart(){
        $this->sendCommand("/restart");
    }

    public function destroy() {
        unset($this->commandService);
        unset($this->playerService);
        unset($this->hostService);
    }

}
