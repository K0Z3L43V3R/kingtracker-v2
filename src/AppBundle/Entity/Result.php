<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Host;
use AppBundle\Entity\ResultPlayer;
use AppBundle\Entity\ResultEvent;
use AppBundle\Packet\IS_LAP;
use AppBundle\Packet\IS_RES;
use AppBundle\Packet\IS_RST;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Result
 *
 * @ORM\Table(name="result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResultRepository")
 */
class Result {

    const TYPE_RACE = 'RACE';
    const TYPE_QUALIFY = 'QUALIFY';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Host
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Host")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * Players results
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ResultPlayer", mappedBy="result", cascade={"persist"})
     */
    private $players;

    /**
     * Result events (lap, split, ...)
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ResultEvent", mappedBy="result", cascade={"persist"})
     */
    private $events;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var array
     *
     * @ORM\Column(name="info", type="json_array", nullable=true)
     */
    private $info;

    /**
     * @var int
     *
     * @ORM\Column(name="finished", type="integer", nullable=true, options={"default" : "0"})
     */
    private $finished = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @param Host $host
     * @param IS_RST|null $packet
     */
    public function __construct(Host $host, IS_RST $packet = null) {
        $this->players = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->host = $host;

        if ($packet !== null) {
            $this->createdAt = new DateTime();
            $this->type = $host->getState() === Host::STATE_RACE_INPROGRESS ? self::TYPE_RACE : self::TYPE_QUALIFY;
            $this->info = [
                'Flags' => $packet->Flags,
                'NumP' => $packet->NumP,
                'QualMins' => $packet->QualMins,
                'RaceLaps' => $packet->RaceLaps,
                'Timing' => $packet->Timing,
                'Track' => $packet->Track,
                'Weather' => $packet->Weather,
                'Wind' => $packet->Wind
            ];
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Result
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set info
     *
     * @param array $info
     *
     * @return Result
     */
    public function setInfo($info) {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return array
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * Set finished
     *
     * @param integer $finished
     *
     * @return Result
     */
    public function setFinished($finished) {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return int
     */
    public function getFinished() {
        return $this->finished;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Result
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * 
     * @return Host
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * 
     * @param Host $host
     * @return Result
     */
    public function setHost($host) {
        $this->host = $host;
        return $this;
    }

    /**
     * 
     * @return ArrayCollection
     */
    public function getPlayerResults() {
        return $this->players;
    }

    /**
     * Add hostConnection
     *
     * @param Player $player
     * @param IS_RES $packet
     *
     * @return ResultPlayer
     */
    public function addPlayerResult(Player $player, IS_RES $packet) {
        $result = null;
        foreach ($this->players as $r) {
            if ($r->getPlayer()->getId() === $player->getId()) {
                $result = $r;
                $this->players->removeElement($r);
                break;
            }
        }

        if ($result === null) {
            $result = new ResultPlayer();
            $result->setCreatedAt(new \DateTime);
            $result->setPlayer($player);
            $result->setResult($this);
        }

        $result->setBTime($packet->BTime);
        $result->setCar($packet->CName);
        $result->setConfirmation($packet->Confirm);
        $result->setFlags($packet->Flags);
        $result->setLapsDone($packet->LapsDone);
        $result->setNumStops($packet->NumStops);
        $result->setPSeconds($packet->PSeconds);
        $result->setPosition($packet->ResultNum + 1);
        $result->setTTime($packet->TTime);

        $this->players[] = $result;

        return $result;
    }

    /**
     * Remove hostConnection
     *
     * @param ResultPlayer $result
     */
    public function removePlayerResult(ResultPlayer $result) {
        $this->players->removeElement($result);
    }

    /**
     * 
     * @return ArrayCollection
     */
    public function getEvents() {
        return $this->events;
    }

    /**
     * Add lap event
     *
     * @param Player $player
     * @param IS_LAP $packet
     *
     * @return ResultEvent
     */
    public function addLapEvent(Player $player, IS_LAP $packet) {
        $event = new ResultEvent();
        $event->setCreatedAt(new \DateTime);
        $event->setType(ResultEvent::TYPE_LAP);
        $event->setPlayer($player);
        $event->setResult($this);
        $event->setData([
            'LapsDone' => $packet->LapsDone,
            'LTime' => $packet->LTime,
            'ETime' => $packet->ETime,
            'NumStops' => $packet->NumStops
        ]);

        $this->events[] = $event;

        return $event;
    }

}
