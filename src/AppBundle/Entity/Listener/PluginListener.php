<?php

namespace AppBundle\Entity\Listener;

use AppBundle\Entity\Plugin;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

class PluginListener {

    /** @var TraceableEventDispatcher */
    protected $eventDispacher;

    /** @var EntityManager|null */
    protected $entityManager;
    
    /** @var ContainerInterface */
    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->eventDispacher = $container->get('event_dispatcher');
    }

    /** @ORM\PostLoad */
    public function postLoadHandler(Plugin $plugin, LifecycleEventArgs $event) {
        $plugin->setContainer($this->container);
        $plugin->setEntityManager($this->entityManager);
        $plugin->setEventDispacher($this->eventDispacher);
    }

}
