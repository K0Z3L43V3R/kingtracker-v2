<?php

namespace AppBundle\Entity;

use AppBundle\Helper\Math\Point2D;
use AppBundle\Packet\CompCar;
use AppBundle\Packet\IS_NPL;

class PlayerCar {

    /** @var string Car name */
    private $CName;

    /** @var int X map (65536 = 1 metre) */
    private $X;

    /** @var int Y map (65536 = 1 metre) */
    private $Y;

    /** @var int Z alt (65536 = 1 metre) */
    private $Z;
    private $position;

    /** @var int speed (32768 = 100 m/s) */
    private $Speed;

    /** @var int car's motion if Speed > 0 : 0 = world y direction, 32768 = 180 deg */
    private $Direction;

    /** @var int direction of forward axis : 0 = world y direction, 32768 = 180 deg */
    private $Heading;

    /** @var int signed, rate of change of heading : (16384 = 360 deg/s) */
    private $AngVel;

    /** @var array compounds */
    public $Tyres = array();

    /** @var int added mass (kg) */
    public $H_Mass;

    /** @var int intake restriction */
    public $H_TRes;

    public function __construct(IS_NPL $npl) {
        $this->CName = $npl->CName;

        $this->Tyres = $npl->Tyres;
        $this->H_Mass = $npl->H_Mass;
        $this->H_TRes = $npl->H_TRes;
    }

    /**
     * @param CompCar $car
     */
    public function setMCI(CompCar $car) {
        $this->X = $car->X;
        $this->Y = $car->Y;
        $this->Z = $car->Z;
        $this->position = new Point2D($car->X, $car->Y);
        $this->Speed = $car->Speed;
        $this->Direction = $car->Direction;
        $this->Heading = $car->Heading;
        $this->AngVel = $car->AngVel;
    }

    public function getCName() {
        return $this->CName;
    }

    public function getX() {
        return $this->X;
    }

    public function getY() {
        return $this->Y;
    }

    public function getZ() {
        return $this->Z;
    }

    public function getPosition() {
        return $this->position;
    }

    public function getSpeed() {
        return $this->Speed;
    }

    public function getDirection() {
        return $this->Direction;
    }

    public function getHeading() {
        return $this->Heading;
    }

    public function getAngVel() {
        return $this->AngVel;
    }

    public function setCName($CName) {
        $this->CName = $CName;
    }

    public function setX($X) {
        $this->X = $X;
    }

    public function setY($Y) {
        $this->Y = $Y;
    }

    public function setZ($Z) {
        $this->Z = $Z;
    }

    public function setPosition($position) {
        $this->position = $position;
    }

    public function setSpeed($Speed) {
        $this->Speed = $Speed;
    }

    public function setDirection($Direction) {
        $this->Direction = $Direction;
    }

    public function setHeading($Heading) {
        $this->Heading = $Heading;
    }

    public function setAngVel($AngVel) {
        $this->AngVel = $AngVel;
    }

}
