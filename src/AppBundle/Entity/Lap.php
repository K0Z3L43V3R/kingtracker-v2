<?php

namespace AppBundle\Entity;

use AppBundle\Helper\TimeHelper;
use AppBundle\Types\PlayerFlags;
use AppBundle\Types\SetupFlags;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lap
 *
 * @ORM\Table(name="lap")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LapRepository")
 */
class Lap {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Player
     * 
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $player;

    /**
     * @var string
     *
     * @ORM\Column(name="track", type="string", length=8)
     */
    private $track;

    /**
     * @var string
     *
     * @ORM\Column(name="layout", type="string", length=128, nullable=true)
     */
    private $layout;

    /**
     * @var string
     *
     * @ORM\Column(name="car", type="string", length=16)
     */
    private $car;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clean", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $clean = true;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer")
     */
    private $time;

    /**
     * @var int
     *
     * @ORM\Column(name="sp_1", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sp_1;

    /**
     * @var int
     *
     * @ORM\Column(name="sp_2", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sp_2;

    /**
     * @var int
     *
     * @ORM\Column(name="sp_3", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sp_3;

    /**
     * @var int
     *
     * @ORM\Column(name="sc_1", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sc_1;

    /**
     * @var int
     *
     * @ORM\Column(name="sc_2", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sc_2;

    /**
     * @var int
     *
     * @ORM\Column(name="sc_3", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sc_3;
    
    /**
     * @var int
     *
     * @ORM\Column(name="sc_4", type="integer", nullable=true, options={"default" : "0"})
     */
    private $sc_4;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_M", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_M = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_W", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_W = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_KB", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_KB = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_KBS", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_KBS = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_AC", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_AC = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_AxisC", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_AxisC = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_BC", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_BC = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_AG", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_AG = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_HS", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_HS = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_SQ", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_SQ = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PF_BRH", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $PF_BRH = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SF_ABS", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $SF_ABS = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SF_TC", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $SF_TC = false;

    /**
     * @var string
     *
     * @ORM\Column(name="tyres", type="string", length=8, nullable=true)
     */
    private $tyres;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set track
     *
     * @param string $track
     *
     * @return Lap
     */
    public function setTrack($track) {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return string
     */
    public function getTrack() {
        return $this->track;
    }

    /**
     * Set layout
     *
     * @param string $layout
     *
     * @return Lap
     */
    public function setLayout($layout) {
        $this->layout = $layout;

        return $this;
    }

    /**
     * Get layout
     *
     * @return string
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * Set car
     *
     * @param string $car
     *
     * @return Lap
     */
    public function setCar($car) {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return string
     */
    public function getCar() {
        return $this->car;
    }

    /**
     * Set clean
     *
     * @param boolean $clean
     *
     * @return Lap
     */
    public function setClean($clean) {
        $this->clean = $clean;

        return $this;
    }

    /**
     * Get clean
     *
     * @return boolean
     */
    public function getClean() {
        return $this->clean;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return Lap
     */
    public function setTime($time) {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * Set sp1
     *
     * @param integer $sp1
     *
     * @return Lap
     */
    public function setSp1($sp1) {
        $this->sp_1 = $sp1;

        return $this;
    }

    /**
     * Get sp1
     *
     * @return integer
     */
    public function getSp1() {
        return $this->sp_1;
    }

    /**
     * Set sp2
     *
     * @param integer $sp2
     *
     * @return Lap
     */
    public function setSp2($sp2) {
        $this->sp_2 = $sp2;

        return $this;
    }

    /**
     * Get sp2
     *
     * @return integer
     */
    public function getSp2() {
        return $this->sp_2;
    }

    /**
     * Set sp3
     *
     * @param integer $sp3
     *
     * @return Lap
     */
    public function setSp3($sp3) {
        $this->sp_3 = $sp3;

        return $this;
    }

    /**
     * Get sp3
     *
     * @return integer
     */
    public function getSp3() {
        return $this->sp_3;
    }

    /**
     * Set sc1
     *
     * @param integer $sc1
     *
     * @return Lap
     */
    public function setSc1($sc1) {
        $this->sc_1 = $sc1;

        return $this;
    }

    /**
     * Get sc1
     *
     * @return integer
     */
    public function getSc1() {
        return $this->sc_1;
    }

    /**
     * Set sc2
     *
     * @param integer $sc2
     *
     * @return Lap
     */
    public function setSc2($sc2) {
        $this->sc_2 = $sc2;

        return $this;
    }

    /**
     * Get sc2
     *
     * @return integer
     */
    public function getSc2() {
        return $this->sc_2;
    }

    /**
     * Set sc3
     *
     * @param integer $sc3
     *
     * @return Lap
     */
    public function setSc3($sc3) {
        $this->sc_3 = $sc3;

        return $this;
    }

    /**
     * Get sc3
     *
     * @return integer
     */
    public function getSc3() {
        return $this->sc_3;
    }
    
    /**
     * Set sc4
     *
     * @param integer $sc4
     *
     * @return Lap
     */
    public function setSc4($sc4) {
        $this->sc_4 = $sc4;

        return $this;
    }

    /**
     * Get sc4
     *
     * @return integer
     */
    public function getSc4() {
        return $this->sc_4;
    }
    
    /**
     * 
     * @return array
     */
    public function getSectors(){
        $sectors = [];
        
        if($this->sc_1){
            $sectors[] = $this->sc_1;
        }
        
        if($this->sc_2){
            $sectors[] = $this->sc_2;
        }
        
        if($this->sc_3){
            $sectors[] = $this->sc_3;
        }
        
        if($this->sc_4){
            $sectors[] = $this->sc_4;
        }
        
        return $sectors;
    }
    
    /**
     * 
     * @return array
     */
    public function getSectorsTimes(){
        $sectors = [];
        
        if($this->sc_1){
            $sectors[] = TimeHelper::timeToString($this->sc_1);
        }
        
        if($this->sc_2){
            $sectors[] = TimeHelper::timeToString($this->sc_2);
        }
        
        if($this->sc_3){
            $sectors[] = TimeHelper::timeToString($this->sc_3);
        }
        
        if($this->sc_4){
            $sectors[] = TimeHelper::timeToString($this->sc_4);
        }
        
        return $sectors;
    }

    /**
     * Set pFM
     *
     * @param boolean $pFM
     *
     * @return Lap
     */
    public function setPFM($pFM) {
        $this->PF_M = $pFM;

        return $this;
    }

    /**
     * Get pFM
     *
     * @return boolean
     */
    public function getPFM() {
        return $this->PF_M;
    }

    /**
     * Set pFW
     *
     * @param boolean $pFW
     *
     * @return Lap
     */
    public function setPFW($pFW) {
        $this->PF_W = $pFW;

        return $this;
    }

    /**
     * Get pFW
     *
     * @return boolean
     */
    public function getPFW() {
        return $this->PF_W;
    }

    /**
     * Set pFKB
     *
     * @param boolean $pFKB
     *
     * @return Lap
     */
    public function setPFKB($pFKB) {
        $this->PF_KB = $pFKB;

        return $this;
    }

    /**
     * Get pFKB
     *
     * @return boolean
     */
    public function getPFKB() {
        return $this->PF_KB;
    }

    /**
     * Set pFKBS
     *
     * @param boolean $pFKBS
     *
     * @return Lap
     */
    public function setPFKBS($pFKBS) {
        $this->PF_KBS = $pFKBS;

        return $this;
    }

    /**
     * Get pFKBS
     *
     * @return boolean
     */
    public function getPFKBS() {
        return $this->PF_KBS;
    }

    /**
     * Set pFAC
     *
     * @param boolean $pFAC
     *
     * @return Lap
     */
    public function setPFAC($pFAC) {
        $this->PF_AC = $pFAC;

        return $this;
    }

    /**
     * Get pFAC
     *
     * @return boolean
     */
    public function getPFAC() {
        return $this->PF_AC;
    }

    /**
     * Set pFAxisC
     *
     * @param boolean $pFAxisC
     *
     * @return Lap
     */
    public function setPFAxisC($pFAxisC) {
        $this->PF_AxisC = $pFAxisC;

        return $this;
    }

    /**
     * Get pFAxisC
     *
     * @return boolean
     */
    public function getPFAxisC() {
        return $this->PF_AxisC;
    }

    /**
     * Set pFBC
     *
     * @param boolean $pFBC
     *
     * @return Lap
     */
    public function setPFBC($pFBC) {
        $this->PF_BC = $pFBC;

        return $this;
    }

    /**
     * Get pFBC
     *
     * @return boolean
     */
    public function getPFBC() {
        return $this->PF_BC;
    }

    /**
     * Set pFAG
     *
     * @param boolean $pFAG
     *
     * @return Lap
     */
    public function setPFAG($pFAG) {
        $this->PF_AG = $pFAG;

        return $this;
    }

    /**
     * Get pFAG
     *
     * @return boolean
     */
    public function getPFAG() {
        return $this->PF_AG;
    }

    /**
     * Set pFHS
     *
     * @param boolean $pFHS
     *
     * @return Lap
     */
    public function setPFHS($pFHS) {
        $this->PF_HS = $pFHS;

        return $this;
    }

    /**
     * Get pFHS
     *
     * @return boolean
     */
    public function getPFHS() {
        return $this->PF_HS;
    }

    /**
     * Set pFSQ
     *
     * @param boolean $pFSQ
     *
     * @return Lap
     */
    public function setPFSQ($pFSQ) {
        $this->PF_SQ = $pFSQ;

        return $this;
    }

    /**
     * Get pFSQ
     *
     * @return boolean
     */
    public function getPFSQ() {
        return $this->PF_SQ;
    }

    /**
     * Set pFBRH
     *
     * @param boolean $pFBRH
     *
     * @return Lap
     */
    public function setPFBRH($pFBRH) {
        $this->PF_BRH = $pFBRH;

        return $this;
    }

    /**
     * Get pFBRH
     *
     * @return boolean
     */
    public function getPFBRH() {
        return $this->PF_BRH;
    }

    /**
     * Set sFABS
     *
     * @param boolean $sFABS
     *
     * @return Lap
     */
    public function setSFABS($sFABS) {
        $this->SF_ABS = $sFABS;

        return $this;
    }

    /**
     * Get sFABS
     *
     * @return boolean
     */
    public function getSFABS() {
        return $this->SF_ABS;
    }

    /**
     * Set sFTC
     *
     * @param boolean $sFTC
     *
     * @return Lap
     */
    public function setSFTC($sFTC) {
        $this->SF_TC = $sFTC;

        return $this;
    }

    /**
     * Get sFTC
     *
     * @return boolean
     */
    public function getSFTC() {
        return $this->SF_TC;
    }

    /**
     * Set tyres
     *
     * @param string $tyres
     *
     * @return Lap
     */
    public function setTyres($tyres) {
        $this->tyres = $tyres;

        return $this;
    }

    /**
     * Get tyres
     *
     * @return string
     */
    public function getTyres() {
        return $this->tyres;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Lap
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Lap
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set player
     *
     * @param Player $player
     *
     * @return Lap
     */
    public function setPlayer(Player $player = null) {
        $this->player = $player;

        if ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::MOUSE))
            $this->PF_M = true;
        elseif ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::KB_NO_HELP))
            $this->PF_KB = true;
        elseif ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::KB_STABILISED))
            $this->PF_KBS = true;
        else
            $this->PF_W = true;

        if ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::AUTOCLUTCH))
            $this->PF_AC = true;
        elseif ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::AXIS_CLUTCH))
            $this->PF_AxisC = true;
        else
            $this->PF_BC = true;

        if ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::AUTOGEARS))
            $this->PF_AG = true;
        elseif ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::SHIFTER))
            $this->PF_HS = true;
        else
            $this->PF_SQ = true;

        if ($this->player->getPlayerFlags()->hasFlag(PlayerFlags::HELP_B))
            $this->PF_BRH = true;

        if ($this->player->getSetupFlags()->hasFlag(SetupFlags::ABS_ENABLE))
            $this->SF_ABS = true;

        if ($this->player->getSetupFlags()->hasFlag(SetupFlags::TC_ENABLE))
            $this->SF_TC = true;

        return $this;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer() {
        return $this->player;
    }

    public function __toString() {
        return json_encode([
            'UName' => $this->getPlayer()->getUName(),
            'car' => $this->getCar(),
            'track' => $this->getTrack(),
            'layout' => $this->getLayout(),
            'time' => $this->getTime()
        ]);
    }

}
