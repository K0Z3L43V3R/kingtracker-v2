<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Host;
use AppBundle\Entity\Player;

/**
 * HostConnection
 *
 * @ORM\Table(name="host_connection")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HostConnectionRepository")
 */
class HostConnection {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="UCID", type="integer", nullable=true)
     */
    private $UCID;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PLID", type="integer", nullable=true)
     */
    private $PLID;

    /**
     * Host
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Host", inversedBy="hostConnections")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * Player
     * 
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Player", inversedBy="hostConnection")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $player;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uCID
     *
     * @param integer $UCID
     *
     * @return HostConnection
     */
    public function setUCID($UCID) {
        $this->UCID = $UCID;

        return $this;
    }

    /**
     * Get uCID
     *
     * @return int
     */
    public function getUCID() {
        return $this->UCID;
    }

    /**
     * Set PLID
     *
     * @param int|null $PLID
     *
     * @return HostConnection
     */
    public function setPLID($PLID) {
        $this->PLID = $PLID;

        return $this;
    }

    /**
     * Get PLID
     *
     * @return int|null
     */
    public function getPLID() {
        return $this->PLID;
    }

    /**
     * Set host
     *
     * @param Host $host
     *
     * @return HostConnection
     */
    public function setHost(Host $host = null) {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return Host
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * Set player
     *
     * @param Player $player
     *
     * @return HostConnection
     */
    public function setPlayer(Player $player = null) {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer() {
        return $this->player;
    }

}
