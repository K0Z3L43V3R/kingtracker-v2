<?php

namespace AppBundle\Entity;

use AppBundle\Types\Weather;
use AppBundle\Types\Wind;
use Doctrine\ORM\Mapping as ORM;

/**
 * Track
 *
 * @ORM\Table(name="track")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TrackRepository")
 */
class Track {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=6, unique=true)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="track", type="string", length=64)
     */
    private $track;

    /**
     * @var string
     *
     * @ORM\Column(name="circuit", type="string", length=64)
     */
    private $circuit;

    /**
     * @var string
     *
     * @ORM\Column(name="licence", type="string", length=16, nullable=true)
     */
    private $licence;

    /**
     * @var int
     *
     * @ORM\Column(name="grid", type="integer", nullable=true, options={"default" : "40"})
     */
    private $grid = 40;

    /**
     * @var string
     *
     * @ORM\Column(name="lenght", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $lenght;

    /**
     * @var string
     *
     * @ORM\Column(name="elevation", type="string", length=64, nullable=true)
     */
    private $elevation;

    /**
     * @var bool
     *
     * @ORM\Column(name="reverse", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $reverse;

    /**
     * @var bool
     *
     * @ORM\Column(name="asphalt", type="boolean", nullable=true, options={"default" : "1"})
     */
    private $asphalt;

    /**
     * @var bool
     *
     * @ORM\Column(name="oval", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $oval;

    /**
     * @var bool
     *
     * @ORM\Column(name="rallycross", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $rallycross;

    /**
     * @var bool
     *
     * @ORM\Column(name="carpark", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $carpark;

    /**
     * @var bool
     *
     * @ORM\Column(name="drag", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $drag;

    /** @var Weather */
    private $weather;

    /** @var Wind */
    private $wind;

    /** @var int */
    private $splitsCount = 0;

    /** @var string Layout name */
    private $LName = '';

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Track
     */
    public function setShortName($shortName) {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName() {
        return $this->shortName;
    }

    /**
     * Set track
     *
     * @param string $track
     *
     * @return Track
     */
    public function setTrack($track) {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return string
     */
    public function getTrack() {
        return $this->track;
    }

    /**
     * Set circuit
     *
     * @param string $circuit
     *
     * @return Track
     */
    public function setCircuit($circuit) {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return string
     */
    public function getCircuit() {
        return $this->circuit;
    }

    /**
     * Set licence
     *
     * @param string $licence
     *
     * @return Track
     */
    public function setLicence($licence) {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return string
     */
    public function getLicence() {
        return $this->licence;
    }

    /**
     * Set grid
     *
     * @param integer $grid
     *
     * @return Track
     */
    public function setGrid($grid) {
        $this->grid = $grid;

        return $this;
    }

    /**
     * Get grid
     *
     * @return int
     */
    public function getGrid() {
        return $this->grid;
    }

    /**
     * Set lenght
     *
     * @param string $lenght
     *
     * @return Track
     */
    public function setLenght($lenght) {
        $this->lenght = $lenght;

        return $this;
    }

    /**
     * Get lenght
     *
     * @return string
     */
    public function getLenght() {
        return $this->lenght;
    }

    /**
     * Set elevation
     *
     * @param string $elevation
     *
     * @return Track
     */
    public function setElevation($elevation) {
        $this->elevation = $elevation;

        return $this;
    }

    /**
     * Get elevation
     *
     * @return string
     */
    public function getElevation() {
        return $this->elevation;
    }

    /**
     * Set reverse
     *
     * @param boolean $reverse
     *
     * @return Track
     */
    public function setReverse($reverse) {
        $this->reverse = $reverse;

        return $this;
    }

    /**
     * Get reverse
     *
     * @return bool
     */
    public function hasReverse() {
        return $this->reverse;
    }

    /**
     * Is reversed
     *
     * @return bool
     */
    public function isReversed() {
        if (strpos($this->shortName, 'R') !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set asphalt
     *
     * @param boolean $asphalt
     *
     * @return Track
     */
    public function setAsphalt($asphalt) {
        $this->asphalt = $asphalt;

        return $this;
    }

    /**
     * Get asphalt
     *
     * @return bool
     */
    public function isAsphalt() {
        return $this->asphalt;
    }

    /**
     * Set oval
     *
     * @param boolean $oval
     *
     * @return Track
     */
    public function setOval($oval) {
        $this->oval = $oval;

        return $this;
    }

    /**
     * Get oval
     *
     * @return bool
     */
    public function isOval() {
        return $this->oval;
    }

    /**
     * Set rallycross
     *
     * @param boolean $rallycross
     *
     * @return Track
     */
    public function setRallycross($rallycross) {
        $this->rallycross = $rallycross;

        return $this;
    }

    /**
     * Get rallycross
     *
     * @return bool
     */
    public function isRallycross() {
        return $this->rallycross;
    }

    /**
     * Set carpark
     *
     * @param boolean $carpark
     *
     * @return Track
     */
    public function setCarpark($carpark) {
        $this->carpark = $carpark;

        return $this;
    }

    /**
     * Get carpark
     *
     * @return bool
     */
    public function isCarpark() {
        return $this->carpark;
    }

    /**
     * Set drag
     *
     * @param boolean $drag
     *
     * @return Track
     */
    public function setDrag($drag) {
        $this->drag = $drag;

        return $this;
    }

    /**
     * Get drag
     *
     * @return bool
     */
    public function isDrag() {
        return $this->drag;
    }

    public function getWeather(): Weather {
        return $this->weather;
    }

    /**
     * @param int $weather
     * @return Track
     */
    public function setWeather($weather) {
        $this->weather = new Weather($weather, $this->shortName);

        return $this;
    }

    public function getWind(): Wind {
        return $this->wind;
    }

    /**
     * @param int $wind
     * @return Track
     */
    public function setWind($wind) {
        $this->wind = new Wind($wind);

        return $this;
    }

    /**
     * @return int
     */
    public function getSplitsCount() {
        return $this->splitsCount;
    }

    /**
     * @param int $splitsCount
     * @return Track
     */
    public function setSplitsCount($splitsCount) {
        $this->splitsCount = $splitsCount;

        return $this;
    }

    public function getLName() {
        return $this->LName;
    }

    public function setLName($LName) {
        $this->LName = $LName;
    }

}
