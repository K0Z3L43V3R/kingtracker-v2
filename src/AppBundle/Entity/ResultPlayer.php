<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Result;
use AppBundle\Entity\Player;
use Doctrine\ORM\Mapping as ORM;

/**
 * ResultPlayer
 *
 * @ORM\Table(name="result_player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResultPlayerRepository")
 */
class ResultPlayer {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Host
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Result", inversedBy="players")
     * @ORM\JoinColumn(name="result_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $result;

    /**
     * Player
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $player;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true, options={"default" : "0"})
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="car", type="string", length=16)
     */
    private $car;

    /**
     * @var int
     *
     * @ORM\Column(name="TTime", type="integer", nullable=true, options={"default" : "0"})
     */
    private $tTime;

    /**
     * @var int
     *
     * @ORM\Column(name="BTime", type="integer", nullable=true, options={"default" : "0"})
     */
    private $bTime;

    /**
     * @var int
     *
     * @ORM\Column(name="NumStops", type="integer", nullable=true, options={"default" : "0"})
     */
    private $numStops;

    /**
     * @var int
     *
     * @ORM\Column(name="LapsDone", type="integer", nullable=true, options={"default" : "0"})
     */
    private $lapsDone;

    /**
     * @var int
     *
     * @ORM\Column(name="PSeconds", type="integer", nullable=true, options={"default" : "0"})
     */
    private $pSeconds;

    /**
     * @var int
     *
     * @ORM\Column(name="Flags", type="integer", nullable=true, options={"default" : "0"})
     */
    private $flags;

    /**
     * @var int
     *
     * @ORM\Column(name="confirmation", type="integer", nullable=true, options={"default" : "0"})
     */
    private $confirmation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ResultPlayer
     */
    public function setPosition($position) {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * Set car
     *
     * @param string $car
     *
     * @return ResultPlayer
     */
    public function setCar($car) {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return string
     */
    public function getCar() {
        return $this->car;
    }

    /**
     * Set tTime
     *
     * @param integer $tTime
     *
     * @return ResultPlayer
     */
    public function setTTime($tTime) {
        $this->tTime = $tTime;

        return $this;
    }

    /**
     * Get tTime
     *
     * @return int
     */
    public function getTTime() {
        return $this->tTime;
    }

    /**
     * Set bTime
     *
     * @param integer $bTime
     *
     * @return ResultPlayer
     */
    public function setBTime($bTime) {
        $this->bTime = $bTime;

        return $this;
    }

    /**
     * Get bTime
     *
     * @return int
     */
    public function getBTime() {
        return $this->bTime;
    }

    /**
     * Set numStops
     *
     * @param integer $numStops
     *
     * @return ResultPlayer
     */
    public function setNumStops($numStops) {
        $this->numStops = $numStops;

        return $this;
    }

    /**
     * Get numStops
     *
     * @return int
     */
    public function getNumStops() {
        return $this->numStops;
    }

    /**
     * Set lapsDone
     *
     * @param integer $lapsDone
     *
     * @return ResultPlayer
     */
    public function setLapsDone($lapsDone) {
        $this->lapsDone = $lapsDone;

        return $this;
    }

    /**
     * Get lapsDone
     *
     * @return int
     */
    public function getLapsDone() {
        return $this->lapsDone;
    }

    /**
     * Set pSeconds
     *
     * @param integer $pSeconds
     *
     * @return ResultPlayer
     */
    public function setPSeconds($pSeconds) {
        $this->pSeconds = $pSeconds;

        return $this;
    }

    /**
     * Get pSeconds
     *
     * @return int
     */
    public function getPSeconds() {
        return $this->pSeconds;
    }

    /**
     * Set flags
     *
     * @param integer $flags
     *
     * @return ResultPlayer
     */
    public function setFlags($flags) {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Get flags
     *
     * @return int
     */
    public function getFlags() {
        return $this->flags;
    }

    /**
     * Set confirmation
     *
     * @param integer $confirmation
     *
     * @return ResultPlayer
     */
    public function setConfirmation($confirmation) {
        $this->confirmation = $confirmation;

        return $this;
    }

    /**
     * Get confirmation
     *
     * @return int
     */
    public function getConfirmation() {
        return $this->confirmation;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ResultPlayer
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * 
     * @return Result
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * 
     * @return Player
     */
    public function getPlayer() {
        return $this->player;
    }

    /**
     * 
     * @param Result $result
     * @return ResultPlayer
     */
    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    /**
     * 
     * @param Player $player
     * @return ResultPlayer
     */
    public function setPlayer($player) {
        $this->player = $player;
        return $this;
    }

}
