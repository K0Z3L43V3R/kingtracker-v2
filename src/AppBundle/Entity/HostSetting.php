<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Host;
use Doctrine\ORM\Mapping as ORM;

/**
 * HostSetting
 *
 * @ORM\Table(name="host_setting")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HostSettingRepository")
 */
class HostSetting {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Host
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Host", inversedBy="settings")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="_group", type="string", length=16, nullable=true)
     */
    private $group;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var array
     *
     * @ORM\Column(name="data", type="json_array", nullable=true)
     */
    private $data;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param string $group
     *
     * @return HostSetting
     */
    public function setGroup($group) {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return string
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return HostSetting
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return HostSetting
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return HostSetting
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * 
     * @return Host
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * 
     * @param Host $host
     * @return HostSetting
     */
    public function setHost($host) {
        $this->host = $host;
        return $this;
    }

}
