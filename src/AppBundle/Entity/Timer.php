<?php

namespace AppBundle\Entity;

/**
 * Timer
 */
class Timer {

    const TIMER_RUNNING = 1;
    const TIMER_FINISHED = 3;
    const RESTART_DELAY = 4;

    public $uid;
    public $name;
    public $state;
    public $timer = 0;
    public $timer_backup = 0;
    public $eventDone = null;

    public function __construct($name, $sec, $eventDone = null) {
        $this->uid = uniqid('', true);
        $this->name = $name;
        $this->timer = $sec;
        $this->timer_backup = $sec;
        $this->eventDone = $eventDone;
        $this->state = Timer::TIMER_RUNNING;
    }

    /**
     * One second
     */
    public function tick() {
        if ($this->state != Timer::TIMER_RUNNING) {
            return;
        }

        $this->timer--;

        if ($this->timer < 0) {
            $this->state = Timer::TIMER_FINISHED;

            if ($this->eventDone) {
                call_user_func($this->eventDone);
            }
        }
    }

    /**
     * Restart timer
     */
    public function restart() {
        $this->timer = $this->timer_backup;
        $this->state = Timer::TIMER_RUNNING;
    }

}
