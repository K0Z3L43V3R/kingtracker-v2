<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Result;
use AppBundle\Entity\Player;
use Doctrine\ORM\Mapping as ORM;

/**
 * ResultEvent
 *
 * @ORM\Table(name="result_event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResultEventRepository")
 */
class ResultEvent {

    const TYPE_LAP = 'LAP';
    const TYPE_SPLIT = 'SPLIT';
    const TYPE_PIT = 'PIT';
    const TYPE_PENALTY = 'PENALTY';
    const TYPE_DNF = 'DNF';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Host
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Result", inversedBy="events")
     * @ORM\JoinColumn(name="result_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $result;

    /**
     * Player
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $player;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var array
     *
     * @ORM\Column(name="data", type="json_array", nullable=true)
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ResultEvent
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return ResultEvent
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ResultEvent
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * 
     * @return Result
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * 
     * @return Player
     */
    public function getPlayer() {
        return $this->player;
    }

    /**
     * 
     * @param Result $result
     * @return ResultEvent
     */
    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    /**
     * 
     * @param Player $player
     * @return ResultEvent
     */
    public function setPlayer($player) {
        $this->player = $player;
        return $this;
    }

}
