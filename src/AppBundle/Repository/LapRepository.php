<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Lap;
use Doctrine\ORM\EntityRepository;

/**
 * LapRepository
 */
class LapRepository extends EntityRepository {

    /**
     * 
     * @param int $player_id
     * @param array $flags
     * @param bool $clean
     * @param string $car
     * @param string $track
     * @param string $layout
     * @return Lap
     */
    public function findOneByFlags(int $player_id, array $flags = [], bool $clean = null, string $car = null, string $track = null, string $layout = null) {
        $qb = $this->createQueryBuilder('lap');

        $qb->select('lap');
        $qb->andWhere('lap.player = :player_id')->setParameter('player_id', $player_id);

        foreach ($flags as $key => $value) {
            $qb->andWhere("lap.{$key} = :{$key}")->setParameter($key, $value);
        }

        if ($clean !== null) {
            $qb->andWhere('lap.clean = :clean')->setParameter('clean', (int) $clean);
        }

        if ($car !== null) {
            $qb->andWhere('lap.car = :car')->setParameter('car', $car);
        }

        if ($track !== null) {
            $qb->andWhere('lap.track = :track')->setParameter('track', $track);
        }

        if ($layout !== null) {
            $qb->andWhere('lap.layout = :layout')->setParameter('layout', $layout);
        }

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
    
    /**
     * 
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * 
     * @return Lap[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null) {
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

}
