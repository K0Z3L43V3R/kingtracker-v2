<?php

namespace AppBundle\Console;

use Symfony\Component\Console\Style\SymfonyStyle;

class CustomStyle extends SymfonyStyle {

    /**
     * Writes a log message to the output.
     * 
     * @param string $message
     */
    public function log($message) {
        $messages = is_array($message) ? array_values($message) : array($message);
        foreach ($messages as $message) {
            $this->writeln(sprintf('[%s] - %s', date('Y-m-d H:i:s'), $message));
        }
    }

}
