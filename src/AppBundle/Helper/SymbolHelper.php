<?php

namespace AppBundle\Helper;

/**
 * ^H
 * ▼ ▽ 
 * ▲ △
 */
class SymbolHelper {

    private static $codepages = array(
        'L' => 'CP1252', // Latin 1
        'G' => 'CP1253', // Greek
        'C' => 'CP1251', // Cyrillic
        'E' => 'CP1250', // Central Europe
        'T' => 'CP1254', // Turkish
        'B' => 'CP1257', // Baltic
        'J' => 'CP932', // Japanese
        'S' => 'CP936', // Simplified Chinese
        'K' => 'CP949', // Korean
        'H' => 'CP950', // Traditional Chinese
    );

    /**
     * Convert UTF text to LFS codepage
     * @param type $string
     * @return type
     */
    public static function toLFS($string) {
        return self::codepageConvert($string);
    }

    /**
     * ▼
     * @return string
     */
    public static function getOrderDesc() {
        return '^H' . iconv("UTF-8", "CP950//TRANSLIT", '▼');
    }

    /**
     * ▲
     * @return string
     */
    public static function getOrderAsc() {
        return '^H' . iconv("UTF-8", "CP950//TRANSLIT", '▲');
    }
    
    /**
     * ×
     * @return string
     */
    public static function getClose() {
        return self::codepageConvert('^L×');
    }

    /**
     * 
     * @param string $in
     * @return string
     */
    private static function codepageConvert($in) {
        $parts = preg_split('#\^([LGCETBJSKH])#', $in, -1, PREG_SPLIT_DELIM_CAPTURE);
        array_unshift($parts, 'L');

        $p = 0;
        $out = '';
        while (isset($parts[$p])) {
            $out .= "^$parts[$p]" . iconv("UTF-8", self::$codepages[$parts[$p]] . "//TRANSLIT", trim($parts[$p + 1]));
            $p += 2;
        }

        return $out;
    }

}
