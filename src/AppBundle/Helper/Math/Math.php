<?php

namespace AppBundle\Helper\Math;

use AppBundle\Entity\PlayerCar;
use AppBundle\Packet\CompCar;

class Math {

    const RAD90DEG = 1.5707963268;                  // 90°
    const CAR_BEHIND_TRESHOLD_ANGLE = 1.5707963268; // 90°
    const DRAFT_DISTANCE_TRESHOLD = 0.3;            // sec
    const DRAFT_DISTANCE_DEBUG = 0.6;               // sec
    const DRAFT_MAX_ANGLE = 10.0;                   // deg
    const DRAFT_MIN_TEST_SPEED = 0.2;               // mps

    /**
     * Distance between two 2D points
     * 
     * @param Point2D $a
     * @param Point2D $b
     * 
     * @return float
     */
    public static function distance(Point2D $a, Point2D $b) {
        $dx = ($b->x - $a->x);
        $dy = ($b->y - $a->y);
        return sqrt(($dx * $dx) + ($dy * $dy)) / 65536;
    }

    /**
     * Angle between two 2D points
     * 
     * @param Point2D $a
     * @param Point2D $b
     * 
     * @return float
     */
    public static function angle(Point2D $a, Point2D $b, $deg = false) {
        $angle = atan2(($a->x - $b->x), ($a->y - $b->y));

        if ($deg)
            $angle = rad2deg($angle);

        return $angle;
    }

    /**
     * Detect slipstream
     * 
     * @param Point2D $posMy - tested car
     * @param Point2D $posCar - car ahead
     * @param PlayerCar $myCar
     * @param array $return
     * 
     * @return boolean
     */
    public static function isSlipStream(Point2D $posMy, Point2D $posCar, PlayerCar $myCar, &$return = array()) {
        // Minimu speed test
        $speed = Math::speedToMps($myCar->getSpeed());
        if ($speed < Math::DRAFT_MIN_TEST_SPEED) {
            return false;
        }

        $distance = Math::distance($posCar, $posMy) / $speed;

        if ($distance < Math::DRAFT_DISTANCE_DEBUG) {
            if (!Math::isCarBehind($posMy, $posCar, $myCar)) {
                return false;
            }

            $angle = -atan2(($posMy->x - $posCar->x), ($posMy->y - $posCar->y));
            $headingTest = $myCar->getHeading() * M_PI / 32768 - M_PI;
            $diff = rad2deg(abs(atan2(sin($angle - $headingTest), cos($angle - $headingTest))));

            $return = array(
                'distance' => $distance,
                'angle' => $diff,
                'status' => '',
            );
                        
            if ($distance < Math::DRAFT_DISTANCE_TRESHOLD) {
                if ($diff < Math::DRAFT_MAX_ANGLE) {
                    $return['status'] = 'DRAFT';
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Detect if car is behind other car
     * 
     * @param Point2D $posMy
     * @param Point2D $posCar
     * @param CompCar $myCar
     * 
     * @return boolean
     */
    public static function isCarBehind(Point2D $posMy, Point2D $posCar, PlayerCar $myCar) {
        // My car heading vector
        $heading = Math::convertAngle($myCar->getHeading());
        $headingVec = Vector2D::fromAngle($heading - Math::RAD90DEG);

        // Vector from test car to my car
        $dirVec = Vector2D::normalize(Vector2D::fromPoints($posCar, $posMy));
        $angle = acos(Vector2D::dot($headingVec, $dirVec));

        return $angle < Math::CAR_BEHIND_TRESHOLD_ANGLE;
    }

    /**
     * Convert LFS speed to meters per second
     * 
     * @param float $speed
     * 
     * @return float
     */
    public static function speedToMps($speed) {
        return $speed / 327.68;
    }
    
    /**
     * Convert LFS speed to KM per hour
     * 
     * @param float $speed
     * 
     * @return float
     */
    public static function speedToKph($speed){
        return number_format((self::speedToMps($speed) * 3.600), 2, '.', '');
    }
    
    /**
     * Convert LFS speed to miles per hour
     * 
     * @param float $speed
     * 
     * @return float
     */
    public static function speedToMph($speed){
        return number_format((self::speedToMps($speed) * 2.237), 2, '.', '');
    }
    
    /**
     * Convert LFS angle 
     * 
     * @param float $angle
     * @param float $deg
     * 
     * @return float
     */
    public static function convertAngle($angle, $deg = false) {
        $rad = $angle * M_PI / 32768 - M_PI;

        if ($deg) {
            return rad2deg($rad);
        } else {
            return $rad;
        }
    }

}
