<?php

namespace AppBundle\Helper\Math;

class Vector2D {

    public $x = 0;
    public $y = 0;

    public function __construct($x = 0, $y = 0) {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Create Vector2D from angle
     * 
     * @param float $angle (rad)
     * 
     * @return Vector2D
     */
    public static function fromAngle($angle) {
        return new Vector2D(cos($angle), sin($angle));
    }

    /**
     * Create Vector2D from Point2D A to Point2S B
     * 
     * @param Point2D $a
     * @param Point2D $b
     * 
     * @return Vector2D
     */
    public static function fromPoints(Point2D $a, Point2D $b) {
        return new Vector2D($a->x - $b->x, $a->y - $b->y);
    }

    /**
     * Dot product
     * 
     * @param Vector2D $a
     * @param Vector2D $b
     * @return float
     */
    public static function dot(Vector2D $a, Vector2D $b) {
        return ($a->x * $b->x) + ($a->y * $b->y);
    }

    /**
     * Normalize vector
     * 
     * @param Vector2D $vec
     * @return Vector2D
     */
    public static function normalize(Vector2D $vec) {
        $length = Vector2D::length($vec);

        if ($length != 0) {
            return new Vector2D($vec->x / $length, $vec->y / $length);
        }

        return $vec;
    }

    /**
     * Vector length
     * 
     * @param Vector2D $vec
     * @return float
     */
    public static function length(Vector2D $vec) {
        return sqrt(($vec->x * $vec->x) + ($vec->y * $vec->y));
    }

}
