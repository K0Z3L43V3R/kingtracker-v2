<?php

namespace AppBundle\Helper;

use DateTime;
use DateTimeZone;

class TimeHelper{
    
    /**
     * Get UTC offset from timezone
     * 
     * @param string $timezone
     * @return string
     */
    public static function getUTCOffset($timezone){
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone($timezone));
        return $date->format('P');
    }
    
    /**
     * Converts LFS (ms) time to string
     * 
     * @param int $int
     * 
     * @return string
     */
    public static function timeToString($int) {
        $hours = 0;
        $minutes = 0;
        
        $sec = floor($int / 1000);
        $ms = $int - floor($sec * 1000);
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return ($hours > 0 ? $hours . ':' : '') . (sprintf('%02d:%02d.%02d', $minutes, $sec, ($ms / 10)));
    }

    public static function secToString($sec) {
        $hours = 0;
        $minutes = 0;
        
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return ($hours > 0 ? $hours . ':' : '') . (sprintf('%02d:%02d', $minutes, $sec));
    }

    public static function diffToString($int, &$negative = false) {
        $hours = 0;
        $minutes = 0;
        
        $negative = $int < 0 ? true : false;
        $sign = $int == 0 ? '' : ($negative ? '-' : '+');
        $int = abs($int);
        $sec = floor($int / 1000);
        $ms = $int - floor($sec * 1000);
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return $sign . ($hours > 0 ? $hours . ':' : '') . ($minutes > 0 ? $minutes . ':' : '') . ("{$sec}.") . (sprintf('%02d', ($ms / 10)));
    }

    public static function stringToTime($string) {
        $t = explode('.', $string);

        $aTime = explode(':', $t[0]);

        if (count($aTime) > 2) {
            $timeString = "{$aTime[0]}:{$aTime[1]}:{$aTime[2]}";
        } else {
            $timeString = "00:{$aTime[0]}:{$aTime[1]}";
        }

        $sec = strtotime($timeString) - strtotime('TODAY');

        return ($sec * 1000) + (intval(@$t[1]) * 10);
    }
    
}
