<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Event\Button\ButtonClearRequestEvent;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Button\ButtonTypedEvent;
use AppBundle\Event\Command\CommandReceivedEvent;
use AppBundle\Event\Command\HiddenCommandReceivedEvent;
use AppBundle\Event\Host\HostConnectedEvent;
use AppBundle\Event\Host\HostDisconnectedEvent;
use AppBundle\Event\Host\StateChangedEvent;
use AppBundle\Event\Packet\AXIReceivedEvent;
use AppBundle\Event\Packet\HLVReceivedEvent;
use AppBundle\Event\Packet\RSTReceivedEvent;
use AppBundle\Event\Player\MCIReceivedEvent;
use AppBundle\Event\Player\PlayerCarContactEvent;
use AppBundle\Event\Player\PlayerCarResetEvent;
use AppBundle\Event\Player\PlayerConnectedEvent;
use AppBundle\Event\Player\PlayerConnectedInfoEvent;
use AppBundle\Event\Player\PlayerConnectionLeaveEvent;
use AppBundle\Event\Player\PlayerFlagsChangedEvent;
use AppBundle\Event\Player\PlayerJoinedTrackEvent;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\Player\PlayerObjectContactEvent;
use AppBundle\Event\Player\PlayerResultEvent;
use AppBundle\Event\Player\PlayerSpectatedEvent;
use AppBundle\Event\Player\PlayerSplitEvent;
use AppBundle\Event\SendPacketEvent;
use AppBundle\Event\SendQueuedPacketEvent;
use AppBundle\Packet\IS_AXI;
use AppBundle\Packet\IS_BFN;
use AppBundle\Packet\IS_BTC;
use AppBundle\Packet\IS_BTT;
use AppBundle\Packet\IS_CNL;
use AppBundle\Packet\IS_CON;
use AppBundle\Packet\IS_CRS;
use AppBundle\Packet\IS_HLV;
use AppBundle\Packet\IS_III;
use AppBundle\Packet\IS_ISI;
use AppBundle\Packet\IS_LAP;
use AppBundle\Packet\IS_MCI;
use AppBundle\Packet\IS_MSO;
use AppBundle\Packet\IS_NCI;
use AppBundle\Packet\IS_NCN;
use AppBundle\Packet\IS_NPL;
use AppBundle\Packet\IS_OBH;
use AppBundle\Packet\IS_PFL;
use AppBundle\Packet\IS_PLL;
use AppBundle\Packet\IS_RES;
use AppBundle\Packet\IS_RST;
use AppBundle\Packet\IS_SPX;
use AppBundle\Packet\IS_STA;
use AppBundle\Packet\IS_TINY;
use AppBundle\Packet\IS_VER;
use AppBundle\Packet\Packet;
use AppBundle\Types\ConnectionStatus;
use AppBundle\Types\InsimFlags;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InsimService extends CoreService {

    /**
     * Delay between sending packets to player in ms
     */
    const DELAY_PACKET_SEND = 6;

    /** Keep alive time */
    const KEEP_ALIVE_TIME = 80;

    /** @var resource|null */
    public $socket;

    /** @var resource|null */
    public $UDPsocket;

    /** @var int */
    public $connectionStatus;

    /** @var Host */
    public $host;

    /** @var string TCP stream buffer */
    private $streamBuf = '';

    /** @var int */
    private $streamBufLen = 0;

    /** @var string UDP stream buffer */
    private $streamBufUDP = '';

    /** @var int */
    private $streamBufLenUDP = 0;

    /** @var array Player based packets queue */
    private $sendQueue = array();

    /** @var array Time (ms) of last sent packet to player */
    private $lastSent = array();

    /** @var array Host packets queue */
    private $hostQueue = array();

    /** @var int Time (ms) of last sent packet to host */
    private $lastSentHost = 0;

    /** @var int */
    private $lastPing = 0;

    public function __construct(EventDispatcherInterface $eventDispacher, EntityManager $entityManger) {
        parent::__construct($eventDispacher, $entityManger);

        $this->eventDispacher->addListener(SendQueuedPacketEvent::NAME, [$this, 'onPacketQueued'], 2040);
        $this->eventDispacher->addListener(SendPacketEvent::NAME, [$this, 'onPacketSend'], 2040);
    }

    /**
     * Connect insim to host
     * @return boolean
     */
    public function connect() {
        $this->connectionStatus = ConnectionStatus::CONNECTING;

        try {
            // Create TCP socket
            $this->socket = $this->createTCPsocket($this->host->getIp(), $this->host->getPort(), $timeout = 100);

            // Create UDP socket
            if ($this->host->getUdpPort()) {
                $this->UDPsocket = $this->createUDPsocket('0.0.0.0', $this->host->getUdpPort());
            }
        } catch (Exception $ex) {
            // Timed out
            return false;
        }

        // Send insim init packet
        $ISP = new IS_ISI();
        $ISP->ReqI = 1;
        $ISP->UDPPort = !empty($this->host->getUdpPort()) ? $this->host->getUdpPort() : 0;
        $ISP->Flags = InsimFlags::ISF_MCI + InsimFlags::ISF_HLV + InsimFlags::ISF_CON + InsimFlags::ISF_OBH;

        if ($this->host->isLocal()) {
            $ISP->Flags += InsimFlags::ISF_LOCAL;
        }

        $ISP->Prefix = ord($this->host->getPreffix() ? $this->host->getPreffix() : '!');
        $ISP->Interval = 250;
        $ISP->Admin = $this->host->getPassword();
        $ISP->IName = 'KingTracker';

        $this->send($ISP);
    }

    /**
     * Disconnect host
     */
    public function disconnect() {
        $packet = new IS_TINY();
        $packet->ReqI = 1;
        $packet->SubT = IS_TINY::TINY_CLOSE;

        $this->send($packet);

        $this->eventDispacher->dispatch(HostDisconnectedEvent::NAME, new HostDisconnectedEvent($this->host));

        socket_close($this->socket);
        socket_close($this->UDPsocket);

        $this->eventDispacher->removeListener(SendQueuedPacketEvent::NAME, [$this, 'onPacketQueued']);
        $this->eventDispacher->removeListener(SendPacketEvent::NAME, [$this, 'onPacketSend']);

        unset($this->host);
    }

    /**
     * Host timeout
     */
    public function timeout() {
        $this->eventDispacher->dispatch(HostDisconnectedEvent::NAME, new HostDisconnectedEvent($this->host));

        socket_close($this->socket);
        socket_close($this->UDPsocket);

        $this->eventDispacher->removeListener(SendQueuedPacketEvent::NAME, [$this, 'onPacketQueued']);
        $this->eventDispacher->removeListener(SendPacketEvent::NAME, [$this, 'onPacketSend']);

        unset($this->host);
    }

    /**
     * Insim tick
     */
    public function tick() {
        if (!($this->connectionStatus == ConnectionStatus::CONNECTING OR $this->connectionStatus == ConnectionStatus::CONNECTED))
            return;

        $this->readTCPsocket();
        $this->readUDPsocket();

        // Send queued packets to players
        foreach ($this->sendQueue as $ucid => $que) {
            if (count($que) > 0) {
                $lastSent = isset($this->lastSent[$ucid]) ? $this->lastSent[$ucid] : 0;
                if ((microtime(true) * 1000) - $lastSent >= self::DELAY_PACKET_SEND) {
                    $temp = array_shift($this->sendQueue[$ucid]);
                    socket_write($this->socket, $temp->pack());

                    $this->lastSent[$ucid] = microtime(true) * 1000;
                }
            }
        }
    }

    /**
     * Send packet
     * @param Packet $packet
     */
    public function send(Packet $packet) {
        if (is_resource($this->socket)) {
            try {
                socket_write($this->socket, $packet->pack());
            } catch (Exception $ex) {
                
            }
        }
    }

    public function handlePacket($rawPacket) {
        // Check packet size
        if ((strlen($rawPacket) % 4) > 0) {
            $this->clearBuffer();
            return false;
        }

        // Parse Packet Header
        $header = unpack('CSize/CType/CReqI/CSubT', $rawPacket);

        $this->maintainConnection($header, $rawPacket);

        // Debug received packet
        if ($header['Type'] != 38 && $header['Type'] != 3) {
            $this->logEvent('Packet received: ' . Packet::getPacketType($header['Type']), OutputInterface::VERBOSITY_VERY_VERBOSE);
        }

        // Dispatch events
        switch ($header['Type']) {
            case Packet::ISP_VER: // Version
                $this->eventDispacher->dispatch(HostConnectedEvent::NAME, new HostConnectedEvent($this->host, new IS_VER($rawPacket)));
                break;
            case Packet::ISP_NCN: // New connection
                $this->eventDispacher->dispatch(PlayerConnectedEvent::NAME, new PlayerConnectedEvent($this->host, new IS_NCN($rawPacket)));
                break;
            case Packet::ISP_NCI: // New connection info
                $this->eventDispacher->dispatch(PlayerConnectedInfoEvent::NAME, new PlayerConnectedInfoEvent($this->host, new IS_NCI($rawPacket)));
                break;
            case Packet::ISP_CNL: // Connection leave
                $this->eventDispacher->dispatch(PlayerConnectionLeaveEvent::NAME, new PlayerConnectionLeaveEvent($this->host, new IS_CNL($rawPacket)));
                break;
            case Packet::ISP_NPL: // Player joined track
                $this->eventDispacher->dispatch(PlayerJoinedTrackEvent::NAME, new PlayerJoinedTrackEvent($this->host, new IS_NPL($rawPacket)));
                break;
            case Packet::ISP_PLL: // Player spectated
                $this->eventDispacher->dispatch(PlayerSpectatedEvent::NAME, new PlayerSpectatedEvent($this->host, new IS_PLL($rawPacket)));
                break;
            case Packet::ISP_PFL: // Player flags changed
                $this->eventDispacher->dispatch(PlayerFlagsChangedEvent::NAME, new PlayerFlagsChangedEvent($this->host, new IS_PFL($rawPacket)));
                break;
            case Packet::ISP_MSO: // Message OUT
                $this->eventDispacher->dispatch(CommandReceivedEvent::NAME, new CommandReceivedEvent($this->host, new IS_MSO($rawPacket)));
                //$p = new IS_MSO($rawPacket);
                //dump(iconv("CP1252", "UTF-8//TRANSLIT", $p->Msg)); 
                break;
            case Packet::ISP_III: // Hidden message
                $this->eventDispacher->dispatch(HiddenCommandReceivedEvent::NAME, new HiddenCommandReceivedEvent($this->host, new IS_III($rawPacket)));
                break;
            case Packet::ISP_BTC: // Button clicked
                $this->eventDispacher->dispatch(ButtonClickedEvent::NAME, new ButtonClickedEvent($this->host, new IS_BTC($rawPacket)));
                break;
            case Packet::ISP_BTT: // Button typed
                $this->eventDispacher->dispatch(ButtonTypedEvent::NAME, new ButtonTypedEvent($this->host, new IS_BTT($rawPacket)));
                break;
            case Packet::ISP_BFN: // Button clear request SHIFT+I / SHIFT+B
                $this->eventDispacher->dispatch(ButtonClearRequestEvent::NAME, new ButtonClearRequestEvent($this->host, new IS_BFN($rawPacket)));
                break;
            case Packet::ISP_MCI: // Multi car info received
                $this->eventDispacher->dispatch(MCIReceivedEvent::NAME, new MCIReceivedEvent($this->host, new IS_MCI($rawPacket)));
                break;
            case Packet::ISP_RST: // Race start
                $this->eventDispacher->dispatch(RSTReceivedEvent::NAME, new RSTReceivedEvent($this->host, new IS_RST($rawPacket)));
                break;
            case Packet::ISP_STA: // State changed
                $this->eventDispacher->dispatch(StateChangedEvent::NAME, new StateChangedEvent($this->host, new IS_STA($rawPacket)));
                break;
            case Packet::ISP_AXI: // Layout info
                $this->eventDispacher->dispatch(AXIReceivedEvent::NAME, new AXIReceivedEvent($this->host, new IS_AXI($rawPacket)));
                break;
            case Packet::ISP_SPX: // Split
                $this->eventDispacher->dispatch(PlayerSplitEvent::NAME, new PlayerSplitEvent($this->host, new IS_SPX($rawPacket)));
                break;
            case Packet::ISP_LAP: // Lap
                $this->eventDispacher->dispatch(PlayerLapEvent::NAME, new PlayerLapEvent($this->host, new IS_LAP($rawPacket)));
                break;
            case Packet::ISP_RES: // Result (qualify or confirmed finish)
                $this->eventDispacher->dispatch(PlayerResultEvent::NAME, new PlayerResultEvent($this->host, new IS_RES($rawPacket)));
                break;
            case Packet::ISP_HLV: // Reports of incidents that would violate HLVC
                $this->eventDispacher->dispatch(HLVReceivedEvent::NAME, new HLVReceivedEvent($this->host, new IS_HLV($rawPacket)));
                break;
            case Packet::ISP_CON: // Car contact
                $this->eventDispacher->dispatch(PlayerCarContactEvent::NAME, new PlayerCarContactEvent($this->host, new IS_CON($rawPacket)));
                break;
            case Packet::ISP_OBH: // Object contact
                $this->eventDispacher->dispatch(PlayerObjectContactEvent::NAME, new PlayerObjectContactEvent($this->host, new IS_OBH($rawPacket)));
                break;
            case Packet::ISP_CRS: // Car reset
                $this->eventDispacher->dispatch(PlayerCarResetEvent::NAME, new PlayerCarResetEvent($this->host, new IS_CRS($rawPacket)));
                break;
        }
    }

    public function onPacketQueued(SendQueuedPacketEvent $event) {
        if ($this->host->getId() != $event->hostId) {
            return;
        }

        $this->sendQueue[$event->UCID][] = $event->packet;

        $event->stopPropagation();
    }

    public function onPacketSend(SendPacketEvent $event) {
        if ($this->host->getId() != $event->hostId) {
            return;
        }

        $this->send($event->packet);

        $event->stopPropagation();
    }

    /**
     * Maintain insim connection
     * @param array $header
     * @param string $rawPacket
     */
    private function maintainConnection($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_TINY:
                if ($header['SubT'] == IS_TINY::TINY_NONE) {   // Ping back
                    $ping = new IS_TINY();
                    $ping->SubT = IS_TINY::TINY_NONE;
                    $this->send($ping);

                    $this->lastPing = time();
                }
                break;
        }

        // TimeOut detecion
        if ($this->lastPing != 0 && (time() - $this->lastPing) >= self::KEEP_ALIVE_TIME) {
            // TODO: Server missed ping, try to recover
            //$this->getHostService()->timeout('Timeout');
        }
    }

    /**
     * Create socket
     * 
     * @param string $ip
     * @param int $port
     * @param int $timeout
     * 
     * @return resource
     * @throws Exception
     */
    private function createTCPsocket($ip, $port, $timeout = 100) {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        /**
         * Set the send and receive timeouts super low so that socket_connect
         * will return to us quickly. We then loop and check the real timeout
         * and check the socket error to decide if its conected yet or not.
         */
        $connect_timeval = array(
            "sec" => 0,
            "usec" => 100
        );
        socket_set_option(
                $socket, SOL_SOCKET, SO_SNDTIMEO, $connect_timeval
        );
        socket_set_option(
                $socket, SOL_SOCKET, SO_RCVTIMEO, $connect_timeval
        );
        $now = microtime(true);

        /**
         * Loop calling socket_connect. As long as the error is 115 (in progress)
         * or 114 (already called) and our timeout has not been reached, keep
         * trying.
         */
        $err = null;
        $socket_connected = false;
        $elapsed = null;

        do {
            try {
                socket_clear_error($socket);
                $socket_connected = socket_connect($socket, $ip, $port);
                $err = socket_last_error($socket);
                $elapsed = (microtime(true) - $now) * 1000;
            } catch (Exception $ex) {
                $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($socket)));
                $this->logEvent("ERROR - {$message} ({$this->host->getName()})");
            }
        } while (($err === 115 || $err === 114) && $elapsed < $timeout);

        /**
         * For some reason, socket_connect can return true even when it is
         * not connected. Make sure it returned true the last error is zero
         */
        $socket_connected = $socket_connected && $err === 0;

        if ($socket_connected) {
            /**
             * Set keep alive on so the other side does not drop us
             */
            socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1);

            /**
             * set the real send/receive timeouts here now that we are connected
             */
            $timeval = array(
                "sec" => 0,
                "usec" => 0
            );
            if ($timeout >= 1000) {
                $ts_seconds = $timeout / 1000;
                $timeval["sec"] = floor($ts_seconds);
                $timeval["usec"] = ($ts_seconds - $timeval["sec"]) * 1000000;
            } else {
                $timeval["usec"] = $timeout * 1000;
            }
            socket_set_option(
                    $socket, SOL_SOCKET, SO_SNDTIMEO, $timeval
            );
            socket_set_option(
                    $socket, SOL_SOCKET, SO_RCVTIMEO, $timeval
            );
        } else {
            $elapsed = round($elapsed, 4);
            if (!is_null($err) && $err !== 0 && $err !== 114 && $err !== 115) {
                $msg = str_replace(PHP_EOL, '', socket_strerror($err));
                $message = "Failed to connect to {$this->host->getName()}. ({$err}: {$msg})";
            } else {
                $message = "Failed to connect to {$this->host->getName()}. (timed out after {$elapsed}ms)";
            }

            $this->logEvent("ERROR - {$message})");
        }

        socket_set_nonblock($socket);
        return $socket;
    }

    /**
     * Create UDP socket
     * 
     * @param string $ip
     * @param int $port
     * 
     * @return resource
     */
    private function createUDPsocket($ip, $port) {
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        if ($socket) {
            socket_set_nonblock($socket);

            if (!socket_bind($socket, $ip, $port)) {
                $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($socket)));
                $this->logEvent("ERROR - {$message} ({$this->host->getName()})");
            }
        } else {
            $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($socket)));
            $this->logEvent("ERROR - {$message} ({$this->host->getName()})");
        }

        return $socket;
    }

    private function readTCPsocket() {
        if (!is_resource($this->socket)) {
            return;
        }

        if (!$this->socket) {
            return;
        }

        // Read TCP packets
        $read = array($this->socket);
        $write = array();
        $except = array($this->socket);

        $num_changed_sockets = @socket_select($read, $write, $except, 0);

        if ($num_changed_sockets === false) {
            $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->socket)));
            $this->logEvent("ERROR - {$message} ({$this->host->getName()})");
            $this->timeout();
            $this->sendQueue = array();
            $this->lastSent = array();
            $this->hostQueue = array();
            $this->lastSentHost = 0;
        } else if ($num_changed_sockets > 0) {
            try {
                $rawPacket = socket_read($this->socket, 8192);
            } catch (Exception $ex) {
                $rawPacket = false;
            }

            if ($rawPacket !== false) {
                $this->appendToBuffer($rawPacket);
                while (true) {
                    $packet = $this->findNextPacket();
                    if (!$packet)
                        break;

                    // Handle received packet
                    $this->handlePacket($packet);
                }
            } else {
                $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->socket)));
                $this->logEvent("ERROR - {$message} ({$this->host->getName()})");
                $this->timeout();
                $this->sendQueue = array();
                $this->lastSent = array();
                $this->hostQueue = array();
                $this->lastSentHost = 0;
                $this->socket = false;
            }
        }
    }

    private function readUDPsocket() {
        if (!is_resource($this->UDPsocket)) {
            return;
        }

        if (!$this->UDPsocket) {
            return;
        }

        $read = array($this->UDPsocket);
        $write = array();
        $except = array($this->UDPsocket);

        $num_changed_sockets = @socket_select($read, $write, $except, 0);

        if ($num_changed_sockets === false) {
            $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->UDPsocket)));
            $this->logEvent('Socket error: ' . $this->host->getName() . ' - ' . $message);
        } else if ($num_changed_sockets > 0) {
            try {
                $rawPacket = socket_read($this->UDPsocket, 8192);
            } catch (Exception $ex) {
                $rawPacket = false;
            }

            if ($rawPacket !== false) {
                $this->appendToBufferUDP($rawPacket);
                while (true) {
                    $packet = $this->findNextPacketUDP();
                    if (!$packet)
                        break;

                    // Handle received packet
                    $this->handlePacket($packet);
                }
            } else {
                $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->UDPsocket)));
                $this->logEvent("ERROR - {$message} ({$this->host->getName()})");

                $this->UDPsocket = false;
            }
        }
    }

    private function appendToBuffer(&$data) {
        $this->streamBuf .= $data;
        $this->streamBufLen = strlen($this->streamBuf);
    }

    private function clearBuffer() {
        $this->streamBuf = '';
        $this->streamBufLen = 0;
    }

    private function findNextPacket() {
        if ($this->streamBufLen == 0)
            return false;

        $sizebyte = ord($this->streamBuf[0]);
        if ($sizebyte == 0) {
            return false;
        } else if ($this->streamBufLen < $sizebyte) {
            return false;
        }

        $packet = substr($this->streamBuf, 0, $sizebyte);
        $packetType = ord($packet[1]);

        $this->streamBuf = substr($this->streamBuf, $sizebyte);
        $this->streamBufLen = strlen($this->streamBuf);

        return $packet;
    }

    private function appendToBufferUDP(&$data) {
        $this->streamBufUDP .= $data;
        $this->streamBufLenUDP = strlen($this->streamBufUDP);
    }

    private function clearBufferUDP() {
        $this->streamBufUDP = '';
        $this->streamBufLenUDP = 0;
    }

    private function findNextPacketUDP() {
        if ($this->streamBufLenUDP == 0)
            return false;

        $sizebyte = ord($this->streamBufUDP[0]);
        if ($sizebyte == 0) {
            return false;
        } else if ($this->streamBufLenUDP < $sizebyte) {
            return false;
        }

        $packet = substr($this->streamBufUDP, 0, $sizebyte);
        $packetType = ord($packet[1]);

        $this->streamBufUDP = substr($this->streamBufUDP, $sizebyte);
        $this->streamBufLenUDP = strlen($this->streamBufUDP);

        return $packet;
    }

}
