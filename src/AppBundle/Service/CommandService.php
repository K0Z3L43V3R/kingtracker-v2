<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Event\Command\CommandReceivedEvent;
use AppBundle\Event\Command\CommandRegisteredEvent;
use AppBundle\Event\Command\HiddenCommandReceivedEvent;
use AppBundle\Event\Host\HostDisconnectedEvent;
use AppBundle\Packet\IS_MSO;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CommandService extends CoreService {

    /** @var Host */
    public $host;

    /** @var CommandRegisteredEvent[] */
    public $commands;

    public function __construct(EventDispatcherInterface $eventDispacher) {
        parent::__construct($eventDispacher);

        $this->eventDispacher->addListener(CommandReceivedEvent::NAME, [$this, 'onCommandReceived'], 2048);
        $this->eventDispacher->addListener(HiddenCommandReceivedEvent::NAME, [$this, 'onHiddenCommandReceived'], 2048);
        $this->eventDispacher->addListener(CommandRegisteredEvent::NAME, [$this, 'onCommandRegistered'], 2048);
        $this->eventDispacher->addListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected'], 2048);
    }

    /**
     * @param CommandReceivedEvent $event
     */
    public function onCommandReceived(CommandReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $isCommand = false;

        switch ($event->packet->UserType) {
            case IS_MSO::MSO_PREFIX:
            case IS_MSO::MSO_USER:
            case IS_MSO::MSO_O:
                $isCommand = $this->processUserCommands($event->packet);
                break;
            case IS_MSO::MSO_SYSTEM:
                $this->processSystemMessages($event->packet);

                if ($this->host->isLocal()) {
                    $isCommand = $this->processUserCommands($event->packet);
                }
                break;
        }

        if (!$isCommand) {
            $event->stopPropagation();
        }
    }

    public function onHiddenCommandReceived(HiddenCommandReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        print_r($event->packet);
    }

    /**
     * @param CommandRegisteredEvent $event
     */
    public function onCommandRegistered(CommandRegisteredEvent $event) {
        $this->commands[$event->command] = $event;
    }

    /**
     * Event called on host disconnected
     * 
     * @param HostDisconnectedEvent $event
     */
    public function onHostDisconnected(HostDisconnectedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->eventDispacher->removeListener(CommandReceivedEvent::NAME, [$this, 'onCommandReceived']);
        $this->eventDispacher->removeListener(HiddenCommandReceivedEvent::NAME, [$this, 'onHiddenCommandReceived']);
        $this->eventDispacher->removeListener(CommandRegisteredEvent::NAME, [$this, 'onCommandRegistered']);
        $this->eventDispacher->removeListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected']);
    }

    /**
     * @param IS_MSO $packet
     */
    private function processUserCommands(IS_MSO $packet) {
        $prefix = '!';

        $string_original = substr($packet->Msg, $packet->TextStart);
        $string = strtolower(substr($packet->Msg, $packet->TextStart));

        if (substr($string, 0, 1) != $prefix) {
            return false;
        }

        foreach ($this->commands as $key => $command) {
            if (strpos($string, $prefix . $key) !== false) {
                if (is_callable($command->callback)) {
                    $player = $this->host->playerService->getPlayerByUCID($packet->UCID);
                    $arguments = $this->parseArguments($command, $string);

                    if ($arguments === false) {
                        $help = '';

                        foreach ($command->arguments as $arg) {
                            $help .= (!empty($help) ? ', ' : '') . $arg['title'];
                        }

                        $player->sendErrorMsg('cmd.incorrect', ['%format%' => $help]);
                    }

                    call_user_func($command->callback, $player, $this->host, $arguments);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param IS_MSO $packet
     */
    private function processSystemMessages(IS_MSO $packet) {
        // TODO: implement me
    }

    /**
     * 
     * @param CommandRegisteredEvent $command
     * @param string $string
     * @return boolean|array
     */
    private function parseArguments(CommandRegisteredEvent $command, $string) {
        $prefix = '!';
        $requiredArguments = 0;
        $arguments = [];
        $returnArgs = [];

        preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $string, $matches);
        if ($matches[0] !== null) {
            $arguments = $matches[0];
            unset($arguments[0]);
        }

        $arguments = array_values($arguments);

        $index = 0;
        foreach ($command->arguments as $key => $arg) {
            if (isset($arg['required']) && $arg['required']) {
                $requiredArguments++;
            }

            if (isset($arg['type']) && isset($arguments[$index])) {
                switch ($arg['type']) {
                    case 'string':
                        $returnArgs[$key] = $this->parseString($arguments[$index]);
                        break;
                    case 'int':
                        $returnArgs[$key] = $this->parseInt($arguments[$index]);
                        break;
                }
            }
            $index++;
        }

        if (count($arguments) < $requiredArguments) {
            return false;
        }

        return $returnArgs;
    }

    private function parseString($value) {
        return trim($value);
    }

    private function parseInt($value) {
        return intval(trim($value));
    }

}
