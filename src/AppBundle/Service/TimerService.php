<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Entity\Timer;
use AppBundle\Service\CoreService;
use DateTime;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TimerService extends CoreService {

    /** @var Host */
    public $host;
    private $timer_sec_last = 0;
    private $timers = [];

    public function __construct(EventDispatcherInterface $eventDispacher) {
        parent::__construct($eventDispacher);
    }

    public function __destruct() {
        
    }

    /**
     * 
     * @param Timer $timer
     */
    public function addTimer(Timer $timer) {
        $this->timers[$timer->uid] = $timer;
    }

    /**
     * 
     * @param string|null $uuid
     */
    public function removeTimer($uuid = null) {
        if ($uuid) {
            unset($this->timers[$uuid]);
        } else {
            $this->timers = [];
        }
    }

    /**
     * Get timer by name
     * @param string | array $name
     * @return Timer|null
     */
    public function getTimerByName($name) {
        if (is_array($name)) {
            foreach ($this->timers as $tm) {
                if (in_array($tm->name, $name)) {
                    return $tm;
                }
            }
        } else {
            foreach ($this->timers as $tm) {
                if ($tm->name == $name) {
                    return $tm;
                }
            }
        }

        return null;
    }

    public function tick() {
        if (time() - $this->timer_sec_last >= 1) {            
            if ($this->timers) {
                foreach ($this->timers as $timer) {
                    $timer->tick();

                    if ($timer->state == Timer::TIMER_FINISHED) {
                        unset($this->timers[$timer->uid]);
                    }
                }
            }

            $this->timer_sec_last = time();
        }
    }

}
