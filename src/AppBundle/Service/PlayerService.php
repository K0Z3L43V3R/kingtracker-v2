<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Entity\HostConnection;
use AppBundle\Entity\Player;
use AppBundle\Entity\PlayerCar;
use AppBundle\Event\Button\ButtonClearRequestEvent;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Button\ButtonTypedEvent;
use AppBundle\Event\Command\CommandReceivedEvent;
use AppBundle\Event\HLVC\HLVCBoundsEvent;
use AppBundle\Event\HLVC\HLVCGroundEvent;
use AppBundle\Event\HLVC\HLVCPitSpeedingEvent;
use AppBundle\Event\HLVC\HLVCSlipstreamEvent;
use AppBundle\Event\HLVC\HLVCWallEvent;
use AppBundle\Event\Host\HostDisconnectedEvent;
use AppBundle\Event\Packet\HLVReceivedEvent;
use AppBundle\Event\Player\MCIReceivedEvent;
use AppBundle\Event\Player\PlayerCarContactEvent;
use AppBundle\Event\Player\PlayerCarResetEvent;
use AppBundle\Event\Player\PlayerConnectedEvent;
use AppBundle\Event\Player\PlayerConnectedInfoEvent;
use AppBundle\Event\Player\PlayerConnectionLeaveEvent;
use AppBundle\Event\Player\PlayerFlagsChangedEvent;
use AppBundle\Event\Player\PlayerJoinedTrackEvent;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\Player\PlayerObjectContactEvent;
use AppBundle\Event\Player\PlayerResultEvent;
use AppBundle\Event\Player\PlayerSpectatedEvent;
use AppBundle\Event\Player\PlayerSplitEvent;
use AppBundle\Helper\Math\Math;
use AppBundle\Repository\HostConnectionRepository;
use AppBundle\Repository\PlayerRepository;
use AppBundle\Types\HLVCType;
use AppBundle\Types\LFSLanguage;
use AppBundle\Types\PlayerFlags;
use AppBundle\Types\PlayerStatus;
use AppBundle\Types\SetupFlags;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PlayerService extends CoreService {

    /** @var Host */
    public $host;

    /** @var PlayerRepository|EntityRepository */
    private $playerRepo;

    /** @var HostConnectionRepository|EntityRepository */
    private $connectionRepo;

    /** @var ContainerInterface */
    private $container;

    /**
     * @var Player[] Players references
     */
    public $players = [];

    /** @var int Total count of players spectating */
    public $playersSpectating = 0;

    /** @var int Total count of players on track */
    public $playersOnTrack = 0;

    /** @var int Total count of players with updated position (MCI received) */
    public $playersUpdated = 0;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $eventDispacher, EntityManager $entityManger) {
        parent::__construct($eventDispacher, $entityManger);

        $this->playerRepo = $entityManger->getRepository(Player::class);
        $this->connectionRepo = $entityManger->getRepository(HostConnection::class);
        $this->container = $container;

        $this->eventDispacher->addListener(CommandReceivedEvent::NAME, [$this, 'onCommandReceived'], 2040);
        $this->eventDispacher->addListener(ButtonClickedEvent::NAME, [$this, 'onButtonClicked'], 2040);
        $this->eventDispacher->addListener(ButtonTypedEvent::NAME, [$this, 'onButtonTyped'], 2040);
        $this->eventDispacher->addListener(ButtonClearRequestEvent::NAME, [$this, 'onButtonClearRequest'], 2040);

        $this->eventDispacher->addListener(PlayerConnectedEvent::NAME, [$this, 'onPlayerConnected'], 100);
        $this->eventDispacher->addListener(PlayerConnectedInfoEvent::NAME, [$this, 'onPlayerConnectedInfo'], 100);
        $this->eventDispacher->addListener(PlayerConnectionLeaveEvent::NAME, [$this, 'onPlayerLeftHost'], 100);
        $this->eventDispacher->addListener(PlayerConnectionLeaveEvent::NAME, [$this, 'onPlayerLeftHostGC'], -2048);
        $this->eventDispacher->addListener(PlayerFlagsChangedEvent::NAME, [$this, 'onPlayerFlagsChanged'], 100);
        $this->eventDispacher->addListener(PlayerCarContactEvent::NAME, [$this, 'onPlayerCarContact'], 2048);
        $this->eventDispacher->addListener(PlayerObjectContactEvent::NAME, [$this, 'onPlayerObjectContact'], 2048);
        $this->eventDispacher->addListener(PlayerCarResetEvent::NAME, [$this, 'onPlayerCarReset'], 2048);

        $this->eventDispacher->addListener(PlayerJoinedTrackEvent::NAME, [$this, 'onPlayerJoinedTrack'], 100);
        $this->eventDispacher->addListener(PlayerSpectatedEvent::NAME, [$this, 'onPlayerSpectated'], 100);

        $this->eventDispacher->addListener(MCIReceivedEvent::NAME, [$this, 'onMCIReceived'], 2048);
        $this->eventDispacher->addListener(HLVReceivedEvent::NAME, [$this, 'onHLVReceived'], 2048);
        $this->eventDispacher->addListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected'], 2048);

        $this->eventDispacher->addListener(PlayerSplitEvent::NAME, [$this, 'onSplitEvent'], 2048);
        $this->eventDispacher->addListener(PlayerLapEvent::NAME, [$this, 'onLapEvent'], 2048);
        $this->eventDispacher->addListener(PlayerResultEvent::NAME, [$this, 'onResultEvent'], 2048);
    }

    public function tick() {
        foreach ($this->players as $player) {
            if ($player->getUi() !== null) {
                $player->getUi()->tick();
            }
        }
    }

    /**
     * Event called on player connected
     * 
     * @param PlayerConnectedEvent $event
     */
    public function onPlayerConnected(PlayerConnectedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Skip host connection
        if ($event->packet->UCID == 0) {
            return;
        }

        $this->logEvent("EVENT: PlayerConnected {Host: {$this->host->getName()}, UName: {$event->packet->UName}}", OutputInterface::VERBOSITY_VERBOSE);

        // Create Host connection
        $hostConnection = new HostConnection();
        $hostConnection->setUCID($event->packet->UCID);
        $hostConnection->setHost($event->host);
        $this->entityManager->persist($hostConnection);
        $this->entityManager->flush($hostConnection);

        // New player
        if (($player = $this->playerRepo->findOneBy(['UName' => $event->packet->UName])) === null) {
            $player = new Player();
        }

        // Update player
        $player->setUName($event->packet->UName);
        $player->setPName($event->packet->PName);
        $player->setStatus(PlayerStatus::SPECTATING);
        $player->setLastJoined(new DateTime());
        $player->setLanguage('EN');
        $player->setLocale('en_US');
        $player->setHostConnection($hostConnection);

        $this->entityManager->persist($player);
        $this->entityManager->flush($player);

        $ui = $this->container->get('app.ui.service');
        $ui->host = $this->host;

        $player->setUi($ui);
        $player->setTranslator(clone $this->host->translator);
        $player->setStint($this->container->get('app.stint.service'));

        $hostConnection->setPlayer($player);

        $this->entityManager->persist($hostConnection);
        $this->entityManager->flush($hostConnection);

        $this->players[$player->getId()] = $player;

        $event->player = $player;
    }

    /**
     * Event called on player connection info received
     * 
     * @param PlayerConnectedInfoEvent $event
     */
    public function onPlayerConnectedInfo(PlayerConnectedInfoEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Skip host connection
        if ($event->packet->UCID == 0) {
            return;
        }

        $player = $this->getPlayerByUCID($event->packet->UCID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerConnectionInfo {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $player->setIp($event->packet->IPAddress);
        $player->setUserId($event->packet->UserID);
        $player->setLanguage(LFSLanguage::getLang($event->packet->Language));
        $player->setLocale(LFSLanguage::getLocale($event->packet->Language));

        $this->entityManager->persist($player);
        $this->entityManager->flush($player);

        $event->player = $player;
    }

    /**
     * Event called on player leave
     * 
     * @param PlayerConnectionLeaveEvent $event
     */
    public function onPlayerLeftHost(PlayerConnectionLeaveEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->logEvent("EVENT: PlayerLeave {Host: {$this->host->getName()}, UCID: {$event->packet->UCID}}", OutputInterface::VERBOSITY_VERBOSE);

        if (($hostConnection = $this->connectionRepo->findOneBy(['UCID' => $event->packet->UCID]))) {
            $this->entityManager->remove($hostConnection);
            $this->entityManager->flush($hostConnection);
        }

        $player = $this->getPlayerByUCID($event->packet->UCID);

        if (!$player instanceof Player) {
            return;
        }

        foreach ($this->players as $key => $player) {
            if ($player->getHostConnection()->getUCID() === $event->packet->UCID) {
                unset($this->players[$key]);
                break;
            }
        }

        $event->player = $player;
    }

    /**
     * Garbage collection when player leaves host
     * 
     * @param PlayerConnectionLeaveEvent $event
     */
    public function onPlayerLeftHostGC(PlayerConnectionLeaveEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->entityManager->detach($event->player);

        $event->player->__destruct();
        unset($this->players[$event->player->getId()]);
        unset($event->player);

        $event->stopPropagation();
    }

    /**
     * Event called when player joins track
     * 
     * @param PlayerJoinedTrackEvent $event
     */
    public function onPlayerJoinedTrack(PlayerJoinedTrackEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByUCID($event->packet->UCID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerJoinedTrack {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        if (($hostConnection = $this->connectionRepo->findOneBy(['UCID' => $event->packet->UCID]))) {
            $hostConnection->setPLID($event->packet->PLID);
            $this->entityManager->persist($hostConnection);
            $this->entityManager->flush($hostConnection);
        }

        $player->setPlayerFlags(new PlayerFlags($event->packet->Flags));
        $player->setSetupFlags(new SetupFlags($event->packet->SetF));
        $player->setStatus(PlayerStatus::ONTRACK);
        $player->setCar(new PlayerCar($event->packet));

        $this->entityManager->persist($player);
        $this->entityManager->flush($player);

        $event->player = $player;
    }

    /**
     * Event called when player spectate
     * 
     * @param PlayerSpectatedEvent $event
     */
    public function onPlayerSpectated(PlayerSpectatedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerSpectated {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        if (($hostConnection = $this->connectionRepo->findOneBy(['PLID' => $event->packet->PLID]))) {
            $hostConnection->setPLID(null);
            $this->entityManager->persist($hostConnection);
            $this->entityManager->flush($hostConnection);
        }

        $player->setStatus(PlayerStatus::SPECTATING);
        $player->setCar(null);

        $this->entityManager->persist($player);
        $this->entityManager->flush($player);

        $event->player = $player;
    }

    /**
     * Event called when player flags changed
     * 
     * @param PlayerFlagsChangedEvent $event
     */
    public function onPlayerFlagsChanged(PlayerFlagsChangedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerFlagsChanged {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $player->setPlayerFlags(new PlayerFlags($event->packet->Flags));

        $this->entityManager->persist($player);
        $this->entityManager->flush($player);

        $event->player = $player;
    }

    /**
     * Event called when player hit another car
     * 
     * @param PlayerCarContactEvent $event
     */
    public function onPlayerCarContact(PlayerCarContactEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Skip resent car contact event
        if ($event->player) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->A->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerCarContact {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;

        if (($playerB = $this->getPlayerByPLID($event->packet->B->PLID))) {
            $event->playerB = $playerB;

            // Resent car contact event to other car
            $newEvent = clone $event;
            $newEvent->player = $playerB;
            $newEvent->playerB = $player;
            $this->eventDispacher->dispatch(PlayerCarContactEvent::NAME, $newEvent);
        }
    }

    /**
     * Event called when player hit object
     * 
     * @param PlayerObjectContactEvent $event
     */
    public function onPlayerObjectContact(PlayerObjectContactEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerObjectContact {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;
    }

    /**
     * Event called when player resets car
     * 
     * @param PlayerCarResetEvent $event
     */
    public function onPlayerCarReset(PlayerCarResetEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerCarReset {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;
    }

    /**
     * Event called when player types command
     * 
     * @param CommandReceivedEvent $event
     */
    public function onCommandReceived(CommandReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Skip host connection
        if ($event->packet->UCID == 0) {
            return;
        }

        $event->player = $this->getPlayerByUCID($event->packet->UCID);
    }

    /**
     * Event called when player clicks on button
     * 
     * @param ButtonClickedEvent $event
     */
    public function onButtonClicked(ButtonClickedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $event->player = $this->getPlayerByUCID($event->packet->UCID);
    }

    /**
     * Event called when player types in button
     * 
     * @param ButtonTypedEvent $event
     */
    public function onButtonTyped(ButtonTypedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $event->player = $this->getPlayerByUCID($event->packet->UCID);
    }

    /**
     * Event called when player presses SHIFT+I / SHIFT+B
     * 
     * @param ButtonClickedEvent $event
     */
    public function onButtonClearRequest(ButtonClearRequestEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $event->player = $this->getPlayerByUCID($event->packet->UCID);
    }

    /**
     * Event called when MCI packet was received
     * 
     * @param MCIReceivedEvent $event
     */
    public function onMCIReceived(MCIReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->playersOnTrack = 0;
        $this->playersSpectating = 0;

        foreach ($this->players as $player) {
            if ($player->hasCar()) {
                $this->playersOnTrack++;
            } else {
                $this->playersSpectating++;
            }
        }

        if ($event->packet->NumC > 0) {
            foreach ($event->packet->Info as $compCar) {
                if (($player = $this->getPlayerByPLID($compCar->PLID)) !== null) {
                    if ($player->hasCar()) {
                        $player->getCar()->setMCI($compCar);
                        $player->setIsLagging($compCar->isLagging());
                    }
                }
            }
        }

        $this->playersUpdated += $event->packet->NumC;

        // All players updated
        if ($this->playersUpdated >= $this->playersOnTrack) {
            $this->playersUpdated = 0;

            foreach ($this->players as $playerTest) {
                // Slipstream detection
                if ($playerTest->hasCar() && $playerTest->getCar()->getPosition()) {
                    foreach ($this->players as $player) {
                        // Is not tested, car is on track and not laggging 
                        if ($player->hasCar() && $player->getHostConnection()->getPLID() != $playerTest->getHostConnection()->getPLID() && $player->getCar()->getPosition() && !$player->isLagging()) {
                            if (Math::isSlipStream($playerTest->getCar()->getPosition(), $player->getCar()->getPosition(), $playerTest->getCar(), $return)) {
                                $this->eventDispacher->dispatch(HLVCSlipstreamEvent::NAME, new HLVCSlipstreamEvent($this->host, $playerTest));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Event called when HLV packet was received
     * 
     * @param HLVReceivedEvent $event
     */
    public function onHLVReceived(HLVReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        switch ($event->packet->HLVC) {
            case HLVCType::HLVC_BOUNDS:
                $this->eventDispacher->dispatch(HLVCBoundsEvent::NAME, new HLVCBoundsEvent($this->host, $player));
                break;
            case HLVCType::HLVC_GROUND:
                $this->eventDispacher->dispatch(HLVCGroundEvent::NAME, new HLVCGroundEvent($this->host, $player));
                break;
            case HLVCType::HLVC_SPEEDING:
                $this->eventDispacher->dispatch(HLVCPitSpeedingEvent::NAME, new HLVCPitSpeedingEvent($this->host, $player));
                break;
            case HLVCType::HLVC_WALL:
                $this->eventDispacher->dispatch(HLVCWallEvent::NAME, new HLVCWallEvent($this->host, $player));
                break;
        }

        $type = new HLVCType($event->packet->HLVC);

        $this->logEvent("EVENT: PlayerHLVCEvent {Host: {$this->host->getName()}, UName: {$player->getUName()}, Type: {$type}}", OutputInterface::VERBOSITY_VERBOSE);
    }

    /**
     * Event called when player crosses split
     * 
     * @param PlayerSplitEvent $event
     */
    public function onSplitEvent(PlayerSplitEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerSplit {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;
    }

    /**
     * Event called when player crosses finish line
     * 
     * @param PlayerLapEvent $event
     */
    public function onLapEvent(PlayerLapEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerLap {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;
    }

    /**
     * Event called when player crosses finishes race / qualify
     * 
     * @param PlayerResultEvent $event
     */
    public function onResultEvent(PlayerResultEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $player = $this->getPlayerByPLID($event->packet->PLID);

        if (!$player instanceof Player) {
            return;
        }

        $this->logEvent("EVENT: PlayerResult {Host: {$this->host->getName()}, UName: {$player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);

        $event->player = $player;

        // Save player result
        if ($this->host->getResult() !== null) {
            $this->host->getResult()->addPlayerResult($player, $event->packet);
            $this->host->getResult()->setFinished($event->packet->NumRes);
            $this->entityManager->persist($this->host->getResult());
            $this->entityManager->flush($this->host->getResult());
        }
    }

    /**
     * Event called on host disconnected
     * 
     * @param HostDisconnectedEvent $event
     */
    public function onHostDisconnected(HostDisconnectedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->eventDispacher->removeListener(CommandReceivedEvent::NAME, [$this, 'onCommandReceived']);
        $this->eventDispacher->removeListener(ButtonClickedEvent::NAME, [$this, 'onButtonClicked']);
        $this->eventDispacher->removeListener(PlayerConnectedEvent::NAME, [$this, 'onPlayerConnected']);
        $this->eventDispacher->removeListener(PlayerConnectedInfoEvent::NAME, [$this, 'onPlayerConnectedInfo']);
        $this->eventDispacher->removeListener(PlayerConnectionLeaveEvent::NAME, [$this, 'onPlayerLeftHost']);
        $this->eventDispacher->removeListener(PlayerJoinedTrackEvent::NAME, [$this, 'onPlayerJoinedTrack']);
        $this->eventDispacher->removeListener(PlayerSpectatedEvent::NAME, [$this, 'onPlayerSpectated']);
        $this->eventDispacher->removeListener(PlayerFlagsChangedEvent::NAME, [$this, 'onPlayerFlagsChanged']);
        $this->eventDispacher->removeListener(MCIReceivedEvent::NAME, [$this, 'onMCIReceived']);
        $this->eventDispacher->removeListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected']);
        $this->eventDispacher->removeListener(PlayerSplitEvent::NAME, [$this, 'onSplitEvent']);
        $this->eventDispacher->removeListener(PlayerLapEvent::NAME, [$this, 'onLapEvent']);
    }

    /**
     * @param int $UCID
     * @return Player|null
     */
    public function &getPlayerByUCID($UCID) {
        $null = null;

        foreach ($this->players as $player) {
            if ($player->getHostConnection()->getUCID() === $UCID) {
                return $player;
            }
        }

        return $null;
    }

    /**
     * @param int $PLID
     * @return Player|null
     */
    public function &getPlayerByPLID($PLID) {
        $null = null;

        foreach ($this->players as $player) {
            if ($player->getHostConnection()->getPLID() === $PLID) {
                return $player;
            }
        }

        return $null;
    }

}
