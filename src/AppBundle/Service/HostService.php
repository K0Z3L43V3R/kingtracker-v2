<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Entity\HostConnection;
use AppBundle\Entity\HostSetting;
use AppBundle\Entity\Result;
use AppBundle\Entity\ResultEvent;
use AppBundle\Entity\Track;
use AppBundle\Event\Host\HostConnectedEvent;
use AppBundle\Event\Host\HostDisconnectedEvent;
use AppBundle\Event\Host\StateChangedEvent;
use AppBundle\Event\Packet\AXIReceivedEvent;
use AppBundle\Event\Packet\RSTReceivedEvent;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\SendPacketEvent;
use AppBundle\Packet\IS_MSL;
use AppBundle\Packet\IS_MST;
use AppBundle\Packet\IS_TINY;
use AppBundle\Repository\HostConnectionRepository;
use AppBundle\Repository\HostRepository;
use AppBundle\Repository\HostSettingRepository;
use AppBundle\Repository\TrackRepository;
use AppBundle\Types\ConnectionStatus;
use AppBundle\Types\Timing;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class HostService extends CoreService {

    /** @var Host */
    public $host;

    /** @var HostRepository|EntityRepository */
    private $hostRepo;

    /** @var HostConnectionRepository|EntityRepository */
    private $connectionRepo;

    /** @var TrackRepository|EntityRepository */
    private $trackRepo;

    /** @var HostSettingRepository|EntityRepository */
    private $hostSettingRepo;

    public function __construct(EventDispatcherInterface $eventDispacher, EntityManager $entityManger) {
        parent::__construct($eventDispacher, $entityManger);

        $this->hostRepo = $entityManger->getRepository(Host::class);
        $this->connectionRepo = $entityManger->getRepository(HostConnection::class);
        $this->trackRepo = $entityManger->getRepository(Track::class);
        $this->hostSettingRepo = $entityManger->getRepository(HostSetting::class);

        $this->eventDispacher->addListener(HostConnectedEvent::NAME, [$this, 'onHostConnected'], 2048);
        $this->eventDispacher->addListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected'], 2048);
        $this->eventDispacher->addListener(RSTReceivedEvent::NAME, [$this, 'onRSTReceived'], 2048);
        $this->eventDispacher->addListener(StateChangedEvent::NAME, [$this, 'onStateChanged'], 2048);
        $this->eventDispacher->addListener(AXIReceivedEvent::NAME, [$this, 'onAXIReceived'], 2048);
        $this->eventDispacher->addListener(PlayerLapEvent::NAME, [$this, 'onLapEvent'], 0);
    }

    public function tick() {
        
    }

    /**
     * Event called on host connected
     * 
     * @param HostConnectedEvent $event
     * 
     * @return
     */
    public function onHostConnected(HostConnectedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->logEvent("EVENT: HostConnected {Host: {$this->host->getName()}, Version: {$event->packet->InSimVer}}", OutputInterface::VERBOSITY_VERBOSE);

        // Clear previous connections and players
        $this->connectionRepo->deleteByHost($event->host->getId());

        // Request all connections
        $tiny_ncn = new IS_TINY();
        $tiny_ncn->ReqI = true;
        $tiny_ncn->SubT = IS_TINY::TINY_NCN;
        $this->host->insim->send($tiny_ncn);

        // Request players
        $tiny_npl = new IS_TINY();
        $tiny_npl->ReqI = true;
        $tiny_npl->SubT = IS_TINY::TINY_NPL;
        $this->host->insim->send($tiny_npl);

        // Request connection info
        if (!$this->host->isLocal()) {
            $tiny_nci = new IS_TINY();
            $tiny_nci->ReqI = true;
            $tiny_nci->SubT = IS_TINY::TINY_NCI;
            $this->host->insim->send($tiny_nci);
        }

        // Request state
        $tiny_sta = new IS_TINY();
        $tiny_sta->ReqI = true;
        $tiny_sta->SubT = IS_TINY::TINY_SST;
        $this->host->insim->send($tiny_sta);

        // Request layout info
        $tiny_axi = new IS_TINY();
        $tiny_axi->ReqI = true;
        $tiny_axi->SubT = IS_TINY::TINY_AXI;
        $this->host->insim->send($tiny_axi);

        // Request race info
        $tiny_rst = new IS_TINY();
        $tiny_rst->ReqI = true;
        $tiny_rst->SubT = IS_TINY::TINY_RST;
        $this->host->insim->send($tiny_rst);

        // Update server info / connection status
        $this->host->setConnectedAt(new DateTime());
        $this->host->setStatus(ConnectionStatus::CONNECTED);

        // Add default host settings
        $defaultSettings = $this->hostSettingRepo->findBy(['host' => null]);

        if (count($defaultSettings) !== count($this->host->getSettings())) {
            foreach ($defaultSettings as $setting) {
                if (!$this->host->hasSetting($setting->getName())) {
                    $newSetting = $this->host->addSetting(clone $setting);
                    $this->entityManager->persist($newSetting);
                    $this->entityManager->flush($newSetting);
                }
            }
        }

        // Apply host cfg settings
        foreach ($this->host->getSettings() as $setting) {
            if ($setting->getGroup() === 'cfg') {
                $this->sendCommand("/{$setting->getName()} {$setting->getValue()}");
            }
        }


        $this->entityManager->persist($this->host);
        $this->entityManager->flush($this->host);
    }

    /**
     * Event called on host disconnected
     * 
     * @param HostDisconnectedEvent $event
     */
    public function onHostDisconnected(HostDisconnectedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        $this->host->setStatus(ConnectionStatus::DISCONNECTED);

        $this->entityManager->persist($this->host);
        $this->entityManager->flush($this->host);

        $this->connectionRepo->deleteByHost($event->host->getId());

        $this->eventDispacher->removeListener(HostConnectedEvent::NAME, [$this, 'onHostConnected']);
        $this->eventDispacher->removeListener(HostDisconnectedEvent::NAME, [$this, 'onHostDisconnected']);
        $this->eventDispacher->removeListener(RSTReceivedEvent::NAME, [$this, 'onRSTReceived']);
        $this->eventDispacher->removeListener(StateChangedEvent::NAME, [$this, 'onStateChanged']);
        $this->eventDispacher->removeListener(AXIReceivedEvent::NAME, [$this, 'onAXIReceived']);
    }

    /**
     * Event called when RST packet was received
     * 
     * @param RSTReceivedEvent $event
     */
    public function onRSTReceived(RSTReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Track info
        if ($this->host->getTrack() === null) {
            $track = $this->trackRepo->findOneBy(['shortName' => $event->packet->Track]);

            if ($track === null) {
                $track = new Track();
                $this->logEvent("ERROR - Unknow track {$event->packet->Track}");
            }

            $track->setShortName($event->packet->Track);
            $track->setWind($event->packet->Weather);
            $track->setWeather($event->packet->Wind);
            $track->setSplitsCount($event->packet->splitsCount);
            $this->host->setTrack($track);
        } else {
            if ($this->host->getTrack()->getShortName() !== $event->packet->Track) {
                // Track changed
                $track = $this->trackRepo->findOneBy(['shortName' => $event->packet->Track]);

                if ($track === null) {
                    $track = new Track();
                    $this->logEvent("ERROR - Unknow track {$event->packet->Track}");
                }

                $track->setShortName($event->packet->Track);
                $track->setWind($event->packet->Weather);
                $track->setWeather($event->packet->Wind);
                $track->setSplitsCount($event->packet->splitsCount);
                $this->host->setTrack($track);
            } else {
                // Update track conditions
                $this->host->getTrack()->setWeather($event->packet->Weather);
                $this->host->setWind($event->packet->Wind);
                $this->host->getTrack()->setSplitsCount($event->packet->splitsCount);
            }
        }

        // Timing info
        $timing = new Timing($event->packet->RaceLaps, $event->packet->QualMins);
        $this->host->setTiming($timing);

        // Results
        if ($event->packet->ReqI == 0) {
            //$this->removeTimer();
            $this->host->setResult(new Result($this->host, $event->packet));
        }

        $this->entityManager->persist($this->host);
        $this->entityManager->flush($this->host);
        $event->host = $this->host;

        //dump($event->packet);
    }

    /**
     * Event called when state has changed
     * @param StateChangedEvent $event
     */
    public function onStateChanged(StateChangedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Track info
        if ($this->host->getTrack() === null) {
            $track = $this->trackRepo->findOneBy(['shortName' => $event->packet->Track]);

            if ($track === null) {
                $track = new Track();
                $this->logEvent("ERROR - Unknow track {$event->packet->Track}");
            }

            $track->setShortName($event->packet->Track);
            $track->setWind($event->packet->Weather);
            $track->setWeather($event->packet->Wind);
            $this->host->setTrack($track);
        } else {
            if ($this->host->getTrack()->getShortName() !== $event->packet->Track) {
                // Track changed
                $track = $this->trackRepo->findOneBy(['shortName' => $event->packet->Track]);

                if ($track === null) {
                    $track = new Track();
                    $this->logEvent("ERROR - Unknow track {$event->packet->Track}");
                }

                $track->setShortName($event->packet->Track);
                $track->setWind($event->packet->Weather);
                $track->setWeather($event->packet->Wind);
                $this->host->setTrack($track);
            } else {
                // Update track conditions
                $this->host->getTrack()->setWeather($event->packet->Weather);
                $this->host->setWind($event->packet->Wind);
            }
        }

        // Timing info
        $timing = new Timing($event->packet->RaceLaps, $event->packet->QualMins);
        $this->host->setTiming($timing);

        $this->entityManager->persist($this->host);
        $this->entityManager->flush($this->host);
        $event->host = $this->host;
    }

    /**
     * Event called when AXI packet was received
     * 
     * @param AXIReceivedEvent $event
     */
    public function onAXIReceived(AXIReceivedEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        if ($this->host->getTrack() !== null) {
            $this->host->setLName($event->packet->LName);
        }

        $this->entityManager->persist($this->host);
        $this->entityManager->flush($this->host);
        $event->host = $this->host;
    }

    /**
     * Event called when player crosses finish line
     * 
     * @param PlayerLapEvent $event
     */
    public function onLapEvent(PlayerLapEvent $event) {
        if ($event->packet->LTime == 3600000) {
            return;
        }

        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        // Save lap event to result
        if ($this->host->getResult() !== null) {
            $this->host->getResult()->addLapEvent($event->player, $event->packet);
            $this->entityManager->persist($this->host->getResult());
            $this->entityManager->flush($this->host->getResult());
        }
    }

    /**
     * Update host setting
     * 
     * @param string $name
     * @param mixed $value
     * @param array|null $data
     */
    public function updateHostSetting($name, $value, $data = null) {
        if (($setting = $this->host->getSetting($name))) {
            $setting->setValue($value);

            if ($data !== null) {
                $setting->setData($data);
            }

            if ($setting->getGroup() === 'cfg') {
                $this->sendCommand("/{$setting->getName()} {$setting->getValue()}");
            }

            $this->entityManager->persist($setting);
            $this->entityManager->flush($setting);
        }
    }

    /**
     * 
     * @param string $command
     */
    public function sendCommand($command) {
        if ($this->host->isLocal()) {
            $packet = new IS_MSL();
            $packet->Msg = $command;
        } else {
            $packet = new IS_MST();
            $packet->Msg = $command;
        }

        $this->eventDispacher->dispatch(SendPacketEvent::NAME, new SendPacketEvent(clone $packet, $this->host->getId()));
    }

}
