<?php

namespace AppBundle\Service;

use AppBundle\Entity\Lap;
use AppBundle\Entity\Player;
use AppBundle\Entity\Track;
use AppBundle\Event\HLVC\HLVCBoundsEvent;
use AppBundle\Event\HLVC\HLVCGroundEvent;
use AppBundle\Event\HLVC\HLVCPitSpeedingEvent;
use AppBundle\Event\HLVC\HLVCSlipstreamEvent;
use AppBundle\Event\HLVC\HLVCWallEvent;
use AppBundle\Event\Host\StateChangedEvent;
use AppBundle\Event\Packet\RSTReceivedEvent;
use AppBundle\Event\Player\PlayerCarContactEvent;
use AppBundle\Event\Player\PlayerCarResetEvent;
use AppBundle\Event\Player\PlayerLapEvent;
use AppBundle\Event\Player\PlayerLapSavedEvent;
use AppBundle\Event\Player\PlayerObjectContactEvent;
use AppBundle\Event\Player\PlayerSplitEvent;
use AppBundle\Repository\LapRepository;
use AppBundle\Service\CoreService;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class StintService extends CoreService {

    /** @var Player */
    public $player;

    /** @var Track */
    public $currentTrack;
    public $currentLap;
    public $numLaps;
    public $splits;
    public $sectors;
    public $prev_splits;
    public $prev_sectors;
    public $laps;
    public $bestSplits;
    public $bestSectors;
    public $bestLap;
    public $tbLap;
    public $split_diff;
    public $sector_diff;
    public $lap_diff;
    public $finish_sector;
    public $isClean = true;
    public $isCleanNextLap = true;
    public $isCleanPreviousLap = true;
    public $nextLapCleanCheck = false;

    /** @var LapRepository|EntityRepository */
    private $lapsRepository;

    public function __construct(EventDispatcherInterface $eventDispacher, EntityManager $entityManger) {
        parent::__construct($eventDispacher, $entityManger);

        $this->lapsRepository = $this->entityManager->getRepository(Lap::class);

        $this->eventDispacher->addListener(PlayerSplitEvent::NAME, [$this, 'onSplitEvent'], 2040);
        $this->eventDispacher->addListener(PlayerLapEvent::NAME, [$this, 'onLapEvent'], 2040);
        $this->eventDispacher->addListener(RSTReceivedEvent::NAME, [$this, 'onRSTEvent'], 2040);
        $this->eventDispacher->addListener(StateChangedEvent::NAME, [$this, 'onStateChanged'], 2040);

        $this->eventDispacher->addListener(PlayerCarContactEvent::NAME, [$this, 'onPlayerCarContact'], 2040);
        $this->eventDispacher->addListener(PlayerObjectContactEvent::NAME, [$this, 'onPlayerObjectContact'], 2040);
        $this->eventDispacher->addListener(PlayerCarResetEvent::NAME, [$this, 'onPlayerCarReset'], 2040);
        $this->eventDispacher->addListener(HLVCSlipstreamEvent::NAME, [$this, 'onHLVCSlipStreamEvent'], 2040);
        $this->eventDispacher->addListener(HLVCBoundsEvent::NAME, [$this, 'onHLVCEvent'], 2040);
        $this->eventDispacher->addListener(HLVCGroundEvent::NAME, [$this, 'onHLVCEvent'], 2040);
        $this->eventDispacher->addListener(HLVCPitSpeedingEvent::NAME, [$this, 'onHLVCEvent'], 2040);
        $this->eventDispacher->addListener(HLVCWallEvent::NAME, [$this, 'onHLVCEvent'], 2040);

        $this->reset();
    }

    public function __destruct() {
        $this->eventDispacher->removeListener(PlayerCarContactEvent::NAME, [$this, 'onPlayerCarContact']);
        $this->eventDispacher->removeListener(PlayerObjectContactEvent::NAME, [$this, 'onPlayerObjectContact']);
        $this->eventDispacher->removeListener(PlayerCarResetEvent::NAME, [$this, 'onPlayerCarReset']);
        $this->eventDispacher->removeListener(PlayerSplitEvent::NAME, [$this, 'onSplitEvent']);
        $this->eventDispacher->removeListener(PlayerLapEvent::NAME, [$this, 'onLapEvent']);
        $this->eventDispacher->removeListener(RSTReceivedEvent::NAME, [$this, 'onRSTEvent']);
        $this->eventDispacher->removeListener(StateChangedEvent::NAME, [$this, 'onStateChanged']);

        $this->eventDispacher->removeListener(RSTReceivedEvent::NAME, [$this, 'onHLVCSlipStreamEvent']);
        $this->eventDispacher->removeListener(HLVCBoundsEvent::NAME, [$this, 'onHLVCEvent']);
        $this->eventDispacher->removeListener(HLVCGroundEvent::NAME, [$this, 'onHLVCEvent']);
        $this->eventDispacher->removeListener(HLVCPitSpeedingEvent::NAME, [$this, 'onHLVCEvent']);
        $this->eventDispacher->removeListener(HLVCWallEvent::NAME, [$this, 'onHLVCEvent']);
    }

    private function reset() {
        $this->numLaps = 0;
        $this->currentLap = 1;
        $this->finish_sector = null;
        $this->splits = [];
        $this->sectors = [];
        $this->laps = [];
        $this->split_diff = null;
        $this->sector_diff = null;
        $this->lap_diff = null;
        $this->bestLap = null;
        $this->tbLap = null;
        $this->bestSectors = [];
        $this->bestSplits = [];
        $this->isClean = true;
        $this->isCleanNextLap = true;
        $this->isCleanPreviousLap = true;
        $this->nextLapCleanCheck = false;
    }

    private function invalidateLap() {
        // Invalidate lap
        $this->isClean = false;

        // Invalidate next lap
        if ($this->nextLapCleanCheck) {
            $this->isCleanNextLap = false;
        }
    }

    public function setPlayer(Player &$player) {
        $this->player = $player;
    }

    /**
     * Event called when player crosses split
     * 
     * @param PlayerSplitEvent $event
     */
    public function onSplitEvent(PlayerSplitEvent $event) {
        if ($event->packet->STime == 3600000) {
            return;
        }

        if ($this->player === null OR $this->currentTrack === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->splits[$event->packet->Split] = $event->packet->STime;

        $this->sectors[$event->packet->Split] = $event->packet->Split == 1 ? $event->packet->STime : ($event->packet->STime - @$this->splits[$event->packet->Split - 1]);

        $this->split_diff = isset($this->bestSplits[$event->packet->Split]) ? ($this->splits[$event->packet->Split] - $this->bestSplits[$event->packet->Split]) : 0;
        $this->sector_diff = isset($this->bestSectors[$event->packet->Split]) ? ($this->sectors[$event->packet->Split] - $this->bestSectors[$event->packet->Split]) : 0;

        if (!isset($this->bestSplits[$event->packet->Split]) OR $event->packet->STime < $this->bestSplits[$event->packet->Split]) {
            $this->bestSplits[$event->packet->Split] = $event->packet->STime;
        }

        if (!isset($this->bestSectors[$event->packet->Split]) OR $this->sectors[$event->packet->Split] < $this->bestSectors[$event->packet->Split]) {
            $this->bestSectors[$event->packet->Split] = $this->sectors[$event->packet->Split];
        }

        // Calculate best possible lap if all sectors are saved
        if (count($this->bestSectors) == ($this->currentTrack->getSplitsCount() + 1)) {
            $this->tbLap = 0;
            foreach ($this->bestSectors as $bs) {
                $this->tbLap += $bs;
            }
        }

        // Check next lap clean validity after last sector
        if ($event->packet->Split === $this->currentTrack->getSplitsCount()) {
            $this->nextLapCleanCheck = true;
        }
    }

    /**
     * Event called when player crosses finish line
     * 
     * @param PlayerLapEvent $event
     */
    public function onLapEvent(PlayerLapEvent $event) {
        if ($event->packet->LTime == 3600000) {
            return;
        }

        if ($this->player === null OR $this->currentTrack === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->numLaps++;
        $this->currentLap++;
        $this->laps[] = $event->packet->LTime;

        if (!isset($this->finish_sector)) {
            $this->finish_sector = count($this->sectors) + 1;
        }

        $this->sectors[$this->finish_sector] = $event->packet->LTime - @$this->splits[$this->finish_sector - 1];
        $this->sector_diff = isset($this->bestSectors[$this->finish_sector]) ? ($this->sectors[$this->finish_sector] - $this->bestSectors[$this->finish_sector]) : 0;
        $this->lap_diff = isset($this->bestLap) ? ($event->packet->LTime - $this->bestLap) : 0;

        if (!isset($this->bestLap) OR $event->packet->LTime < $this->bestLap) {
            $this->bestLap = $event->packet->LTime;
        }

        if (!isset($this->bestSectors[$this->finish_sector]) OR $this->sectors[$this->finish_sector] < $this->bestSectors[$this->finish_sector]) {
            $this->bestSectors[$this->finish_sector] = $this->sectors[$this->finish_sector];
        }

        // Calculate best possible lap if all sectors are saved
        if (count($this->bestSectors) == ($this->currentTrack->getSplitsCount() + 1)) {
            $this->tbLap = 0;
            foreach ($this->bestSectors as $bs) {
                $this->tbLap += $bs;
            }
        }

        $saveLap = true;
        $isUpdated = false;

        // Wind invalidates clean lap
        if ($this->currentTrack->getWind()->getValue() > 0) {
            $this->isClean = false;
        }

        // Check if all splits was set
        if ($this->currentTrack->getSplitsCount() != count($this->splits)) {
            $saveLap = false;
        }

        // Save lap
        if ($saveLap) {
            // Lap conditions
            $flags = $this->player->getPlayerFlags()->__toArray();
            $flags = array_merge($flags, $this->player->getSetupFlags()->__toArray());
            $flags = array_merge($flags, ['tyres' => implode('-', $this->player->getCar()->Tyres)]);

            // Find if lap with same conditions already exists
            $lap = $this->lapsRepository->findOneByFlags($this->player->getId(), $flags, $this->isClean, $this->player->getCar()->getCName(), $this->currentTrack->getShortName(), $this->currentTrack->getLName());


            if ($lap === null) {
                // New lap
                $lap = new Lap();
                $lap->setCar($this->player->getCar()->getCName());
                $lap->setTrack($this->currentTrack->getShortName());
                $lap->setLayout($this->currentTrack->getLName());
                $lap->setPlayer($this->player);
                $lap->setTyres(implode('-', $this->player->getCar()->Tyres));
                $lap->setCreatedAt(new DateTime());
            } else {
                // Don't save - better or equal lap time was already done
                if ($lap->getTime() <= $event->packet->LTime) {
                    $saveLap = false;
                } else {
                    $lap->setUpdatedAt(new DateTime());
                    $isUpdated = true;
                }
            }

            if ($saveLap) {
                $lap->setClean($this->isClean);
                $lap->setTime($event->packet->LTime);

                foreach ($this->splits as $key => $value) {
                    switch ($key) {
                        case 1: $lap->setSp1($value);
                            break;
                        case 2: $lap->setSp2($value);
                            break;
                        case 3: $lap->setSp3($value);
                            break;
                    }
                }

                foreach ($this->sectors as $key => $value) {
                    switch ($key) {
                        case 1: $lap->setSc1($value);
                            break;
                        case 2: $lap->setSc2($value);
                            break;
                        case 3: $lap->setSc3($value);
                            break;
                        case 3: $lap->setSc4($value);
                            break;
                    }
                }

                $this->entityManager->persist($lap);
                $this->entityManager->flush($lap);

                $this->eventDispacher->dispatch(PlayerLapSavedEvent::NAME, new PlayerLapSavedEvent($lap, $isUpdated));
            }
        }

        // Clean up for next lap
        $this->prev_sectors = $this->sectors;
        $this->prev_splits = $this->splits;
        $this->isCleanPreviousLap = $this->isClean;

        $this->isClean = $this->isCleanNextLap;
        $this->isCleanNextLap = true;
        $this->nextLapCleanCheck = false;
        $this->splits = [];
        $this->sectors = [];
    }

    /**
     * Event called when IS_RST packet was received
     * 
     * @param PlayerLapEvent $event
     */
    public function onRSTEvent(RSTReceivedEvent $event) {
        if ($this->player->getHostConnection()->getHost()->getId() !== $event->host->getId()) {
            return;
        }

        $this->currentTrack = $event->host->getTrack();
    }

    /**
     * Event called when state has changed
     * @param StateChangedEvent $event
     */
    public function onStateChanged(StateChangedEvent $event) {
        if ($this->player->getHostConnection()->getHost()->getId() !== $event->host->getId()) {
            return;
        }

        $this->currentTrack = $event->host->getTrack();
    }

    /**
     * Event called when player is drafting other car
     * 
     * @param HLVCSlipstreamEvent $event
     * @return
     */
    public function onHLVCSlipStreamEvent(HLVCSlipstreamEvent $event) {
        if ($this->player === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->invalidateLap();

        $this->logEvent("EVENT: PlayerDraftEvent {Player: {$event->player->getUName()}}", OutputInterface::VERBOSITY_VERBOSE);
    }

    /**
     * Event called when incidents that would violate HLVC happend
     */
    public function onHLVCEvent($event) {
        if ($this->player === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->invalidateLap();
    }

    /**
     * Event called when player hit another car
     * 
     * @param PlayerCarContactEvent $event
     */
    public function onPlayerCarContact(PlayerCarContactEvent $event) {
        if ($this->player === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->invalidateLap();
    }

    /**
     * Event called when player hit object
     * 
     * @param PlayerObjectContactEvent $event
     */
    public function onPlayerObjectContact(PlayerObjectContactEvent $event) {
        if ($this->player === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->invalidateLap();
    }

    /**
     * Event called when player resets car
     * 
     * @param PlayerCarResetEvent $event
     */
    public function onPlayerCarReset(PlayerCarResetEvent $event) {
        if ($this->player === null OR $this->player->getId() !== $event->player->getId()) {
            return;
        }

        $this->invalidateLap();
    }

    public function getLapsRepository(): LapRepository {
        return $this->lapsRepository;
    }

}
