<?php

namespace AppBundle\Service;

use AppBundle\Entity\Host;
use AppBundle\Entity\Player;
use AppBundle\Entity\Timer;
use AppBundle\Entity\UI\UI;
use AppBundle\Entity\UI\UIRaceControl;
use AppBundle\Entity\UI\UIWindow;
use AppBundle\Event\Button\ButtonClearRequestEvent;
use AppBundle\Event\Button\ButtonClickedEvent;
use AppBundle\Event\Player\PlayerConnectedInfoEvent;
use AppBundle\Event\Player\PlayerShowSettingsEvent;
use AppBundle\Event\SendQueuedPacketEvent;
use AppBundle\Event\UI\WindowClosedEvent;
use AppBundle\Packet\IS_MSL;
use AppBundle\Packet\IS_MTC;
use AppBundle\Types\ButtonStyle;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UIService extends CoreService {

    const RESTART_TIMERS = array('restart', 'end', 'qualify', 'trackRotate');

    /** @var Host */
    public $host;

    /** @var Player */
    public $player;

    /** @var UIWindow|null */
    public $activeWIndow;

    /** @var boolean */
    private $requestClear = false;
    private $ui_perm = [];

    public function __construct(EventDispatcherInterface $eventDispacher, EntityManager $entityManger) {
        parent::__construct($eventDispacher, $entityManger);

        $this->eventDispacher->addListener(ButtonClearRequestEvent::NAME, [$this, 'onButtonClearRequest'], 2030);
        $this->eventDispacher->addListener(WindowClosedEvent::NAME, [$this, 'onWindowClosed'], 2030);
        $this->eventDispacher->addListener(PlayerConnectedInfoEvent::NAME, [$this, 'onPlayerConnectedInfo'], -2048);
    }

    public function __destruct() {
        $this->eventDispacher->removeListener(ButtonClearRequestEvent::NAME, [$this, 'onButtonClearRequest']);
        $this->eventDispacher->removeListener(WindowClosedEvent::NAME, [$this, 'onWindowClosed']);
        $this->eventDispacher->removeListener(PlayerConnectedInfoEvent::NAME, [$this, 'onPlayerConnectedInfo']);

        unset($this->activeWIndow);
    }

    public function tick() {
        foreach ($this->ui_perm as $ui) {
            $ui->tick();
        }
    }

    public function showWindow(UIWindow $window) {
        if ($this->activeWIndow !== null) {
            $this->activeWIndow->close();
        }

        $this->activeWIndow = $window;

        // TODO: implement player specific window position override

        $this->activeWIndow->show();
    }

    public function addPermanentUI($name, UI &$newUI, $show = true) {
        $startId = 0;

        foreach ($this->ui_perm as $ui) {
            if ($ui->getCurrentId() >= $startId) {
                $startId = $ui->getCurrentId();
            }
        }

        $startId += 1;

        $newUI->setIdStart($startId);
        if ($show) {
            $newUI->show();
        }

        $this->ui_perm[$name] = $newUI;
    }

    public function getPermanentUI($name) {
        return isset($this->ui_perm[$name]) ? $this->ui_perm[$name] : null;
    }

    public function removePermanentUI($name) {
        if (isset($this->ui_perm[$name])) {
            $this->ui_perm[$name]->close();
            unset($this->ui_perm[$name]);
        }
    }

    /**
     * 
     * @param string $title
     * @param int $secs
     * @param int $delay
     * @param bool $show
     * 
     */
    public function showRaceControl($title = '', $secs, $delay = 0, $show = true) {
        if (!isset($this->ui_perm['rc'])) {
            $ui = new UIRaceControl($this->eventDispacher, $this->player, $this->host);
            $this->addPermanentUI('rc', $ui, false);
        } else {
            $ui = $this->getPermanentUI('rc');
            $ui->redrawTitle = true;
        }

        $ui->setData($secs, [$this, 'closeRaceControl'], $delay);
        $ui->setTitle($title);
        $ui->show();
    }

    public function closeRaceControl() {
        if (isset($this->ui_perm['rc'])) {
            $this->ui_perm['rc']->close();
            unset($this->ui_perm['rc']);
        }
    }

    /**
     * Event called when player presses SHIFT+I / SHIFT+B
     * 
     * @param ButtonClickedEvent $event
     */
    public function onButtonClearRequest(ButtonClearRequestEvent $event) {
        if ($this->player->getId() !== $event->player->getId()) {
            return;
        }

        if (!$this->requestClear) {
            $this->userClearButtons();

            // Show Race control timers if any is running
            if (($timer = $this->host->getTimerService()->getTimerByName(self::RESTART_TIMERS))) {
                if ($this->player->getUi() !== null) {
                    $this->player->getUi()->showRaceControl($this->player->trans("rc.{$timer->name}"), ($timer->timer + Timer::RESTART_DELAY));
                }
            }

            $this->eventDispacher->dispatch(PlayerShowSettingsEvent::NAME, new PlayerShowSettingsEvent($event->host, $event->player));

            $this->requestClear = true;
        } else {
            $this->userClearButtons();
        }
    }

    /**
     * Event called when UIWindow is closed
     * @param WindowClosedEvent $event
     */
    public function onWindowClosed(WindowClosedEvent $event) {
        if ($this->host->getId() !== $event->getHost()->getId()) {
            return;
        }

        if ($this->player->getId() !== $event->getPlayer()->getId()) {
            return;
        }

        unset($this->activeWIndow);
        $this->activeWIndow = null;
    }

    /**
     * Event called when player connected
     * 
     * @param PlayerConnectedInfoEvent $event
     * @return
     */
    public function onPlayerConnectedInfo(PlayerConnectedInfoEvent $event) {
        if ($this->host->getId() !== $event->host->getId()) {
            return;
        }

        if ($this->player->getId() !== $event->player->getId()) {
            return;
        }

        // Show Race control timers if any is running
        if (($timer = $this->host->getTimerService()->getTimerByName(self::RESTART_TIMERS))) {
            if ($this->player->getUi() !== null) {
                $this->player->getUi()->showRaceControl($this->player->trans("rc.{$timer->name}"), ($timer->timer + Timer::RESTART_DELAY));
            }
        }
    }

    /**
     * Send error message to player
     * 
     * @param string $text
     * @param array $replace
     */
    public function sendErrorMsg($text, $replace = []) {
        if ($this->host->isLocal()) {
            $packet = new IS_MSL();
            $packet->Msg = ButtonStyle::COL_RED . $this->player->trans($text, $replace);
        } else {
            $packet = new IS_MTC();
            $packet->UCID = $this->player->getHostConnection()->getUCID();
            $packet->Text = ButtonStyle::COL_RED . $this->player->trans($text, $replace);
        }

        $this->eventDispacher->dispatch(SendQueuedPacketEvent::NAME, new SendQueuedPacketEvent(clone $packet, $this->player->getHostConnection()->getUCID(), $this->host->getId()));
    }

    private function userClearButtons() {
        if ($this->activeWIndow !== null) {
            $this->activeWIndow->close();
            $this->activeWIndow = null;
        }

        foreach ($this->ui_perm as $key => $ui) {
            $ui->close();
            unset($this->ui_perm[$key]);
        }

        $this->requestClear = false;
    }

}
