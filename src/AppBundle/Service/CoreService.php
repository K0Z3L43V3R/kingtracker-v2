<?php

namespace AppBundle\Service;

use AppBundle\Event\LogEvent;
use AppBundle\Types\LogType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CoreService {

    /** @var EventDispatcherInterface */
    protected $eventDispacher;

    /** @var EntityManager|null */
    protected $entityManager;

    public function __construct(EventDispatcherInterface $eventDispacher, EntityManager $entityManger = null) {
        $this->entityManager = $entityManger;

        $this->eventDispacher = $eventDispacher;
    }
    
    /**
     * Dispatch log event
     * 
     * @param string $message
     * @param int $verbosity
     */
    public function logEvent($message, $verbosity = OutputInterface::VERBOSITY_NORMAL) {
        $event = new LogEvent($message, LogType::EVENT, $verbosity);
        $this->eventDispacher->dispatch(LogEvent::NAME, $event);
    }

}
