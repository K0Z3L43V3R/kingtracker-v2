<?php

namespace AppBundle\Types;

class MessageSound {

    const SND_SILENT = 0;
    const SND_MESSAGE = 1;
    const SND_SYSMESSAGE = 2;
    const SND_INVALIDKEY = 3;
    const SND_ERROR = 4;
    const SND_NUM = 5;

}
