<?php

namespace AppBundle\Types;

class LFSLanguage {

    const LFS_ENGLISH = 0;
    const LFS_DEUTSCH = 1;
    const LFS_PORTUGUESE = 2;
    const LFS_FRENCH = 3;
    const LFS_SUOMI = 4;
    const LFS_NORSK = 5;
    const LFS_NEDERLANDS = 6;
    const LFS_CATALAN = 7;
    const LFS_TURKISH = 8;
    const LFS_CASTELLANO = 9;
    const LFS_ITALIANO = 10;
    const LFS_DANSK = 11;
    const LFS_CZECH = 12;
    const LFS_RUSSIAN = 13;
    const LFS_ESTONIAN = 14;
    const LFS_SERBIAN = 15;
    const LFS_GREEK = 16;
    const LFS_POLSKI = 17;
    const LFS_CROATIAN = 18;
    const LFS_HUNGARIAN = 19;
    const LFS_BRAZILIAN = 20;
    const LFS_SWEDISH = 21;
    const LFS_SLOVAK = 22;
    const LFS_GALEGO = 23;
    const LFS_SLOVENSKI = 24;
    const LFS_BELARUSSIAN = 25;
    const LFS_LATVIAN = 26;
    const LFS_LITHUANIAN = 27;
    const LFS_TRADITIONAL_CHINESE = 28;
    const LFS_SIMPLIFIED_CHINESE = 29;
    const LFS_JAPANESE = 30;
    const LFS_KOREAN = 31;
    const LFS_BULGARIAN = 32;
    const LFS_LATINO = 33;
    const LFS_UKRAINIAN = 34;
    const LFS_INDONESIAN = 35;
    const LFS_ROMANIAN = 36;
    const LFS_NUM_LANG = 37;

    protected static $langs = array(
        self::LFS_ENGLISH => 'EN',
        self::LFS_DEUTSCH => 'DE',
        self::LFS_PORTUGUESE => 'PT',
        self::LFS_FRENCH => 'FR',
        self::LFS_SUOMI => 'FI',
        self::LFS_NORSK => 'NO',
        self::LFS_NEDERLANDS => 'NL',
        self::LFS_CATALAN => 'CA',
        self::LFS_TURKISH => 'TR',
        self::LFS_CASTELLANO => 'ES',
        self::LFS_ITALIANO => 'IT',
        self::LFS_DANSK => 'DA',
        self::LFS_CZECH => 'CS',
        self::LFS_RUSSIAN => 'RU',
        self::LFS_ESTONIAN => 'ET',
        self::LFS_SERBIAN => 'SR',
        self::LFS_GREEK => 'EL',
        self::LFS_POLSKI => 'PL',
        self::LFS_CROATIAN => 'HR',
        self::LFS_HUNGARIAN => 'HU',
        self::LFS_BRAZILIAN => 'BR',
        self::LFS_SWEDISH => 'SV',
        self::LFS_SLOVAK => 'SK',
        self::LFS_GALEGO => 'GL',
        self::LFS_SLOVENSKI => 'SL',
        self::LFS_BELARUSSIAN => 'BE',
        self::LFS_LATVIAN => 'LV',
        self::LFS_LITHUANIAN => 'LT',
        self::LFS_TRADITIONAL_CHINESE => 'ZH',
        self::LFS_SIMPLIFIED_CHINESE => 'ZH',
        self::LFS_JAPANESE => 'JA',
        self::LFS_KOREAN => 'KO',
        self::LFS_BULGARIAN => 'BG',
        self::LFS_LATINO => 'LA',
        self::LFS_UKRAINIAN => 'UK',
        self::LFS_INDONESIAN => 'ID',
        self::LFS_ROMANIAN => 'RO',
    );
    protected static $locales = array(
        self::LFS_ENGLISH => 'en_US',
        self::LFS_DEUTSCH => 'de_DE',
        self::LFS_PORTUGUESE => 'pt_PT',
        self::LFS_FRENCH => 'fr_FR',
        self::LFS_SUOMI => 'fi_FI',
        self::LFS_NORSK => 'nb_NO',
        self::LFS_NEDERLANDS => 'nl_NL',
        self::LFS_CATALAN => 'ca_ES',
        self::LFS_TURKISH => 'tr_TR',
        self::LFS_CASTELLANO => 'en_US',
        self::LFS_ITALIANO => 'it_IT',
        self::LFS_DANSK => 'da_DK',
        self::LFS_CZECH => 'cs_CZ',
        self::LFS_RUSSIAN => 'ru_RU',
        self::LFS_ESTONIAN => 'et_EE',
        self::LFS_SERBIAN => 'sr_RS',
        self::LFS_GREEK => 'el_GR',
        self::LFS_POLSKI => 'pl_PL',
        self::LFS_CROATIAN => 'hr_HR',
        self::LFS_HUNGARIAN => 'hu_HU',
        self::LFS_BRAZILIAN => 'pt_BR',
        self::LFS_SWEDISH => 'sv_SE',
        self::LFS_SLOVAK => 'sk_SK',
        self::LFS_GALEGO => 'en_US',
        self::LFS_SLOVENSKI => 'sl_SI',
        self::LFS_BELARUSSIAN => 'be_BY',
        self::LFS_LATVIAN => 'lv_LV',
        self::LFS_LITHUANIAN => 'lt_LT',
        self::LFS_TRADITIONAL_CHINESE => 'zh_TW',
        self::LFS_SIMPLIFIED_CHINESE => 'zh_CN',
        self::LFS_JAPANESE => 'ja_JP',
        self::LFS_KOREAN => 'ko_KR',
        self::LFS_BULGARIAN => 'bg_BG',
        self::LFS_LATINO => 'en_US',
        self::LFS_UKRAINIAN => 'ru_UA',
        self::LFS_INDONESIAN => 'id_ID',
        self::LFS_ROMANIAN => 'ro_RO',
    );

    public static function getLang($lang_code) {
        return isset(self::$langs[$lang_code]) ? self::$langs[$lang_code] : 'EN';
    }

    public static function getLocale($lang_code) {
        return isset(self::$locales[$lang_code]) ? self::$locales[$lang_code] : 'en_US';
    }

}
