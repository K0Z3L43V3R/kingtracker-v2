<?php

namespace AppBundle\Types;

class Weather {

    private $Weather = 0;
    private $Track;

    /**
     * @param int $weather
     * @param string $track
     */
    public function __construct($weather, $track = null) {
        $this->Weather = $weather;
        $this->Track = $track;
    }

    public function __toString() {
        $track = substr($this->Track, 0, 2);

        switch ($track) {
            case 'BL':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'SO':
                switch ($this->Weather) {
                    case 0: return 'Clear afternoon';
                    case 1: return 'Overcast day';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'FE':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy sunset';
                    case 2: return 'Overcast dusk';
                }
                break;
            case 'AU':
                switch ($this->Weather) {
                    case 0: return 'Overcast afternoon';
                    case 1: return 'Clear morning';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'KY':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                    case 2: return 'Cloudy morning';
                }
                break;
            case 'WE':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy sunset';
                }
                break;
            case 'AS':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                }
                break;
            case 'RO':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Clear morning';
                    case 2: return 'Overcast afternoon';
                    case 3: return 'Clear sunset';
                }
                break;
        }

        return $this->Weather;
    }

}
