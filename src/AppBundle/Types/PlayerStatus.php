<?php

namespace AppBundle\Types;

class PlayerStatus {

    const SPECTATING = 0;
    const ONTRACK = 1;
}
