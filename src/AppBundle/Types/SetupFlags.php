<?php

namespace AppBundle\Types;

class SetupFlags {

    const SETF_SYMM_WHEELS = 1;
    const TC_ENABLE = 2;
    const ABS_ENABLE = 4;

    private $value = 0;

    /**
     * @param int $value
     */
    public function __construct($value) {
        $this->value = $value;
    }

    /**
     * @param int $flag
     * @return boolean
     */
    public function hasFlag($flag) {
        return $this->value & $flag;
    }

    public function __toString() {
        $out = '';

        if ($this->value & self::TC_ENABLE)
            $out = 'TC';

        if ($this->value & self::ABS_ENABLE)
            $out .= (!empty($out) ? ', ' : '') . 'ABS';

        return $out;
    }
    
    public function __toArray() {
        $out = [];

        if ($this->value & self::TC_ENABLE)
            $out['SF_TC'] = 1;

        if ($this->value & self::ABS_ENABLE)
            $out['SF_ABS'] = 1;

        return $out;
    }

}
