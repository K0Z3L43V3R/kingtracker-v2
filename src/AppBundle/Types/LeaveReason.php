<?php

namespace AppBundle\Types;

class LeaveReason {

    /** @var int none */
    const LEAVR_DISCO = 0;

    /** @var int timed out */
    const LEAVR_TIMEOUT = 1;

    /** @var int lost connection */
    const LEAVR_LOSTCONN = 2;

    /** @var int kicked */
    const LEAVR_KICKED = 3;

    /** @var int banned */
    const LEAVR_BANNED = 4;

    /** @var int security */
    const LEAVR_SECURITY = 5;

    /** @var int cheat protection wrong */
    const LEAVR_CPW = 6;

    /** @var int out of sync with host */
    const LEAVR_OOS = 7;

    /** @var int join OOS (initial sync failed) */
    const LEAVR_JOOS = 8;

    /** @var int invalid packet */
    const LEAVR_HACK = 9;
    
    public static function toString($reson) {
        switch ($reson) {
            case self::LEAVR_DISCO:
                return 'Disconnected';
            case self::LEAVR_TIMEOUT:
                return 'Timed out';
            case self::LEAVR_LOSTCONN:
                return 'Lost connection';
            case self::LEAVR_KICKED:
                return 'Kicked';
            case self::LEAVR_BANNED:
                return 'Banned';
            case self::LEAVR_SECURITY:
                return 'Security';
            case self::LEAVR_CPW:
                return 'Cheat protection';
            case self::LEAVR_OOS:
                return 'Out of sync with host';
            case self::LEAVR_JOOS:
                return 'Join OOS (initial sync failed)';
            case self::LEAVR_HACK:
                return 'Hack (invalid packet)';
        }
        
        return 'Unknown';
    }

}
