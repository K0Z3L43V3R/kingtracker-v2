<?php

namespace AppBundle\Types;

/**
 * Penalty values (VALID means the penalty can now be cleared)
 */
class PenaltyType {

    const PENALTY_NONE = 0;
    const PENALTY_DT = 1;
    const PENALTY_DT_VALID = 2;
    const PENALTY_SG = 3;
    const PENALTY_SG_VALID = 4;
    const PENALTY_30 = 5;
    const PENALTY_45 = 6;

}
