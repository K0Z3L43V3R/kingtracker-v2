<?php

namespace AppBundle\Types;

class ButtonClick {

    /** left click */
    const ISB_LMB = 1;

    /** right click */
    const ISB_RMB = 2;

    /** ctrl + click */
    const ISB_CTRL = 4;

    /** shift + click */
    const ISB_SHIFT = 8;

}
