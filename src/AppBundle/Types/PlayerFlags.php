<?php

namespace AppBundle\Types;

class PlayerFlags {

    const SWAPSIDE = 1;
    const RESERVED_2 = 2;
    const RESERVED_4 = 4;
    const AUTOGEARS = 8;
    const SHIFTER = 16;
    const RESERVED_32 = 32;
    const HELP_B = 64;
    const AXIS_CLUTCH = 128;
    const INPITS = 256;
    const AUTOCLUTCH = 512;
    const MOUSE = 1024;
    const KB_NO_HELP = 2048;
    const KB_STABILISED = 4096;
    const CUSTOM_VIEW = 8192;

    private $value = 0;

    /**
     * @param int $value
     */
    public function __construct($value) {
        $this->value = $value;
    }

    /**
     * @param int $flag
     * @return boolean
     */
    public function hasFlag($flag) {
        return $this->value & $flag;
    }

    public function __toString() {
        if ($this->value & self::MOUSE)
            $out = 'M';
        elseif ($this->value & self::KB_NO_HELP)
            $out = 'KB';
        elseif ($this->value & self::KB_STABILISED)
            $out = 'KBS';
        else
            $out = 'W';

        if ($this->value & self::AUTOCLUTCH)
            $out .= ', AC';
        elseif ($this->value & self::AXIS_CLUTCH)
            $out .= ', AxisC';
        else
            $out .= ', BC';

        if ($this->value & self::AUTOGEARS)
            $out .= ', AutoGears';
        elseif ($this->value & self::SHIFTER)
            $out .= ', H-Shifter';

        if ($this->value & self::HELP_B)
            $out .= ', Brake Help';

        return $out;
    }

    public function __toArray() {
        $out = [];

        if ($this->value & self::MOUSE)
            $out['PF_M'] = 1;
        elseif ($this->value & self::KB_NO_HELP)
            $out['PF_KB'] = 1;
        elseif ($this->value & self::KB_STABILISED)
            $out['PF_KBS'] = 1;
        else
            $out['PF_W'] = 1;

        if ($this->value & self::AUTOCLUTCH)
            $out['PF_AC'] = 1;
        elseif ($this->value & self::AXIS_CLUTCH)
            $out['PF_AxisC'] = 1;
        else
            $out['PF_BC'] = 1;

        if ($this->value & self::AUTOGEARS)
            $out['PF_AG'] = 1;
        elseif ($this->value & self::SHIFTER)
            $out['PF_HS'] = 1;
        else
            $out['PF_SQ'] = 1;

        if ($this->value & self::HELP_B)
            $out['PF_BRH'] = 1;

        return $out;
    }

}
