<?php

namespace AppBundle\Types;

class Wind {

    private $value = 0;

    public function __construct($value) {
        $this->value = $value;
    }

    public function __toString() {
        switch ($this->value) {
            case 0:
                return 'none';
            case 1:
                return 'weak';
            case 2:
                return 'strong';
        }

        return $this->value;
    }
    
    public function getValue(){
        return $this->value;
    }

}
