<?php

namespace AppBundle\Types;

class InsimFlags {

    const ISF_LOCAL = 4;
    const ISF_MCI = 32;
    const ISF_CON = 64;
    const ISF_OBH = 128;
    const ISF_HLV = 256;

}
