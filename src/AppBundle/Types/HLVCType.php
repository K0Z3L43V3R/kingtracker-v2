<?php

namespace AppBundle\Types;

class HLVCType {

    const HLVC_GROUND = 0;
    const HLVC_WALL = 1;
    const HLVC_SPEEDING = 4;
    const HLVC_BOUNDS = 5;

    private $value = 0;

    /**
     * @param int $value
     */
    public function __construct($value) {
        $this->value = $value;
    }

    public function __toString() {
        switch ($this->value) {
            case self::HLVC_GROUND:
                return 'ground';
            case self::HLVC_WALL:
                return 'wall';
            case self::HLVC_SPEEDING:
                return 'speeding';
            case self::HLVC_BOUNDS:
                return 'bounds';
        }
    }

}
