<?php

namespace AppBundle\Types;

class ConnectionStatus {

    const DISCONNECTED = -1;
    const NOTCONNECTED = 0;
    const CONNECTING = 1;
    const CONNECTED = 2;
    const TIMEOUT = 3;

}
