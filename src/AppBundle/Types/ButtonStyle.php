<?php

namespace AppBundle\Types;

class ButtonStyle {

    /**
     * click this button to send IS_BTC
     */
    const ISB_CLICK = 8;

    /**
     * light button
     */
    const ISB_LIGHT = 16;

    /**
     * dark button
     */
    const ISB_DARK = 32;

    /**
     * align text to left
     */
    const ISB_LEFT = 64;

    /**
     * align text to right
     */
    const ISB_RIGHT = 128;
    const COLOUR_LIGHT_GREY = 0;
    const COLOUR_YELLOW = 1;
    const COLOUR_BLACK = 2;
    const COLOUR_WHITE = 3;
    const COLOUR_GREEN = 4;
    const COLOUR_RED = 5;
    const COLOUR_BLUE = 6;
    const COLOUR_GREY = 7;
    
    const COL_ENABLED = '^6';
    const COL_DISABLED = '^1';
    const COL_NEUTRAL = '^8';
    const COL_RED = '^1';
    const COL_GREEN = '^2';
    const COL_YELLOW = '^3';
    const COL_LIGHT_BLUE = '^6';
    const COL_WHITE = '^7';

}
