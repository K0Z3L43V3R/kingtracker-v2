<?php

namespace AppBundle\Types;

/**
 * Description of NotifyTypes
 *
 */
class NotifyTypes {

    const NO = 0;
    const YES = 1;
    const OWN = 2;

}
