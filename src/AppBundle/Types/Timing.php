<?php

namespace AppBundle\Types;

class Timing {

    const RST_TIMING_STANDARD = 64;
    const RST_TIMING_CUSTOM = 128;
    const RST_TIMING_NONE = 192;

    private $RaceLaps;
    private $QualMins;

    /**
     * @param int $RaceLaps
     * @param int $QualMins
     */
    public function __construct($RaceLaps, $QualMins) {
        $this->RaceLaps = $RaceLaps;
        $this->QualMins = $QualMins;
    }

    public function __toString() {
        if ($this->RaceLaps == 0) {
            return $this->QualMins == 0 ? 'practice' : 'qualify';
        } elseif ($this->RaceLaps >= 1 && $this->RaceLaps <= 99) {
            return $this->RaceLaps . ' laps';
        } elseif ($this->RaceLaps >= 100 && $this->RaceLaps <= 190) {
            return (($this->RaceLaps - 100) * 10 + 100) . ' laps';
        } elseif ($this->RaceLaps >= 191 && $this->RaceLaps <= 238) {
            return ($this->RaceLaps - 190) . ' hours';
        }
    }

    public function getRaceLaps() {
        return $this->RaceLaps;
    }

    public function getQualMins() {
        return $this->QualMins;
    }

}
