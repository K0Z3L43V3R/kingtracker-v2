<?php

namespace AppBundle\Types;

class ButtonFunction {

    /** Delete one button (must set ClickID) */
    const BFN_DEL_BTN = 0;

    /** Clear all buttons made by this insim instance */
    const BFN_CLEAR = 1;

    /** User cleared this insim instance's buttons */
    const BFN_USER_CLEAR = 2;

    /** SHIFT+B or SHIFT+I - request for buttons */
    const BFN_REQUEST = 3;

}
