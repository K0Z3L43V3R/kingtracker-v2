<?php

namespace AppBundle\Types;

class LogType {

    const GENERAL = 'general';
    const EVENT = 'event';
    const PACKET = 'packet';

}
