<?php

namespace AppBundle\Component;

use Symfony\Component\Translation\Translator;

/**
 * Custom translator
 */
class LFSTranslator extends Translator {

    private $codepages = array(
        'L' => 'CP1252', // Latin 1
        'G' => 'CP1253', // Greek
        'C' => 'CP1251', // Cyrillic
        'E' => 'CP1250', // Central Europe
        'T' => 'CP1254', // Turkish
        'B' => 'CP1257', // Baltic
        'J' => 'CP932', // Japanese
        'S' => 'CP936', // Simplified Chinese
        'K' => 'CP949', // Korean
        'H' => 'CP950', // Traditional Chinese
    );
    private $locales = [
        'be_BY' => 'C',
        'pt_BR' => 'L',
        'bg_BG' => 'C',
        'ca_ES' => 'L',
        'hr_HR' => 'E',
        'cs_CZ' => 'E',
        'da_DK' => 'L',
        'nl_NL' => 'L',
        'de_DE' => 'L',
        'en_US' => 'L',
        'et_EE' => 'L',
        'fr_FR' => 'L',
        'el_GR' => 'G',
        'hu_HU' => 'E',
        'id_ID' => 'L',
        'it_IT' => 'L',
        'ja_JP' => 'J',
        'ko_KR' => 'K',
        'lv_LV' => 'B',
        'lt_LT' => 'B',
        'nb_NO' => 'L',
        'pl_PL' => 'E',
        'pt_PT' => 'L',
        'ro_RO' => 'E',
        'ru_UA' => 'C',
        'ru_RU' => 'C',
        'sr_RS' => 'E',
        'zh_CN' => 'S',
        'zh_TW' => 'H',
        'sk_SK' => 'E',
        'sl_SI' => 'E',
        'fi_FI' => 'L',
        'sv_SE' => 'L',
        'tr_TR' => 'T',
    ];
    private $languages = [
        'be_BY' => 'Belarussian',
        'pt_BR' => 'Brazilian',
        'bg_BG' => 'Bulgarian',
        'ca_ES' => 'Catalan',
        'hr_HR' => 'Croatian',
        'cs_CZ' => 'Czech',
        'da_DK' => 'Danish',
        'nl_NL' => 'Dutch',
        'de_DE' => 'German',
        'en_US' => 'English',
        'et_EE' => 'Estonian',
        'fr_FR' => 'French',
        'el_GR' => 'Greek',
        'hu_HU' => 'Hungarian',
        'id_ID' => 'Indonesian',
        'it_IT' => 'Italian',
        'ja_JP' => 'Japanese',
        'ko_KR' => 'Korean',
        'lv_LV' => 'Latvian',
        'lt_LT' => 'Lithuanian',
        'nb_NO' => 'Norwegian',
        'pl_PL' => 'Polish',
        'pt_PT' => 'Portuguese',
        'ro_RO' => 'Romanian',
        'ru_UA' => 'Ukrainian',
        'ru_RU' => 'Russian',
        'sr_RS' => 'Serbian',
        'zh_CN' => 'Chinese S.',
        'zh_TW' => 'Chinese T.',
        'sk_SK' => 'Slovak',
        'sl_SI' => 'Slovenian',
        'fi_FI' => 'Finnish',
        'sv_SE' => 'Swedish',
        'tr_TR' => 'Turkish',
    ];
    
    public function trans($id, array $parameters = array(), $domain = null, $locale = null) {
        $translated = parent::trans($id, $parameters, $domain, $locale);
        
        $locChar = 'L';
        if (isset($this->locales[$this->getLocale()])) {
            $locChar = $this->locales[$this->getLocale()];

            if (isset($this->codepages[$locChar])) {
                $codepage = $this->codepages[$locChar];
                $translated = @iconv("UTF-8", "{$codepage}//TRANSLIT", $translated);
            }
        }
        
        return "^{$locChar}{$translated}";
    }

    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null) {
        $translated = parent::transChoice($id, $number, $parameters, $domain, $locale);
        
        $locChar = 'L';
        if (isset($this->locales[$this->getLocale()])) {
            $locChar = $this->locales[$this->getLocale()];

            if (isset($this->codepages[$locChar])) {
                $codepage = $this->codepages[$locChar];
                $translated = @iconv("UTF-8", "{$codepage}//TRANSLIT", $translated);
            }
        }
        
        return "^{$locChar}{$translated}";
    }
}
