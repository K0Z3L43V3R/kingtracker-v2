<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170409174959 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE track (id INT AUTO_INCREMENT NOT NULL, short_name VARCHAR(6) NOT NULL, track VARCHAR(64) NOT NULL, circuit VARCHAR(64) NOT NULL, licence VARCHAR(16) NOT NULL, grid INT DEFAULT 40, lenght NUMERIC(10, 3) DEFAULT NULL, elevation VARCHAR(64) DEFAULT NULL, reverse TINYINT(1) DEFAULT \'0\', asphalt TINYINT(1) DEFAULT \'1\', oval TINYINT(1) DEFAULT \'0\', rallycross TINYINT(1) DEFAULT \'0\', carpark TINYINT(1) DEFAULT \'0\', drag TINYINT(1) DEFAULT \'0\', UNIQUE INDEX UNIQ_D6E3F8A63EE4B093 (short_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE track');
    }

    public function postUp(Schema $schema) {
        $this->connection->executeQuery("INSERT INTO `track` (`id`, `short_name`, `track`, `circuit`, `licence`, `grid`, `lenght`, `elevation`, `reverse`, `asphalt`, `oval`, `rallycross`, `carpark`, `drag`) VALUES
(NULL, 'BL1', 'Blackwood', 'GP Track', 'DEMO', 40, '3.307', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'BL2', 'Blackwood', 'Historic', 'DEMO', 40, '3.307', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'BL3', 'Blackwood', 'Rallycross', 'DEMO', 30, '1.839', NULL, 1, 0, 0, 1, 0, 0),
(NULL, 'BL4', 'Blackwood', 'Car Park', 'DEMO', 16, '0.000', NULL, 0, 1, 0, 0, 1, 0),
(NULL, 'SO1', 'South City', 'Classic', 'S1', 30, '2.033', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'SO2', 'South City', 'Sprint Track 1', 'S1', 16, '2.048', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'SO3', 'South City', 'Sprint Track 2', 'S1', 16, '1.334', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'SO4', 'South City', 'City Long', 'S1', 32, '4.029', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'SO5', 'South City', 'Town Course', 'S1', 32, '3.146', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'SO6', 'South City', 'Chicane Route', 'S1', 32, '2.917', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'FE1', 'Fern Bay', 'Club', 'S1', 32, '1.584', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'FE2', 'Fern Bay', 'Green Track', 'S1', 32, '3.086', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'FE3', 'Fern Bay', 'Gold Track', 'S1', 32, '3.514', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'FE4', 'Fern Bay', 'Black Track', 'S1', 32, '6.559', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'FE5', 'Fern Bay', 'Rallycross', 'S1', 32, '2.018', NULL, 1, 0, 0, 1, 0, 0),
(NULL, 'FE6', 'Fern Bay', 'Rallycross Green', 'S1', 24, '0.745', NULL, 1, 0, 0, 1, 0, 0),
(NULL, 'AU1', 'Autocross', 'Autocross', 'S1', 16, '0.000', NULL, 1, 1, 0, 0, 1, 0),
(NULL, 'AU2', 'Autocross', 'Skid Pad', 'S1', 16, '0.000', NULL, 1, 1, 0, 0, 1, 0),
(NULL, 'AU3', 'Autocross', 'Drag Strip', 'S1', 2, '0.402', NULL, 1, 1, 0, 0, 0, 1),
(NULL, 'AU4', 'Autocross', 'Eight Lane Drag', 'S1', 8, '0.402', NULL, 1, 1, 0, 0, 0, 1),
(NULL, 'KY1', 'Kyoto Ring', 'Oval', 'S2', 32, '2.980', NULL, 1, 1, 1, 0, 0, 0),
(NULL, 'KY2', 'Kyoto Ring', 'National', 'S2', 32, '5.138', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'KY3', 'Kyoto Ring', 'GP Long', 'S2', 32, '7.377', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'WE1', 'Westhill', 'National', 'S2', 40, '4.396', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'WE2', 'Westhill', 'International', 'S2', 40, '5.750', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'WE3', 'Westhill', 'Car Park', 'S2', 40, '0.000', NULL, 1, 1, 0, 0, 1, 0),
(NULL, 'WE4', 'Westhill', 'Karting', 'S2', 40, '0.483', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'WE5', 'Westhill', 'Karting National', 'S2', 40, '1.316', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS1', 'Aston', 'Cadet', 'S2', 32, '1.870', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS2', 'Aston', 'Club', 'S2', 32, '3.077', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS3', 'Aston', 'National', 'S2', 32, '5.602', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS4', 'Aston', 'Historic', 'S2', 32, '8.089', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS5', 'Aston', 'Grand Prix', 'S2', 32, '8.802', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS6', 'Aston', 'Grand Touring', 'S2', 32, '8.002', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'AS7', 'Aston', 'North', 'S2', 32, '5.168', NULL, 1, 1, 0, 0, 0, 0),
(NULL, 'RO1', 'Rockingham', 'ISSC', 'S3', 40, '3.097', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO2', 'Rockingham', 'National', 'S3', 40, '2.697', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO3', 'Rockingham', 'Oval', 'S3', 40, '2.363', NULL, 0, 1, 1, 0, 0, 0),
(NULL, 'RO4', 'Rockingham', 'ISSC Long', 'S3', 40, '3.253', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO5', 'Rockingham', 'Lake', 'S3', 40, '1.046', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO6', 'Rockingham', 'Handling', 'S3', 40, '1.559', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO7', 'Rockingham', 'International', 'S3', 40, '3.874', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO8', 'Rockingham', 'Historic', 'S3', 40, '3.565', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO9', 'Rockingham', 'Historic Short', 'S3', 40, '2.197', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO10', 'Rockingham', 'International Long', 'S3', 40, '4.056', NULL, 0, 1, 0, 0, 0, 0),
(NULL, 'RO11', 'Rockingham', 'Sportscar', 'S3', 40, '2.693', NULL, 0, 1, 0, 0, 0, 0);");

        $this->connection->executeQuery("INSERT INTO `plugin` (`id`, `name`, `class`, `dir`, `description`, `author`, `version`, `active`) VALUES
(1,	'Welcome message',	'MsgWelcomePlugin',	'msg_welcome',	'Sents welcome message after player connects host',	'K0Z3L_43V3R',	'1.0.1',	1),
(2,	'Core version',	'CoreVersionPlugin',	'core_version',	'Displays current version and changelog',	'K0Z3L_43V3R',	'1.0.1',	1),
(3,	'Core help',	'CoreHelpPlugin',	'core_help',	'Displays all commands',	'K0Z3L_43V3R',	'1.0.1',	1);");
    }

}
