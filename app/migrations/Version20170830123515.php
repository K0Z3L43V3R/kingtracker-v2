<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170830123515 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE result (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, type VARCHAR(16) NOT NULL, info LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', finished INT DEFAULT 0, created_at DATETIME DEFAULT NULL, INDEX IDX_136AC1131FB8D185 (host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE result_event (id INT AUTO_INCREMENT NOT NULL, result_id INT DEFAULT NULL, player_id INT DEFAULT NULL, type VARCHAR(16) NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME DEFAULT NULL, INDEX IDX_AA5A5D2B7A7B643 (result_id), INDEX IDX_AA5A5D2B99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE result_player (id INT AUTO_INCREMENT NOT NULL, result_id INT DEFAULT NULL, player_id INT DEFAULT NULL, position INT DEFAULT 0, car VARCHAR(16) NOT NULL, TTime INT DEFAULT 0, BTime INT DEFAULT 0, NumStops INT DEFAULT 0, LapsDone INT DEFAULT 0, PSeconds INT DEFAULT 0, Flags INT DEFAULT 0, confirmation INT DEFAULT 0, created_at DATETIME DEFAULT NULL, INDEX IDX_7C8641397A7B643 (result_id), INDEX IDX_7C86413999E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1131FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE result_event ADD CONSTRAINT FK_AA5A5D2B7A7B643 FOREIGN KEY (result_id) REFERENCES result (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE result_event ADD CONSTRAINT FK_AA5A5D2B99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE result_player ADD CONSTRAINT FK_7C8641397A7B643 FOREIGN KEY (result_id) REFERENCES result (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE result_player ADD CONSTRAINT FK_7C86413999E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lap CHANGE sp_4 sc_4 INT DEFAULT 0');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result_event DROP FOREIGN KEY FK_AA5A5D2B7A7B643');
        $this->addSql('ALTER TABLE result_player DROP FOREIGN KEY FK_7C8641397A7B643');
        $this->addSql('DROP TABLE result');
        $this->addSql('DROP TABLE result_event');
        $this->addSql('DROP TABLE result_player');
        $this->addSql('ALTER TABLE lap CHANGE sc_4 sp_4 INT DEFAULT 0');
    }

}
