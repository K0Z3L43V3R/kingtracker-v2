<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170417141114 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lap (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, track VARCHAR(8) NOT NULL, layout VARCHAR(128) DEFAULT NULL, car VARCHAR(16) NOT NULL, clean TINYINT(1) DEFAULT \'0\', time INT NOT NULL, sp_1 INT DEFAULT 0, sp_2 INT DEFAULT 0, sp_3 INT DEFAULT 0, sp_4 INT DEFAULT 0, sc_1 INT DEFAULT 0, sc_2 INT DEFAULT 0, sc_3 INT DEFAULT 0, PF_M TINYINT(1) DEFAULT \'0\', PF_W TINYINT(1) DEFAULT \'0\', PF_KB TINYINT(1) DEFAULT \'0\', PF_KBS TINYINT(1) DEFAULT \'0\', PF_AC TINYINT(1) DEFAULT \'0\', PF_AxisC TINYINT(1) DEFAULT \'0\', PF_BC TINYINT(1) DEFAULT \'0\', PF_AG TINYINT(1) DEFAULT \'0\', PF_HS TINYINT(1) DEFAULT \'0\', PF_SQ TINYINT(1) DEFAULT \'0\', PF_BRH TINYINT(1) DEFAULT \'0\', SF_ABS TINYINT(1) DEFAULT \'0\', SF_TC TINYINT(1) DEFAULT \'0\', tyres VARCHAR(8) DEFAULT NULL, created_ad DATETIME DEFAULT NULL, updated_ad DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_926FC08C99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lap ADD CONSTRAINT FK_926FC08C99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lap');
    }

}
