<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170327202057 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE host (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, ip VARCHAR(255) DEFAULT \'localhost\', port INT DEFAULT 29999, active TINYINT(1) DEFAULT \'1\', local TINYINT(1) DEFAULT \'0\', status INT DEFAULT 0, connected_at DATETIME DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, Track VARCHAR(255) DEFAULT \'\', LName VARCHAR(255) DEFAULT \'\', Wind INT DEFAULT 0, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE host_connection (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, player_id INT DEFAULT NULL, UCID INT DEFAULT NULL, INDEX IDX_8164296F1FB8D185 (host_id), UNIQUE INDEX UNIQ_8164296F99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, UserId INT DEFAULT NULL, UName VARCHAR(64) DEFAULT NULL, PName VARBINARY(255) DEFAULT NULL, licence VARCHAR(16) DEFAULT NULL, ip VARCHAR(64) DEFAULT NULL, country VARCHAR(64) DEFAULT NULL, language VARCHAR(32) DEFAULT NULL, locale VARCHAR(16) DEFAULT NULL, offset VARCHAR(16) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, last_joined DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plugin (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, class VARCHAR(255) NOT NULL, dir VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, author VARCHAR(255) DEFAULT NULL, version VARCHAR(32) DEFAULT \'1.0.0\', active TINYINT(1) DEFAULT \'0\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE host_connection ADD CONSTRAINT FK_8164296F1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE host_connection ADD CONSTRAINT FK_8164296F99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE host_connection DROP FOREIGN KEY FK_8164296F1FB8D185');
        $this->addSql('ALTER TABLE host_connection DROP FOREIGN KEY FK_8164296F99E6F5DF');
        $this->addSql('DROP TABLE host');
        $this->addSql('DROP TABLE host_connection');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE plugin');
    }

}
